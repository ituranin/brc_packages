# brc_packages

Hier befinden sich die ROS-Pakete für die BRCs. Sie stellen den Kern des High-Level-Systems dar. Einige Pakete sind team-intern und deswegen leer. Die Dokumentation ist auch team-intern im eigenen Confluence-Wiki hinterlegt.

Der Code ist bereit für das autonome Fahren. Im Paket `brc_acceleration` existiert ein Beispiel für die Ansteuerung der Lenkung und der Antriebes. Es reicht um den Wagen im Acceleration-Event bis auf 80 km/h zu beschleunigen und auf der Strecke zu halten. Bei sehr breiten Streckenabgrenzungen kommt es zu Schlängelbewegungen, weswegen dieser Code bereits von einem State-Estimator und einer allgemeinen Pfadplanung ersetzt wurde.
