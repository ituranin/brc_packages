#include <iostream>
#include <sstream>
#include <math.h>
#include <string>
#include <bitset>

// ROS Libraries
#include "ros/ros.h"

#include "brc_base/Imu.h"
#include "brc_base/Gps.h"
#include "brc_base/Attitude.h"
#include "brc_base/Ins.h"

ros::Publisher pubIMU, pubGPS, pubAttitude, pubINS;


// Include this header file to get access to VectorNav sensors.
#include "vn/sensors.h"

using namespace std;
using namespace vn::math;
using namespace vn::sensors;
using namespace vn::protocol::uart;
using namespace vn::xplat;

// Method declarations for future use.
void BinaryAsyncMessageReceived(void* userData, Packet& p, size_t index);

std::string frame_id;

int main(int argc, char *argv[])
{
    std::cout.precision(10);
    
    ros::init(argc, argv, "vectornav");
    ros::NodeHandle n;
    
    pubIMU = n.advertise<brc_base::Imu>("vectornav/IMU", 1000);
    pubGPS = n.advertise<brc_base::Gps>("vectornav/GPS", 1000);
    pubAttitude = n.advertise<brc_base::Attitude>("vectornav/Attitude", 1000);
    pubINS = n.advertise<brc_base::Ins>("vectornav/INS", 1000);
    
    n.param<std::string>("frame_id", frame_id, "vectornav");
    
    // Serial Port Settings
    string SensorPort;
    int SensorBaudrate;
    
    n.param<std::string>("serial_port", SensorPort, "/dev/ttyUSB0");
    n.param<int>("serial_baud", SensorBaudrate, 115200);
    
    ROS_INFO("Connecting to: %s @ %d Baud", SensorPort.c_str(), SensorBaudrate);
    
    // Create a VnSensor object and connect to sensor
    VnSensor vs;
    vs.connect(SensorPort, SensorBaudrate);
    
    // Query the sensor's model number.
    string mn = vs.readModelNumber();
    ROS_INFO("Model Number: %s", mn.c_str());
    
    // Set Data output Freq [Hz]
    int async_output_rate;
    n.param<int>("async_output_rate", async_output_rate, 40);
    vs.writeAsyncDataOutputFrequency(async_output_rate);
    
    
    // Configure binary output message
    BinaryOutputRegister bor(
                             ASYNCMODE_PORT1,
                             1000 / async_output_rate,  // update rate [ms]
                             COMMONGROUP_NONE
                             ,
//                             TIMEGROUP_NONE
                                TIMEGROUP_TIMESTARTUP |
                                TIMEGROUP_TIMEGPS
                             ,
//                             IMUGROUP_NONE
                                IMUGROUP_UNCOMPACCEL |
                                IMUGROUP_UNCOMPGYRO |
                                IMUGROUP_ACCEL |
                                IMUGROUP_ANGULARRATE
                             ,
//                             GPSGROUP_NONE
//                                GPSGROUP_UTC |
                                GPSGROUP_NUMSATS |
                                GPSGROUP_FIX |
                                GPSGROUP_POSLLA |
                                GPSGROUP_VELNED |
                                GPSGROUP_POSU |
                                GPSGROUP_VELU
//                                GPSGROUP_TIMEU
                             ,
//                             ATTITUDEGROUP_NONE
                                ATTITUDEGROUP_YAWPITCHROLL |
                             ATTITUDEGROUP_QUATERNION |
                                ATTITUDEGROUP_ACCELNED |
                                ATTITUDEGROUP_LINEARACCELBODY |
                                ATTITUDEGROUP_LINEARACCELNED |
                                ATTITUDEGROUP_YPRU
                             ,
//                             INSGROUP_NONE
                                INSGROUP_INSSTATUS |
                                INSGROUP_POSLLA |
                                INSGROUP_VELBODY |
                                INSGROUP_VELNED |
                                INSGROUP_POSU |
                                INSGROUP_VELU
                             );
    
    vs.writeBinaryOutput1(bor);
    vs.registerAsyncPacketReceivedHandler(NULL, BinaryAsyncMessageReceived);
    
    
    // You spin me right round, baby
    // Right round like a record, baby
    // Right round round round
    ros::spin();
    
    
    // Node has been terminated
    vs.unregisterAsyncPacketReceivedHandler();
    vs.disconnect();
    return 0;
}

void BinaryAsyncMessageReceived(void* userData, Packet& p, size_t index)
{
    if (p.type() == Packet::TYPE_BINARY)
    {
        if (!p.isCompatible(
                            COMMONGROUP_NONE
                            ,
                            //                             TIMEGROUP_NONE
                            TIMEGROUP_TIMESTARTUP |
                            TIMEGROUP_TIMEGPS
                            ,
                            //                             IMUGROUP_NONE
                            IMUGROUP_UNCOMPACCEL |
                            IMUGROUP_UNCOMPGYRO |
                            IMUGROUP_ACCEL |
                            IMUGROUP_ANGULARRATE
                            ,
                            //                             GPSGROUP_NONE
//                            GPSGROUP_UTC |
                            GPSGROUP_NUMSATS |
                            GPSGROUP_FIX |
                            GPSGROUP_POSLLA |
                            GPSGROUP_VELNED |
                            GPSGROUP_POSU |
                            GPSGROUP_VELU
//                                                          GPSGROUP_TIMEU
                            ,
                            //                             ATTITUDEGROUP_NONE
                            ATTITUDEGROUP_YAWPITCHROLL |
                            ATTITUDEGROUP_QUATERNION |
                            ATTITUDEGROUP_ACCELNED |
                            ATTITUDEGROUP_LINEARACCELBODY |
                            ATTITUDEGROUP_LINEARACCELNED |
                            ATTITUDEGROUP_YPRU
                            ,
                            //                             INSGROUP_NONE
                            INSGROUP_INSSTATUS |
                            INSGROUP_POSLLA |
                            INSGROUP_VELBODY |
                            INSGROUP_VELNED |
                            INSGROUP_POSU |
                            INSGROUP_VELU
                            ))
            // Not the type of binary packet we are expecting.
        {
            ROS_INFO("Bin1 Output Config ist falsch.");
            return;
        }
        
        // Daten entpacken
        
        // Time Group
        uint64_t timeStartup = p.extractUint64(); // in Nano-Sekunden
        uint64_t timeGPS = p.extractUint64(); // in Nano-Sekunden seit 1980
        
        // IMU Group
        vec3f imuUncompAccel = p.extractVec3f(); // in m/s^2
        vec3f imuUncompGyro = p.extractVec3f(); // in rad/s
        vec3f imuAccel = p.extractVec3f();
        vec3f imuAngularRate = p.extractVec3f();

        // GPS Group
//        int8_t gpsUtcYear = p.extractInt8();
//        uint8_t gpsUtcMonth = p.extractUint8();
//        uint8_t gpsUtcDay = p.extractUint8();
//        uint8_t gpsUtcHour = p.extractUint8();
//        uint8_t gpsUtcMin = p.extractUint8();
//        uint8_t gpsUtcSec = p.extractUint8();
//        uint16_t gpsUtcMs = p.extractUint16();
//
        uint8_t gpsNumSats = p.extractUint8();
        uint8_t gpsFix = p.extractUint8();
        vec3d gpsPosLla = p.extractVec3d();
        vec3f gpsVelNed = p.extractVec3f();
        vec3f gpsPosU = p.extractVec3f();
        float gpsVelU = p.extractFloat();
//        uint32_t gpsTimeU = p.extractUint32();
//
        // Attitude Group
        vec3f attitudeYawPitchRoll = p.extractVec3f();
        vec4f attitudeQuaternion = p.extractVec4f();
        vec3f attitudeAccelNed = p.extractVec3f();
        vec3f attitudeLinearAccelBody = p.extractVec3f();
        vec3f attitudeLinearAccelNed = p.extractVec3f();
        vec3f attitudeYprU = p.extractVec3f();

        // INS Group
        uint16_t insStatus = p.extractUint16();
        vec3d insPosLla = p.extractVec3d();
        vec3f insVelBody = p.extractVec3f();
        vec3f insVelNed = p.extractVec3f();
        float insPosU = p.extractFloat();
        float insVelU = p.extractFloat();
        
        //-------------
        
        ros::Time headerTime = ros::Time::now();
        
        // IMU Pub
        brc_base::Imu msgIMU;
        
        msgIMU.header.stamp = headerTime;
        msgIMU.header.frame_id = frame_id;
        
        msgIMU.uncompAccel.x = imuUncompAccel[0];
        msgIMU.uncompAccel.y = imuUncompAccel[1];
        msgIMU.uncompAccel.z = imuUncompAccel[2];
        
        msgIMU.uncompGyro.x = imuUncompGyro[0];
        msgIMU.uncompGyro.y = imuUncompGyro[1];
        msgIMU.uncompGyro.z = imuUncompGyro[2];
        
        msgIMU.accel.x = imuAccel[0];
        msgIMU.accel.y = imuAccel[1];
        msgIMU.accel.z = imuAccel[2];
        
        msgIMU.angularRate.x = imuAngularRate[0];
        msgIMU.angularRate.y = imuAngularRate[1];
        msgIMU.angularRate.z = imuAngularRate[2];
        
        pubIMU.publish(msgIMU);
        
        // GPS Pub
        brc_base::Gps msgGPS;
        
        msgGPS.header.stamp = headerTime;
        msgGPS.header.frame_id = frame_id;
        
//        msgGPS.utc =
        msgGPS.numSats = gpsNumSats;
        msgGPS.fix = gpsFix;
        
        msgGPS.position.latitude = gpsPosLla[0];
        msgGPS.position.longitude = gpsPosLla[1];
        msgGPS.position.altitude = gpsPosLla[2];
        
        msgGPS.positionUncertainty.latitude = gpsPosU[0];
        msgGPS.positionUncertainty.longitude = gpsPosU[1];
        msgGPS.positionUncertainty.altitude = gpsPosU[2];

        msgGPS.velocityNed.x = gpsVelNed[0];
        msgGPS.velocityNed.y = gpsVelNed[1];
        msgGPS.velocityNed.z = gpsVelNed[2];
        
        msgGPS.velocityUncertainty = gpsVelU;
        
        pubGPS.publish(msgGPS);
        
        
        // Attitude Pub
        brc_base::Attitude msgAttitude;
        
        msgAttitude.header.stamp = headerTime;
        msgAttitude.header.frame_id = frame_id;
        
        msgAttitude.yawPitchRoll.yaw = attitudeYawPitchRoll[0];
        msgAttitude.yawPitchRoll.pitch = attitudeYawPitchRoll[1];
        msgAttitude.yawPitchRoll.roll = attitudeYawPitchRoll[2];
        
        msgAttitude.yawPitchRollUncertainty.yaw = attitudeYprU[0];
        msgAttitude.yawPitchRollUncertainty.pitch = attitudeYprU[1];
        msgAttitude.yawPitchRollUncertainty.roll = attitudeYprU[2];
        
        msgAttitude.orientation.x = attitudeQuaternion[0];
        msgAttitude.orientation.y = attitudeQuaternion[1];
        msgAttitude.orientation.z = attitudeQuaternion[2];
        msgAttitude.orientation.w = attitudeQuaternion[3];
        
        msgAttitude.accelNed.x = attitudeAccelNed[0];
        msgAttitude.accelNed.y = attitudeAccelNed[1];
        msgAttitude.accelNed.z = attitudeAccelNed[2];
        
        msgAttitude.linearAccelBody.x = attitudeLinearAccelBody[0];
        msgAttitude.linearAccelBody.y = attitudeLinearAccelBody[1];
        msgAttitude.linearAccelBody.z = attitudeLinearAccelBody[2];
        
        msgAttitude.linearAccelNed.x = attitudeLinearAccelNed[0];
        msgAttitude.linearAccelNed.y = attitudeLinearAccelNed[1];
        msgAttitude.linearAccelNed.z = attitudeLinearAccelNed[2];
        
        pubAttitude.publish(msgAttitude);

        // INS Pub
        brc_base::Ins msgIns;
        
        msgIns.header.stamp = headerTime;
        msgIns.header.frame_id = frame_id;
        
        uint8_t insStatusMode = (insStatus >> 0) & 0b11;
        uint8_t insStatusGpsFix = (insStatus >> 2) & 0b1;
        uint8_t insStatusErrorImu = (insStatus >> 4) & 0b1;
        uint8_t insStatusErrorMagPres = (insStatus >> 5) & 0b1;
        uint8_t insStatusErrorGps = (insStatus >> 6) & 0b1;
        uint8_t insStatusGpsHeading = (insStatus >> 8) & 0b1;
        uint8_t insStatusCompass = (insStatus >> 9) & 0b1;
        
        msgIns.status.insMode = insStatusMode;
        msgIns.status.gpsFix = insStatusGpsFix;
        msgIns.status.gpsHeadingIns = insStatusGpsHeading;
        msgIns.status.gpsCompass = insStatusCompass;
        msgIns.status.errorImu = insStatusErrorImu;
        msgIns.status.errorMagPres = insStatusErrorMagPres;
        msgIns.status.errorGps = insStatusErrorGps;
        
        msgIns.position.latitude = insPosLla[0];
        msgIns.position.longitude = insPosLla[1];
        msgIns.position.altitude = insPosLla[2];
        
        msgIns.positionUncertainty = insPosU;
        
        msgIns.velocityBody.x = insVelBody[0];
        msgIns.velocityBody.y = insVelBody[1];
        msgIns.velocityBody.z = insVelBody[2];
        
        msgIns.velocityNed.x = insVelNed[0];
        msgIns.velocityNed.y = insVelNed[1];
        msgIns.velocityNed.z = insVelNed[2];
        
        msgIns.velocityUncertainty = insVelU;
        
        pubINS.publish(msgIns);
    }
}

