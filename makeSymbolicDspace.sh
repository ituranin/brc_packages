#!/bin/bash
# Creates device specific sym-links 
# ln -sf /home/$USER/brc_packages/<package> /home/$USER/catkin_ws/src/<package>

ln -sf /home/$USER/brc_packages/brc_base /home/$USER/catkin_ws/src/brc_base
ln -sf /home/$USER/brc_packages/brc_imu /home/$USER/catkin_ws/src/brc_imu
ln -sf /home/$USER/brc_packages/brc_can /home/$USER/catkin_ws/src/brc_can
ln -sf /home/$USER/brc_packages/brc_director /home/$USER/catkin_ws/src/brc_director
ln -sf /home/$USER/brc_packages/brc_steering /home/$USER/catkin_ws/src/brc_steering
ln -sf /home/$USER/brc_packages/brc_tractivesystem /home/$USER/catkin_ws/src/brc_tractivesystem
ln -sf /home/$USER/brc_packages/brc_inspection /home/$USER/catkin_ws/src/brc_inspection
ln -sf /home/$USER/brc_packages/brc_acceleration /home/$USER/catkin_ws/src/brc_acceleration
ln -sf /home/$USER/brc_packages/brc_autox /home/$USER/catkin_ws/src/brc_autox
ln -sf /home/$USER/brc_packages/brc_cruisecontrol /home/$USER/catkin_ws/src/brc_cruisecontrol
ln -sf /home/$USER/brc_packages/brc_stateestimator /home/$USER/catkin_ws/src/brc_stateestimator
ln -sf /home/$USER/brc_packages/socketcan_interface /home/$USER/catkin_ws/src/socketcan_interface
ln -sf /home/$USER/brc_packages/velodyne /home/$USER/catkin_ws/src/velodyne
ln -sf /home/$USER/brc_packages/brc_tempomat /home/$USER/catkin_ws/src/brc_tempomat
ln -sf /home/$USER/brc_packages/brc_tools /home/$USER/catkin_ws/src/brc_tools

