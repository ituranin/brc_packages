#ifndef DATASETWRITER_H
#define DATASETWRITER_H

#include <brc_base/threadinterface.h>
#include <datasetentry.h>
#include <mutex>
#include <iostream>
#include <fstream>

using namespace std;

class DatasetWriter : public ThreadInterface
{
public:
  DatasetWriter(string path);
  ~DatasetWriter();

  void addEntry(DatasetEntry entry);
private:
  void process();
  void removeFirstEntry();

  long imageCount = 0;
  vector<DatasetEntry> queue;
  mutex queueMutex;
  ofstream file;
  string folderPath;
};

#endif // DATASETWRITER_H
