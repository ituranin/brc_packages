#ifndef DATASETENTRY_H
#define DATASETENTRY_H

#include <brc_base/camerathread.h>

// TODO add cone count for cones in picture and cones that account to onTrack state
class DatasetEntry
{
public:
  DatasetEntry();
  shared_ptr<Mat> image;
  int steering = 0;
  int throttle = 0;
  int brake = 0;
  int onTrack = 0;
  int gear = 0;
  int rpm = 0;
  int speed = 0;
  int yellow = 0;
  int blue = 0;
  int orange = 0;
  int allYellow = 0;
  int allBlue = 0;
  int allOrange = 0;
};

#endif // DATASETENTRY_H
