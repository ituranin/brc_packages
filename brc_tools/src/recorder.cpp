#define CM_MANUAL		0
#define CM_AUTONOMOUS	1

#include <ros/ros.h>
#include <rosbag/recorder.h>
#include <brc_base/ASState.h>
#include <brc_base/asStates.h>
#include <std_msgs/Time.h>
#include <std_msgs/Bool.h>
#include <boost/regex.h>

using namespace std;

static const string excludeExpression = "\/camera\/ImageTransport(\/compressedDepth|\/theora).*|\/camera\/ImageTransport$";

static bool captureStartReceived = false;
static bool captureShouldStop = false;

static ros::Subscriber asStateSubscriber;
static ros::Time captureShouldStopReceiveTime;

static uint8_t state = AS_STATE_OFF;

void setState(const brc_base::ASState::ConstPtr& msg) {
    state = msg->currentState;

    if(!captureStartReceived)
    {
        if(state == AS_STATE_READY)
        {
            captureStartReceived = true;
        }
    }
    else
    {
        if(state == AS_STATE_FINISHED || state == AS_STATE_EBS)
        {
            if(!captureShouldStop)
            {
                captureShouldStopReceiveTime = ros::Time::now();
            }

            captureShouldStop = true;
        }

        if(captureShouldStop)
        {
            if((ros::Time::now() - captureShouldStopReceiveTime).toSec() >= 10.0)
            {
                ros::shutdown();
            }
        }
    }
}

int main(int argc, char *argv[]) {

  int retCode = 0;

  ros::init(argc, argv, "brcBagRecorder");
  ros::NodeHandle n;

  asStateSubscriber = n.subscribe("/asState", 1, setState);

  ros::Rate r(20);

  captureShouldStopReceiveTime = ros::Time::now();

  while (!captureStartReceived && ros::ok()) {
    ros::spinOnce();
    r.sleep();
  }

  // set options
  rosbag::RecorderOptions options;
  options.record_all = true;
  options.append_date = true;

  const string user = getenv("USER");
  string baseFolderPath = "/home/" + user + "/bags/";
  options.prefix = baseFolderPath;

  options.do_exclude = true;
  options.exclude_regex = excludeExpression;

  rosbag::Recorder recorder(options);

  // will spin until done
  retCode = recorder.run();

  return retCode;
}
