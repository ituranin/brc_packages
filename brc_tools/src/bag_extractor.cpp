#include <cstdlib>
#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/PointCloud2.h>
#include <brc_base/ControlLog.h>
#include <brc_base/SensorData.h>
#include <brc_base/DashOne.h>
#include <brc_base/cone.h>
#include <brc_base/detectionhelpers.h>
#include <math.h>
#include <datasetentry.h>
#include <datasetwriter.h>
#include <boost/foreach.hpp>
#include <rosbag/bag.h>
#include <rosbag/view.h>

using namespace std;
using namespace cv;
using namespace sensor_msgs;
using namespace brc_base;

static DatasetWriter* datasetWriter = nullptr;
static DatasetEntry entry;
static bool initializedPair = false;
static bool gotSteering = false;

int main(int argc, char *argv[])
{

    ros::init(argc, argv, "bag_extractor");
    ros::NodeHandle n;
    uint32_t queueSize = 10;

    // TODO change to use argument
    datasetWriter = new DatasetWriter("-");
    datasetWriter->start();

    rosbag::Bag bag;
    bag.open("-", rosbag::bagmode::Read);

    std::vector<std::string> topics;
    topics.push_back("/camera/ImageTransport/compressed");
    topics.push_back("/steering/sensorData1");

    rosbag::View view(bag, rosbag::TopicQuery(topics));

    std::string user = std::getenv("USER");

    long count = 0;
    BOOST_FOREACH(rosbag::MessageInstance const m, view)
    {
        if(!initializedPair)
        {
            std::cout << "init 1" << std::endl;
            entry = DatasetEntry();
            std::cout << "init 2" << std::endl;
            initializedPair = true;
        }

        if(m.getTopic() == topics.at(0) || m.getTopic() == "/" + topics.at(0))
        {
            //std::cout << "img 1" << std::endl;
            sensor_msgs::CompressedImage::ConstPtr image = m.instantiate<sensor_msgs::CompressedImage>();
            //std::cout << "img 2" << std::endl;
            if(image != nullptr)
            {
                //std::cout << "img 2.5" << std::endl;
                cv_bridge::CvImagePtr cv_ptr;
                //std::cout << "img 3" << std::endl;
                cv_ptr = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::BGR8);

                entry.image = make_shared<Mat>(cv_ptr->image.clone());
                //std::cout << "got image" << std::endl;

                if(gotSteering)
                {
                    datasetWriter->addEntry(entry);
                    gotSteering = false;
                    initializedPair = false;
                    std::cout << count++ << std::endl;
                    //std::cout << "shoudl write" << std::endl;
                }
            }
        }

        if(m.getTopic() == topics.at(1) || m.getTopic() == "/" + topics.at(1))
        {
            brc_base::SensorData::ConstPtr steering = m.instantiate<brc_base::SensorData>();

            if(steering != nullptr)
            {
                entry.steering = static_cast<int16_t>(steering->value2);
                gotSteering = true;
                //std::cout << "got      steering" << std::endl;
            }
        }
    }

    bag.close();

    ros::spin();

    datasetWriter->stop();

    while(datasetWriter->isRunning());

    delete datasetWriter;

    return 0;

}
