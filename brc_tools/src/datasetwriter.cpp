#include "datasetwriter.h"

DatasetWriter::DatasetWriter(string path)
{
    folderPath = path;
    file.open(path + "set.csv");

    file << "FILENAME"
         << ","
         << "STEERING"
         << ","
         << "BRAKE"
         << ","
         << "THROTTLE"
         << ","
         << "RPM"
         << ","
         << "GEAR"
         << ","
         << "SPEED"
         << ","
         << "ONTRACK"
         << ","
         << "YELLOW"
         << ","
         << "BLUE"
         << ","
         << "ORANGE"
         << ","
         << "ALLYELLOW"
         << ","
         << "ALLBLUE"
         << ","
         << "ALLORANGE"
         << '\n';
}

DatasetWriter::~DatasetWriter()
{
    file.close();
}

void DatasetWriter::addEntry(DatasetEntry entry)
{
    lock_guard<mutex> guard(queueMutex);
    queue.push_back(entry);
}

void DatasetWriter::process()
{
    if(queue.size() != 0)
    {
        DatasetEntry entry = queue[0];
        string name = "img" + to_string(imageCount) + ".jpg";
        file << name
             << ","
             << entry.steering
             << ","
             << entry.brake
             << ","
             << entry.throttle
             << ","
             << entry.rpm
             << ","
             << entry.gear
             << ","
             << entry.speed
             << ","
             << entry.onTrack
             << ","
             << entry.yellow
             << ","
             << entry.blue
             << ","
             << entry.orange
             << ","
             << entry.allYellow
             << ","
             << entry.allBlue
             << ","
             << entry.allOrange
             << '\n';
        cv::imwrite(folderPath + name, *entry.image);
        removeFirstEntry();
        imageCount++;
    }
}

void DatasetWriter::removeFirstEntry()
{
    lock_guard<mutex> guard(queueMutex);
    queue.erase(queue.begin());
}
