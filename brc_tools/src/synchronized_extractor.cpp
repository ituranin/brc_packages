#include <cstdlib>
#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/PointCloud2.h>
#include <brc_base/ControlLog.h>
#include <brc_base/SensorData.h>
#include <brc_base/DashOne.h>
#include <brc_base/cone.h>
#include <brc_base/detectionhelpers.h>
#include <math.h>
#include <datasetentry.h>
#include <datasetwriter.h>

using namespace std;
using namespace cv;
using namespace sensor_msgs;
using namespace brc_base;
static std::vector<Cone> filtered;
static int blue = 0;
static int yellow = 0;
static int orange = 0;
static int allBlue = 0;
static int allYellow = 0;
static int allOrange = 0;

static DatasetWriter* datasetWriter = nullptr;

typedef message_filters::sync_policies::ApproximateTime<CompressedImage, SensorData, SensorData, SensorData, DashOne> NNSync;

void callback(const CompressedImageConstPtr& image,
              const SensorDataConstPtr& steering,
              const SensorDataConstPtr& throttle,
              const SensorDataConstPtr& brake,
              const DashOneConstPtr& dash)
{
    DatasetEntry entry;
    entry.steering = static_cast<int16_t>(steering->value2);
    entry.throttle = throttle->value1;
    entry.brake = brake->value1;
    entry.gear = dash->gear;
    entry.rpm = dash->rpm;
    entry.speed = static_cast<int>(dash->speed * 0.036);
    entry.yellow = yellow;
    entry.blue = blue;
    entry.orange = orange;
    entry.allYellow = allYellow;
    entry.allBlue = allBlue;
    entry.allOrange = allOrange;

    if(filtered.size() != 0)
    {
        if(blue == 0 && yellow == 0)
        {
            entry.onTrack = 0;
        } else {
            entry.onTrack = 1;
        }
    } else {
        entry.onTrack = 0;
    }

    cv_bridge::CvImagePtr cv_ptr;
    cv_ptr = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::BGR8);

    entry.image = make_shared<Mat>(cv_ptr->image.clone());
    datasetWriter->addEntry(entry);
}

void conesCallback(const PointCloud2ConstPtr& cones)
{
    std::vector<Cone> detections;
    detectionHelper::readDetectionsToVector(cones, detections);
    filtered.clear();
    blue = 0;
    yellow = 0;
    orange = 0;
    allBlue = 0;
    allYellow = 0;
    allOrange = 0;
    for (size_t i = 0; i < detections.size(); i++) {
        if(abs(detections[i].x) < 100 && abs(detections[i].y) < 100)
        {
            if(detections[i].type == CONE_ORANGE || detections[i].type == CONE_ORANGE_BIG) allOrange++;
            if(detections[i].type == CONE_BLUE) allBlue++;
            if(detections[i].type == CONE_YELLOW) allYellow++;
        }

        if(abs(detections[i].x) < 15 && abs(detections[i].y) < 40)
        {
            filtered.push_back(detections[i]);
            if(detections[i].type == CONE_ORANGE || detections[i].type == CONE_ORANGE_BIG) orange++;
            if(detections[i].type == CONE_BLUE) blue++;
            if(detections[i].type == CONE_YELLOW) yellow++;
        }
    }
}

int main(int argc, char *argv[])
{

    ros::init(argc, argv, "synchronized_extractor");
    ros::NodeHandle n;
    uint32_t queueSize = 10;

    // TODO change to use argument
    datasetWriter = new DatasetWriter("-");
    datasetWriter->start();

    message_filters::Subscriber<CompressedImage> cameraSub(n, "/camera/ImageTransport/compressed", 1);
    message_filters::Subscriber<SensorData> steeringSub(n, "/steering/sensorData1", 1);
    message_filters::Subscriber<SensorData> throttleSub(n, "/throttle/sensorData1", 1);
    message_filters::Subscriber<SensorData> serviceBrakeSub(n, "/serviceBrake/sensorData1", 1);
    message_filters::Subscriber<DashOne> dashSub(n, "/dashOne", 1);
    ros::Subscriber coneSub = n.subscribe("/camera/ConePosition", 1, &conesCallback);

    message_filters::Synchronizer<NNSync> sync(NNSync(queueSize), cameraSub, steeringSub, throttleSub, serviceBrakeSub, dashSub);
    sync.registerCallback(boost::bind(&callback, _1, _2, _3, _4, _5));

    std::string user = std::getenv("USER");

    ros::spin();

    datasetWriter->stop();

    while(datasetWriter->isRunning());

    delete datasetWriter;

    // TODO

    return 0;

}
