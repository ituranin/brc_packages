#!/bin/bash
# Creates device specific sym-links 
# ln -sf /home/$USER/brc_packages/<package> /home/$USER/catkin_ws/src/<package>

ln -sf /home/$USER/brc_packages/brc_base /home/$USER/catkin_ws/src/brc_base
ln -sf /home/$USER/brc_packages/brc_yolo /home/$USER/catkin_ws/src/brc_yolo
ln -sf /home/$USER/brc_packages/brc_ssd_mobilenet /home/$USER/catkin_ws/src/brc_ssd_mobilenet
ln -sf /home/$USER/brc_packages/brc_matrixvision /home/$USER/catkin_ws/src/brc_matrixvision
ln -sf /home/$USER/brc_packages/brc_baumer /home/$USER/catkin_ws/src/brc_baumer
ln -sf /home/$USER/brc_packages/cv_bridge /home/$USER/catkin_ws/src/cv_bridge
ln -sf /home/$USER/brc_packages/image_transport /home/$USER/catkin_ws/src/image_transport
ln -sf /home/$USER/brc_packages/compressed_image_transport /home/$USER/catkin_ws/src/compressed_image_transport
ln -sf /home/$USER/brc_packages/velodyne /home/$USER/catkin_ws/src/velodyne
ln -sf /home/$USER/brc_packages/brc_stereo_camera /home/$USER/catkin_ws/src/brc_stereo_camera
