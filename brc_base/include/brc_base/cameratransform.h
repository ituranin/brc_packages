#include <opencv2/opencv.hpp>
/**
 * @brief #frankregeltdas #frankhatgeregelt
 */
class CameraTransform {

public:
        CameraTransform( int w=2048, int h=1088, float f=963 ) {
		this->w = w;
		this->h = h;
		this->cameraMatrix = (cv::Mat_<double>(3, 3) <<
				f, 0, (w-1)/2.0,
				0, f, (h-1)/2.0,
				0, 0,       1.0
			);
		updateParams(); 
        }

        int get_w() const { return w; }
        int get_h() const { return h; }
        int get_f() const { return f; }

	// cam roll angle in degree. positive value -> downwards
        void setPitch( float phi ) { this->phi   =   phi * M_PI / 180.0; updateParams(); }
        void setYaw( float theta ) { this->theta = theta * M_PI / 180.0; updateParams(); }
        void setRoll( float psi )  { this->psi   =   psi * M_PI / 180.0; updateParams(); }


	// the world coordinates are in the SAE Vehicle Axis System
	// x faces forward (north)
	// y faces to the right hand side (east)
	// z faces downwards (down)
	// all measured in millimeters
	cv::Point2d world2cam( cv::Point3d pt )
	{
		cv::Mat worldPoint4 = ( cv::Mat_<double>(4, 1) << pt.y, pt.z, pt.x, 1 ); // we swap the coordinates!
		cv::Mat projectedPt = projectionMatrix * worldPoint4;
		// normalize
		double x = projectedPt.at<double>(0, 0); 			
		double y = projectedPt.at<double>(1, 0); 			
		double z = projectedPt.at<double>(2, 0); 			
		cv::Point2d imagePt = cv::Point2d( x/z, y/z );
		//std::cout << "world2cam(" << pt << ") = " << imagePt << std::endl;
		return imagePt;
        }

	// z is the height of the world plane in which the world point is assumed to be
	cv::Point2d cam2world( cv::Point2d pt, double z=0.0 )
	{
		// inverse problem: we fix a plane (e.g. the ground floor with height y==0) and find the 3D world point from image coordinates
		cv::Mat uv1 = (cv::Mat_<double>(3, 1) << pt.x, pt.y, 1);
		//std::cout << "uv1 " << endl << uv1 << endl << endl;

		// in camera coordinates, compute the line of sight from the camera center C=(0,0,0) in direction of image point uv1:
		cv::Mat uvLOSCam = cameraMatrixInv * uv1;
		//cout << "uvLOSCam " << endl << uvLOSCam << endl << endl;

		// transform to normalized (z=1) camera coordinates
		cv::Mat uvLOSworld = rotationMatrixInv * uvLOSCam; // R.inv() is the same as the transposed Rt
		//cout  << "uvLOSworld " << endl << uvLOSworld << endl << endl;

		// define the plane where the world points are assumed to be (a*x+b*y+c*z+d=0): (a,b,c,d)=(0,1,0,0) (floor)
		double a = 0;
		double b = 1;
		double c = 0;
		double d = -z;

		// find a t such that a*(C_x+v_x*t) + b*(C_y+v_y*t) + c*(C_z+v_z*t) + d = 0.
		double C_x = rotatedTranslationVector.at<double>(0);
		double C_y = rotatedTranslationVector.at<double>(1);
		double C_z = rotatedTranslationVector.at<double>(2);
		double v_x = uvLOSworld.at<double>(0);
		double v_y = uvLOSworld.at<double>(1);
		double v_z = uvLOSworld.at<double>(2);
		double t = -(a*C_x + b*C_y + c*C_z + d) / (a*v_x + b*v_y + c*v_z);
		//cout << "a*(C_x+v_x*t) + b*(C_y+v_y*t) + c*(C_z+v_z*t) + d: " << a*(C_x + v_x*t) + b*(C_y + v_y*t) + c*(C_z + v_z*t) + d << endl << endl;

		cv::Mat worldPt = rotatedTranslationVector + t*uvLOSworld;
		//cout << "worldPt " << endl << worldPt << endl << endl;

		return cv::Point2d( worldPt.at<double>(0,2), worldPt.at<double>(0,0));
        }

        static cv::Point2d pointFromCoords(cv::Mat* image, float yMin, float xMin, float yMax, float xMax) {
            float xMinScaled = xMin * image->cols;
            float xMaxScaled = xMax * image->cols;
            float yMaxScaled = yMax * image->rows;
            float yMinScaled = yMin * image->rows;
            float xScaled = xMinScaled + ( (xMaxScaled - xMinScaled) / 2 );
            return cv::Point2d(xScaled, yMaxScaled);
        }


protected:

	// camera intrinsics
	int w;
	int h;
	double f; // from camera calibration

	cv::Mat cameraMatrix = (cv::Mat_<double>(3, 3) <<
		f, 0, 1023.5,
		0, f,  543.5,
		0, 0,    1.0
	);

	// distortion for our lens is small and currently not used
	//std::vector<double> distCoeffs = { -1.2775376033365005e-01, 7.9831308554251565e-02, 7.8715936603502700e-04, -1.7794646662773231e-04, -2.0487629570574310e-02 };

	// camera extrinsics: pose
	double theta =  -1 * M_PI / 180.0;		//   yaw angle -1� (estimated from scene image)
	double   phi = +23 * M_PI /180.0;		// pitch angle 23� (estimated from scene image)
	double   psi =  -1 * M_PI / 180.0;		//  roll angle -1� (estimated from scene image)

	double x = 0;
	double y = 0;
	double z = -1050;	// camera height above the ground

        cv::Mat rotationMatrix; 	// a 3x3 rotation matrix is an alternative representation for 3D rotations
	cv::Mat rotationMatrixInv;	// inverse rotation matrix (same as transposed)

	cv::Mat translationVector; 
	cv::Mat rotatedTranslationVector;

	cv::Mat cameraMatrixInv; 	// inverse camera matrix
	cv::Mat extrinsicMatrix;
	cv::Mat projectionMatrix;

	void updateParams() {

		cameraMatrixInv = cameraMatrix.inv();

		cv::Mat yaw = (cv::Mat_<double>(3, 3) <<
			cos(theta), 0, sin(theta),
			0, 1, 0,
			-sin(theta), 0, cos(theta)
		);

		cv::Mat pitch = (cv::Mat_<double>(3, 3) <<
			1, 0, 0,
			0, cos(phi), -sin(phi),
			0, sin(phi), cos(phi)
		);

		cv::Mat roll = (cv::Mat_<double>(3, 3) <<
			cos(psi), -sin(psi), 0,
			sin(psi), cos(psi), 0,
			0, 0, 1
		);  

		rotationMatrix = yaw * pitch * roll;
		rotationMatrixInv = rotationMatrix.inv();
 
		translationVector = (cv::Mat_<double>(3,1) << x, z, y );

		rotatedTranslationVector = rotationMatrixInv * translationVector;
		cv::hconcat(rotationMatrix, -translationVector, extrinsicMatrix);

		projectionMatrix = cameraMatrix * extrinsicMatrix;
        }

};

