#ifndef CAMERATHREAD_H
#define CAMERATHREAD_H

#include "threadinterface.h"
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <mutex>
#include <stdexcept>

using namespace cv;
using namespace std;

typedef void (*CameraCallbackFunction)(shared_ptr<Mat> image);

class CameraThread : public ThreadInterface
{
public:
    CameraThread();
    virtual ~CameraThread();

    virtual void initCamera(string settingsPath) = 0;
    void startCapture();
    shared_ptr<Mat> getFrame();
    void setFrame(shared_ptr<Mat> image);

    bool initialized = false;

    void setCallback(const CameraCallbackFunction &value);

private:
    shared_ptr<Mat> frame;
    CameraCallbackFunction callback = nullptr;
    mutex frameMutex;
};

#endif // CAMERATHREAD_H
