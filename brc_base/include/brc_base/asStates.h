/*
 * asStates.h
 *
 */ 


#ifndef ASSTATES_H_
#define ASSTATES_H_

#include <stdint.h>

//////////////////////////////////////////////////////////////////////////
//AS States
#define AS_STATE_OFF		0
#define AS_STATE_READY		1
#define AS_STATE_DRIVING	2
#define AS_STATE_FINISHED	3
#define AS_STATE_EBS		4
#define AS_STATE_MANUAL		5
#define AS_STATE_LIGHT      6   // extra light mode state

//ETC States
#define ETC_STATE_OFF			0
#define ETC_STATE_AUTONOMOUS	1
#define	ETC_STATE_MANUAL		2

//BAC-EBS States

#define EBS_STATE_UNAVAILABLE   0
#define EBS_STATE_ARMED         1
#define EBS_STATE_ACTIVATED     2

//BAC_SBC States

#define SB_STATE_UNAVAILABLE    0
#define SB_STATE_ENGAGED        1
#define SB_STATE_AVAILABLE      2

//SAC States

#define SA_STATE_UNAVAILABLE    0
#define SA_STATE_AVAILABLE      1

//HSC States

#define HS_STATE_UNAVAILABLE    0
#define HS_STATE_AVAILABLE      1

//CAC States

#define CA_STATE_UNAVAILABLE    0
#define CA_STATE_ENGAGED        1
#define CA_STATE_AVAILABLE      2

//MCU States
#define MCU_STATE_PASSIV        0
#define MCU_STATE_LIGHT         1

//CPU States
#define CPU_STATE_OFF           0
#define CPU_STATE_READY         1
#define CPU_STATE_ACTIVE        2
#define CPU_STATE_FINISHED      3

//AMI States
#define AMI_STATE_NORMAL        0
#define AMI_STATE_SHOW_RESTART  1
#define AMI_STATE_SHOW_RELEASE  2
#define AMI_STATE_LOCKED        3

// AMI SUBSTATES
#define MISSION_NONE            0
#define MISSION_MANUAL          1
#define MISSION_MAINTENANCE     2
#define MISSION_ACCELERATION    3
#define MISSION_SKIDPAD         4
#define MISSION_AUTOCROSS       5
#define MISSION_TRACKDRIVE      6
#define MISSION_EBSTEST         7
#define MISSION_INSPECTION      8
#define MISSION_CONTROLLER      9

// ASSI States
#define ASSI_STATE_OFF          0
#define ASSI_STATE_YELLOW_CONST 1
#define ASSI_STATE_YELLOW_FLASH 2
#define ASSI_STATE_BLUE_CONST   3
#define ASSI_STATE_BLUE_FLASH   4

//////////////////////////////////////////////////////////////////////////
//Masks
#define AS_STATE_MASK		0b111			//3 bits
#define AS_SUBSTATE_MASK  0b11111			//5 bits

#define CS_STATE_MASK		 0b11			//2 bits
#define CS_SUBSTATE_MASK   0b1111			//4 bits
#define ASSI_STATE_MASK     0b111           //3 bits

typedef struct asStates {
	uint8_t asState;
	uint8_t cpuState;
	uint8_t vcState;
	uint8_t ebsState;
	uint8_t sbState;
	uint8_t sacState;
	uint8_t cacState;
	uint8_t etcState;
	uint8_t hscState;
	uint8_t amiState;
}asStates;

/*
* writes the asStates into a buffer which can be sent by CAN (as data field buffer)
* length should be minimum 6
*/
void convertASStatesToASStateMsgBuffer(asStates* states, uint8_t* buffer, uint8_t length);

/*
* converts the asStates from a buffer (e.g. CAN message data buffer) to the asState type
* length should be minimum 6
*/
void convertASStateMsgBufferToASStates(asStates* states, uint8_t* buffer, uint8_t length);

//////////////////////////////////////////////////////////////////////////
//AS States

/*
*	returns main asState (from FS-rules)
*	example call: getState(asStates.asState)
*/
uint8_t getASState(uint8_t asState);

/*
*	returns sub asState (from FS-rules)
*	example call: getState(asStates.asState)
*/
uint8_t getASSubstate(uint8_t asState);


/*
 *	returns asState generated from rules state and substate
 *	example usage: asStates.asState = getFullASState(off, 0);
 */
 uint8_t getFullASState(uint8_t state, uint8_t substate);

 //////////////////////////////////////////////////////////////////////////
 //CS States

 /*
*	returns main state (from FS-rules) from control state
*	example call: getState(asStates.sacState)
*/
uint8_t getCSState(uint8_t csState);

/*
*	returns substate (from FS-rules) from control state
*	example call: getState(asStates.sacState)
*/
uint8_t getCSSubstate(uint8_t csState);


/*
 *	returns csState generated from state and substate
 *	example usage: asStates.sacState = getFullCSState(off, 0);
 */
 uint8_t getFullCSState(uint8_t state, uint8_t substate);

#endif /* ASSTATES_H_ */
