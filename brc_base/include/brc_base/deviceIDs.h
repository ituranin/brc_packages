/*
 * deviceIDs.h
 *
 */ 


#ifndef DEVICEIDS_H_
#define DEVICEIDS_H_

#define DEV_ID_ASSC		0x0
#define DEV_ID_ASCPU	0x1
#define DEV_ID_ASGPU	0x2
#define DEV_ID_ASBAC	0x3
#define DEV_ID_ASSAC	0x4
#define DEV_ID_ASCAC	0x5
#define DEV_ID_ASSWC	0x6
#define DEV_ID_ASETC	0x7
#define DEV_ID_ASHSC	0x8
#define DEV_ID_AMI		0x9
#define DEV_ID_MCU		0xA
#define DEV_ID_ASSBC		0xB
#define DEV_ID_X3		0xC
#define DEV_ID_X4		0xD
#define DEV_ID_X5		0xE
#define DEV_ID_X6		0xF

#endif /* DEVICEIDS_H_ */
