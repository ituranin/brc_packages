#ifndef SETTINGSLOADER_H
#define SETTINGSLOADER_H

#include <jsoncpp/json/value.h>
#include <jsoncpp/json/reader.h>
#include <iostream>
#include <fstream>

using namespace std;

class SettingsLoader
{
public:
    static SettingsLoader* getInstance();
    Json::Value loadSettings(const string fileName);
private:
    string settingsPath = "";
    static SettingsLoader* _instance;
    SettingsLoader();
    ~SettingsLoader();
    SettingsLoader(const SettingsLoader&);
};

#endif // SETTINGSLOADER_H
