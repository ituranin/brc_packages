#ifndef BRCHELPERS_H
#define BRCHELPERS_H

#define MSG_BASE_FILTER             0xFF0
#define MSG_DEVID_FILTER            0x00F

#define LOW_BYTE(x)        (x & 0xff)
#define HIGH_BYTE(x)       ((x >> 8) & 0xff)

#define DEVICE_ID_NUMBER               16

#include <iostream>

const std::string mainTopics[DEVICE_ID_NUMBER] = {
    "stateControl",
    "cpu",
    "vc",
    "ebs",
    "steering",
    "clutch",
    "steeringWheel",
    "throttle",
    "hsc",
    "mission",
    "mcu",
    "serviceBrake",
    "ecu",
    "",
    "",
    ""
};

// https://stackoverflow.com/questions/6499183/converting-a-uint32-value-into-a-uint8-array4
void int32ToInt8ArrayBigEndian(int32_t &value, int8_t (&output) [4]) {
    output[0] = value >> 24;
    output[1] = static_cast<int8_t>(value >> 16);
    output[2] = static_cast<int8_t>(value >>  8);
    output[3] = static_cast<int8_t>(value);
}

int filterDeviceID(const unsigned int frameID) {
  return frameID & MSG_DEVID_FILTER;
}

uint16_t to16bit(uint8_t low, uint8_t high) {
  return low | static_cast<uint16_t>(high << 8);
}

// https://stats.stackexchange.com/questions/281162/scale-a-number-between-a-range
int8_t normalizeSteeringAngle(int16_t value) {
    return static_cast<int8_t>( ( ( ( (250 - (-250)) * (value - (-105)) ) / (105 - (-105)) ) + (-250) ) * 0.5 );
}

int16_t getAppsPosition(double percentage)
{
    if(percentage > 1)
       return 8192;
    if(percentage < 0)
       return 0;

    return static_cast<int16_t>(8192.0 * percentage);
}

int16_t getAppsOutput(double percentage)
{
    //limit throttle to 0-100%
    if(percentage < 0)
       percentage = 0;
    if(percentage > 1)
      percentage = 1;

    return getAppsPosition(percentage);
}

//pressure in bar -> 8bit integer     1bar -> 2
int16_t getBrakePressureOutput(double pressure)
{
    if(pressure > 127.5)
       return 255;
    if(pressure < 0)
       return 0;

    return static_cast<int16_t>(pressure * 2);
}

//can input 16bit -> pressure in bar
double getBrakePressureInput(int16_t pressure)
{
    return static_cast<double>(pressure) / 100;
}

//pressure in bar -> 8bit integer      1bar -> 10
int16_t getClutchPressureOutput(double pressure)
{
    if(pressure > 25.5)
       return 255;
    if(pressure < 0)
       return 0;

    return static_cast<int16_t>(pressure * 10);
}

//can input 16bit -> pressure in bar
double getClutchPressureInput(int16_t pressure)
{
    return static_cast<double>(pressure) / 100;
}

#endif
