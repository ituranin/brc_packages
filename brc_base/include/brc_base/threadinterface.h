#ifndef THREADINTERFACE_H
#define THREADINTERFACE_H

#include <thread>
#include "stopwatch.h"

using namespace std;

class ThreadInterface
{
public:
    ThreadInterface();
    virtual ~ThreadInterface();
    void start();
    void stop();
    void setFps(int fps);
    bool isRunning() const;

private:
    bool running = false;
    int ms = 0;
    StopWatch timeTracker;

    void run();
    virtual void process() = 0;
};

#endif // THREADINTERFACE_H
