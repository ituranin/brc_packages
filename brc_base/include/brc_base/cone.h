#ifndef CONE_H
#define CONE_H

#define CONE_ORANGE     1.0f
#define CONE_YELLOW     2.0f
#define CONE_BLUE       3.0f
#define CONE_ORANGE_BIG 4.0f

class Cone
{
public:
  Cone();
  ~Cone();

  float x = 0.0f;
  float y = 0.0f;
  float z = 0.0f;
  float score = 0.0f;
  float type = 0.0f;
};

#endif // CONE_H
