#ifndef CONTROLSERVER_H
#define CONTROLSERVER_H

#include "threadinterface.h"
#include "mutex"
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <iostream>

using namespace std;

class ControlServer : public ThreadInterface
{
public:
  ControlServer();
  ~ControlServer();

  int16_t getSteeringvalue();

private:
  void setSteeringvalue(const int16_t &value);
  void process();
  int16_t steeringvalue = 0;
  mutex steeringMutex;
  boost::asio::io_service io_service;
  boost::asio::ip::udp::socket* socket = nullptr;
};

#endif // CONTROLSERVER_H
