#ifndef MEANFILTER_H
#define MEANFILTER_H

template<class T>
class MeanFilter
{
public:
  MeanFilter(){}

  ~MeanFilter(){}

  T getFiltered(T input)
  {
      if(value == 0)
      {
          value = input;
      }

      value = (value + input) / 2;

      return value;
  }

  T getValue()
  {
      return value;
  }

private:
  T value = 0;
};

#endif // MEANFILTER_H
