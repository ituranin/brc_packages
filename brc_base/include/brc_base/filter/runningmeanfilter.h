#ifndef RUNNINGMEANFILTER_H
#define RUNNINGMEANFILTER_H

#include <vector>
#include <stdexcept>
#include <iostream>

using namespace std;

template<class T>
class RunningMeanFilter
{
public:
  RunningMeanFilter(int size)
  {
      if(size <= 0)
      {
          throw std::invalid_argument("size must be positive and not 0");
      }

      this->size = size;
  }

  ~RunningMeanFilter(){}

  T getFiltered(T input)
  {
      if (holder.size() >= static_cast<unsigned long>(this->size)) {
          holder.erase(holder.begin());
      }

      holder.push_back(input);

      T filtered = 0;

      for (size_t i = 0; i < holder.size(); i++) {
          filtered += holder[i];
      }

      filtered = filtered / holder.size();

      return filtered;
  }

private:
  vector<T> holder;
  int size = 0;
};

#endif // RUNNINGMEANFILTER_H
