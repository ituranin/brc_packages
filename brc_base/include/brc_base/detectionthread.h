#ifndef DETECTIONTHREAD_H
#define DETECTIONTHREAD_H

#include "threadinterface.h"
#include "camerathread.h"
#include "cameratransform.h"

class DetectionThread : public ThreadInterface
{
public:
    DetectionThread();
    virtual ~DetectionThread();

    void initDetection(CameraThread* cameraThread);
    void startDetection();
    CameraThread *getCamera() const;
    bool initialized = false;
    CameraTransform *transform = nullptr;

    shared_ptr<Mat> getFrame();
    void setFrame(shared_ptr<Mat> value);

    cv::Point2f pointFromCoords(Mat& image, float yMin, float xMin, float yMax, float xMax);

private:
    shared_ptr<Mat> frame;
    mutex frameMutex;
    CameraThread *camera = nullptr;
};

#endif // DETECTIONTHREAD_H
