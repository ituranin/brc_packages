#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <chrono>

using namespace std::chrono;

/**
 * @brief Eine einfache Klasse für das Time-Keeping
 */
class StopWatch
{
public:
    StopWatch() {}

    /**
     * @brief start Nimmt den Start-Timestamp auf
     */
    void start(){
        this->t1 = steady_clock::now();
    }

    /**
     * @brief stop Nimmt den End-Timestamp auf
     */
    void stop(){
        this->t2 = steady_clock::now();
    }

    /**
     * @brief getDurationInMilliseconds Berechnet aus Start -und Endzeit die vergangenen Millisekunden
     * @return Zeit in Millisekunden
     */
    long long getDurationInMilliseconds(){
        return duration_cast<milliseconds>( this->t2 - this->t1 ).count();
    }

    /**
     * @brief getDurationInMicroseconds Berechnet aus Start -und Endzeit die vergangenen Microsekunden
     * @return Zeit in Microsekunden
     */
    long long getDurationInMicroseconds(){
        return duration_cast<microseconds>( this->t2 - this->t1 ).count();
    }

private:

    /**
     * @brief t1 Startzeit
     */
    steady_clock::time_point t1 = steady_clock::now();

    /**
     * @brief t2 Endzeit
     */
    steady_clock::time_point t2 = steady_clock::now();
};

#endif // STOPWATCH_H
