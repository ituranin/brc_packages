#ifndef TRACTIVESYSTEMRESULT_H
#define TRACTIVESYSTEMRESULT_H

#include "baseinputoutput.h"

class TractiveSystemResult : public BaseInputOutput
{
public:
    TractiveSystemResult();
    virtual ~TractiveSystemResult();
    int16_t apps = 0;
    int16_t clutch = 0;
    int16_t brake = 0;      //TODO ACHTUNG
    int16_t brakeRear = 0;
    int16_t brakeFront = 0;
    virtual string getType();
};

#endif // TRACTIVESYSTEMRESULT_H
