#ifndef BASECALCULATION_H
#define BASECALCULATION_H

#include "baseinputoutput.h"
#include <stdexcept>

class BaseCalculation
{
public:
    virtual void addResultHolder(BaseInputOutput *result) = 0;
    virtual void initialize() = 0;
    virtual void calculate() = 0;
    virtual ~BaseCalculation();
    BaseCalculation();
};

#endif // BASECALCULATION_H
