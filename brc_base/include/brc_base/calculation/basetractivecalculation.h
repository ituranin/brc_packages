#ifndef BASETRACTIVECALCULATION_H
#define BASETRACTIVECALCULATION_H

#include "baseroscalculation.h"
#include "tractivesystemresult.h"
#include "boolinputoutput.h"
#include "brc_base/SensorData.h"
#include "brc_base/ControlLog.h"
#include "brc_base/WheelSpeed.h"
#include "brc_base/DashOne.h"
#include "brc_base/CorSysData1.h"
#include "../BrcHelpers.h"
#include "../settingsloader.h"

#include <std_msgs/Bool.h>
#include <std_msgs/Int16.h>
#include <iostream>

using namespace ros;

class BaseTractiveCalculationSettings {

  public:
  BaseTractiveCalculationSettings();

  //BSPD
  double BRAKE_BSPD_LIMIT;            //bar
  double APPS_BSPD_LIMIT;
  //SHIFTING
  double SHIFT_UP_REQUEST_TIMEOUT;    //s
  double SHIFT_DOWN_REQUEST_TIMEOUT;  //s

};

class BaseTractiveCalculation : public BaseRosCalculation
{
public:
    BaseTractiveCalculation();
    virtual ~BaseTractiveCalculation();
    virtual void initialize();
    virtual void calculate() = 0;
    virtual void addResultHolder(BaseInputOutput *result);

    BaseTractiveCalculationSettings settingsTractive;
protected:
    TractiveSystemResult *result = nullptr;
    BoolInputOutput *directorGo = nullptr;

    Publisher shiftPublisher;
    Publisher directorReadyPublisher;
    Publisher directorFinishedPublisher;
    Subscriber clutchSubscriber;
    Subscriber brakeSubscriber;
    Subscriber speedSubscriber;
    Subscriber dashOneSubscriber;
    Subscriber corrSysData1Subscriber;
    Subscriber steeringSubscriber;
    Time shiftCooldown;
    Time gearCooldown;

    bool brakeEngaged = true;
    uint8_t gear = 0;
    uint8_t gearCheck = 0;
    int16_t gearCheckCounter = 0;
    double brakePressureFront = 0;      //in bar
    double brakePressureRear = 0;       //in bar
    double clutchPressure = 0;          //in bar
    int16_t rpm = 0;
    double speed = 0;                   //in km/h
    double wheelSpeedFL = 0;            //in km/h
    double wheelSpeedFR = 0;            //in km/h
    double wheelSpeedRL = 0;            //in km/h
    double wheelSpeedRR = 0;            //in km/h
    double steeringTravel = 0;          //in mm
    bool frontWheelsStopped = true;
    bool rearWheelsStopped = true;
    bool allWheelsStopped = true;
    bool speedIsZero = true;
    uint16_t corrSysSpeed = 0;          //in km/h

    void sendUpshift();
    void sendDownshift();
    void setGearAndRpm(const brc_base::DashOne::ConstPtr& msg);
    void setGear(uint8_t value);
    void setRpm(int16_t value);
    void setBrakeEngaged(const brc_base::SensorData::ConstPtr& msg);
    void setCheckClutchPressure(const brc_base::SensorData::ConstPtr& msg);
    void setSpeed(const brc_base::WheelSpeed::ConstPtr& msg);
    void setCorrSysSpeed(const brc_base::CorSysData1::ConstPtr& msg);
    void setSteering(const brc_base::SensorData::ConstPtr& msg);

    void setResultApps(double percentage);
    void setResultBrakeFront(double pressure);
    void setResultBrakeRear(double pressure);

    void setReady();
    void setFinished();
};

#endif // BASETRACTIVECALCULATION_H
