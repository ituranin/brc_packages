#ifndef BOOLRESULT_H
#define BOOLRESULT_H

#include "baseinputoutput.h"

class BoolInputOutput : public BaseInputOutput
{
public:
    BoolInputOutput();
    virtual ~BoolInputOutput();
    bool value = false;

    virtual string getType();
};

#endif // BOOLRESULT_H
