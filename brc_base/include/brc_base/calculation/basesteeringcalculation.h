#ifndef BASESTEERINGCALCULATION_H
#define BASESTEERINGCALCULATION_H

#include "baseroscalculation.h"
#include "int32inputoutput.h"
#include "brc_base/WheelSpeed.h"
#include "brc_base/SensorData.h"
#include <std_msgs/Bool.h>

class BaseSteeringCalculation : public BaseRosCalculation
{
public:
    BaseSteeringCalculation();
    virtual ~BaseSteeringCalculation();
    virtual void initialize();
    virtual void calculate() = 0;
    virtual void addResultHolder(BaseInputOutput *result) = 0;
protected:
    Publisher directorReadyPublisher;
    Subscriber speedSubscriber;
    Subscriber steeringSubscriber;
    Int32InputOutput *result = nullptr;
    double speed = 0;
    int16_t currentSteeringPosition = 0;

    void setReady();
    void setSpeed(const brc_base::WheelSpeed::ConstPtr& msg);
    void setSteering(const brc_base::SensorData::ConstPtr& msg);
};

#endif // BASESTEERINGCALCULATION_H
