#ifndef INT32RESULT_H
#define INT32RESULT_H

#include "baseinputoutput.h"

class Int32InputOutput : public BaseInputOutput
{
public:
    Int32InputOutput();
    virtual ~Int32InputOutput();
    int32_t value = 0;

    virtual string getType();
};

#endif // INT32RESULT_H
