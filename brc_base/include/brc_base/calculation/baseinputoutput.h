#ifndef BASERESULT_H
#define BASERESULT_H

#include <iostream>
using namespace std;

class BaseInputOutput
{
public:
    BaseInputOutput();
    virtual ~BaseInputOutput();

    virtual string getType() = 0;

    void setDescription(string description);
    string getDescription();
private:
    string description = "";
};

#endif // BASERESULT_H
