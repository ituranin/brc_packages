#ifndef FLOATRESULT_H
#define FLOATRESULT_H

#include "baseinputoutput.h"

class FloatInputOutput : public BaseInputOutput
{
public:
    FloatInputOutput();
    virtual ~FloatInputOutput();
    float value = 0;

    virtual string getType();
};

#endif // FLOATRESULT_H
