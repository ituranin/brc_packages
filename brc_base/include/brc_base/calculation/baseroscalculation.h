#ifndef BASEROSCALCULATION_H
#define BASEROSCALCULATION_H

#include "basecalculation.h"
#include <ros/ros.h>

using namespace ros;

class BaseRosCalculation : public BaseCalculation
{
public:
    BaseRosCalculation();
    virtual ~BaseRosCalculation();
    virtual void initialize();
    virtual void calculate() = 0;
    virtual void addResultHolder(BaseInputOutput *result) = 0;
    void setNodeHandle(NodeHandle *handle);
    void setRate(int rate);
protected:
    NodeHandle *nodeHandle = nullptr;
    Rate *rosRate = nullptr;
    int rate = -1;
};

#endif // BASEROSCALCULATION_H
