#ifndef VIDEOTHREAD_H
#define VIDEOTHREAD_H

#include "camerathread.h"
#include <chrono>

using namespace std;
using namespace cv;

class VideoThread : public CameraThread
{
public:
    VideoThread();
    ~VideoThread();

    void initCamera(string path);
private:

    Mat frame;
    string path;
    std::chrono::system_clock::time_point start;
    std::chrono::system_clock::time_point end;
    unique_ptr<VideoCapture> video;
    void process();
};

#endif // VIDEOTHREAD_H
