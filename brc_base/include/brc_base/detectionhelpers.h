#ifndef DETECTIONHELPERS_H
#define DETECTIONHELPERS_H

#define CAMERA_CAPTURE_WIDTH    2048
#define CAMERA_CAPTURE_HEIGHT    412
#define CAMERA_CAPTURE_Y_OFFSET  380

#define DETECTION_INDEX_X 0
#define DETECTION_INDEX_Y 1
#define DETECTION_INDEX_Z 2
#define DETECTION_INDEX_CLASS 3
#define DETECTION_INDEX_SCORE 4
#define DETECTION_CLOUD_FIELD_NUMBER 5
#define DETECTION_CLOUD_WIDTH DETECTION_CLOUD_FIELD_NUMBER
#define DETECTION_CLOUD_HEIGHT 1
#define DETECTION_CLOUD_FIRST_FIELD "x"

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include "cone.h"

namespace detectionHelper
{

/**
 * @brief prepareDetectionCloud TODO
 * @param cloud
 * @param size
 */
inline void prepareDetectionCloud(sensor_msgs::PointCloud2 &cloud, size_t size)
{
    // TODO
    cloud.height = DETECTION_CLOUD_HEIGHT;
    cloud.width = DETECTION_CLOUD_WIDTH;
    cloud.header.frame_id = "ConePosition";

    sensor_msgs::PointCloud2Modifier modifier(cloud);
    modifier.setPointCloud2Fields(DETECTION_CLOUD_FIELD_NUMBER,
                                  "x", 1, sensor_msgs::PointField::FLOAT32,
                                  "y", 1, sensor_msgs::PointField::FLOAT32,
                                  "z", 1, sensor_msgs::PointField::FLOAT32,
                                  "class", 1, sensor_msgs::PointField::FLOAT32,
                                  "score", 1, sensor_msgs::PointField::FLOAT32);
    modifier.resize(size);
}

/**
 * @brief addDetection TODO
 * @param iter
 * @param x
 * @param y
 * @param z
 * @param score
 * @param classPropability
 */
inline void addDetection(sensor_msgs::PointCloud2Iterator<float> &iter, float &x, float &y, float &z, float &score, float &type)
{
    // TODO
    iter[DETECTION_INDEX_X] = x;
    iter[DETECTION_INDEX_Y] = y;
    iter[DETECTION_INDEX_Z] = z;
    iter[DETECTION_INDEX_CLASS] = type;
    iter[DETECTION_INDEX_SCORE] = score;
}

/**
 * @brief readDetectionsToVector
 * @param cloud
 * @param detections
 */
inline void readDetectionsToVector(const sensor_msgs::PointCloud2::ConstPtr &cloud, std::vector<Cone> &detections)
{
    // TODO
    detections.clear();

    for (sensor_msgs::PointCloud2ConstIterator<float> iter(*cloud, DETECTION_CLOUD_FIRST_FIELD); iter != iter.end(); ++iter) {
        Cone cone;
        cone.x = iter[DETECTION_INDEX_X];
        cone.y = iter[DETECTION_INDEX_Y];
        cone.z = iter[DETECTION_INDEX_Z];
        cone.type = iter[DETECTION_INDEX_CLASS];
        cone.score = iter[DETECTION_INDEX_SCORE];
        detections.push_back(cone);
    }
}

}

#endif // DETECTIONHELPERS_H
