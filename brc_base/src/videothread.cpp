#include "videothread.h"

VideoThread::VideoThread()
{

}

VideoThread::~VideoThread()
{

}

void VideoThread::initCamera(string path)
{
    this->path = path;
    video = unique_ptr<VideoCapture>(new VideoCapture(this->path));
    start = std::chrono::system_clock::now();
    end = std::chrono::system_clock::now();
    initialized = true;
}

void VideoThread::process()
{
    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end-start;

    if(elapsed_seconds.count() > 0.03) {
        start = std::chrono::system_clock::now();
        *(video) >> frame;
    }

    if(frame.empty()) {
        video.release();
        video = unique_ptr<VideoCapture>(new VideoCapture(this->path));
    } else {
        setFrame(make_shared<Mat>(frame));
    }
}
