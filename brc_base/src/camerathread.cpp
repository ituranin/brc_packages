#include "camerathread.h"

CameraThread::CameraThread()
{
    frame = make_shared<Mat>(300, 300, CV_8UC3);
}

CameraThread::~CameraThread()
{

}

void CameraThread::startCapture()
{
    if(!initialized) {
        throw std::invalid_argument("Camera not initialized");
    }

    start();
}

shared_ptr<Mat> CameraThread::getFrame()
{
    lock_guard<mutex> guard(frameMutex);
    return frame;
}

void CameraThread::setFrame(shared_ptr<Mat> image)
{
    lock_guard<mutex> guard(frameMutex);
    frame = image;

    if(callback != nullptr)
    {
        callback(frame);
    }
}

void CameraThread::setCallback(const CameraCallbackFunction &value)
{
    callback = value;
}
