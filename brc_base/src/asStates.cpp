/*
 * asStates.c
 *
 */ 

 #include "asStates.h"

 /*
 * writes the asStates into a buffer which can be sent by CAN (as data field buffer)
 * length should be 8
 */
 void convertASStatesToASStateMsgBuffer(asStates* states, uint8_t* buffer, uint8_t length) {
	if(length < 8)
		return;

	uint32_t data1;
	uint16_t data2;

	data1 = ((states->cpuState & 0x3F) << 26 ) | ((states->vcState & 0x3F) << 20 ) | ((states->ebsState & 0x3F) << 14 ) | ((states->sbState & 0x3F) << 8 ) |
			((states->sacState & 0x3F) << 2 ) | ((states->cacState & 0x3F) >> 4 );
			
	data2 = ((states->cacState & 0x0F) << 12 ) | ((states->etcState & 0x3F) << 6 ) | ((states->hscState & 0x3F) << 0 );

	//first three bits represent real AS State, the coming 5 bits are reserved for the substate
	buffer[0] = (uint8_t)(states->asState);
	buffer[1] = (uint8_t)(data1>>24);
	buffer[2] = (uint8_t)(data1>>16);
	buffer[3] = (uint8_t)(data1>>8);
	buffer[4] = (uint8_t)(data1);
	buffer[5] = (uint8_t)(data2>>8);
	buffer[6] = (uint8_t)(data2);
	
	buffer[7] = (uint8_t)((states->amiState & 0x3F) << 2);	//reserved, not yet used
 }

 /*
 * converts the asStates from a buffer (e.g. CAN message data buffer) to the asState type
 * length should be 8
 */
 void convertASStateMsgBufferToASStates(asStates* states, uint8_t* buffer, uint8_t length) {
	if(length < 8)
		return;

	uint32_t temp1 = 0;
	temp1 |= (buffer[0] << 24);
	temp1 |= (buffer[1] << 16);
	temp1 |= (buffer[2] << 8);
	temp1 |= (buffer[3]);
	
	uint32_t temp2 = 0;
	temp2 |= (buffer[4] << 24);
	temp2 |= (buffer[5] << 16);
	temp2 |= (buffer[6] << 8);
	temp2 |= (buffer[7]);
		
	states->amiState = (0x3F & temp2>>2);
	states->hscState = (0x3F & temp2>>8);
	states->etcState = (0x3F & temp2>>14);
	states->cacState = (0x3F & temp2>>20);
	states->sacState = (0x3F & temp2>>26);
	
	states->sbState	 = (0x3F & temp1>>0);
	states->ebsState = (0x3F & temp1>>6);
	states->vcState  = (0x3F & temp1>>12);
	states->cpuState = (0x3F & temp1>>18);
	
	states->asState = buffer[0];
 }


 
/*
*	returns main asState (from FS-rules)
*	example call: getState(asStates.asState)
*/
uint8_t getASState(uint8_t asState) {
	return (0b11100000 & asState)>>5;
}

/*
*	returns sub asState (from FS-rules)
*	example call: getASSubstate(asStates.asState)
*/
uint8_t getASSubstate(uint8_t asState) {
	return (0b11111 & asState);
}


/*
 *	returns asState generated from rules state and substate
 *	example usage: asStates.asState = getFullASState(off, 0);
 */
 uint8_t getFullASState(uint8_t state, uint8_t substate)  {
	 return ((0b111 & state)<<5) | (0b11111 & substate);
 }

 //////////////////////////////////////////////////////////////////////////

 /*
 *	returns main state (from FS-rules) from control state
 *	example call: getState(asStates.sacState)
 */
 uint8_t getCSState(uint8_t csState) {
	return (0b110000 & csState)>>4;
 }

 /*
 *	returns substate (from FS-rules) from control state
 *	example call: getState(asStates.sacState)
 */
 uint8_t getCSSubstate(uint8_t csState) {
	return (0b1111 & csState);
 }

 /*
 *	returns csState generated from state and substate
 *	example usage: asStates.sacState = getFullCSState(off, 0);
 */
 uint8_t getFullCSState(uint8_t state, uint8_t substate) {
	return ((state & 0b11)<<4) | (substate & 0b1111);
 }
