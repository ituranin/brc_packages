#include "detectionthread.h"

DetectionThread::DetectionThread()
{
    frame = make_shared<Mat>(300, 300, CV_8UC3);
}

DetectionThread::~DetectionThread()
{

}

void DetectionThread::initDetection(CameraThread *cameraThread)
{
    if(initialized) {
        throw std::invalid_argument("do not initialize twice!");
    }

    camera = cameraThread;
    transform = new CameraTransform(2048, 1088, 963);
    transform->setPitch(10.5f);
}

void DetectionThread::startDetection()
{
    if(!initialized) {
        throw std::invalid_argument("Detection not initialized");
    }

    if(camera == nullptr) {
        throw std::invalid_argument("Camera not set for detection");
    }

    start();
}

CameraThread *DetectionThread::getCamera() const
{
    return camera;
}

shared_ptr<Mat> DetectionThread::getFrame()
{
    lock_guard<mutex> guard(frameMutex);
    return frame;
}

void DetectionThread::setFrame(shared_ptr<Mat> value)
{
    lock_guard<mutex> guard(frameMutex);
    frame = value;
}

cv::Point2f DetectionThread::pointFromCoords(Mat& image, float yMin, float xMin, float yMax, float xMax) {
    float xMinScaled = xMin * image.cols;
    float xMaxScaled = xMax * image.cols;
    float yMaxScaled = yMax * image.rows;
    float yMinScaled = yMin * image.rows;
    float xScaled = xMinScaled + ( (xMaxScaled - xMinScaled) / 2 );
    return cv::Point2f(xScaled, yMaxScaled);
}
