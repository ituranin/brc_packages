#include "controlserver.h"

uint16_t to16bitMine(uint8_t low, uint8_t high) {
  return low | static_cast<uint16_t>(high << 8);
}

ControlServer::ControlServer()
{
    socket = new boost::asio::ip::udp::socket(io_service, boost::asio::ip::udp::endpoint(boost::asio::ip::address_v4::broadcast(), 9000));
}

ControlServer::~ControlServer()
{
    socket->close();
    delete socket;
}

int16_t ControlServer::getSteeringvalue()
{
    lock_guard<mutex> lock(steeringMutex);
    return steeringvalue;
}

void ControlServer::setSteeringvalue(const int16_t &value)
{
    lock_guard<mutex> lock(steeringMutex);
    steeringvalue = value;
}

void ControlServer::process()
{
    std::cout << "running" << std::endl;
    boost::array<char, 128> recv_buf;
    boost::asio::ip::udp::endpoint remote_endpoint;
    boost::system::error_code error;

    size_t len = socket->receive_from(boost::asio::buffer(recv_buf), remote_endpoint, 0, error);
    int16_t value = static_cast<int16_t>(to16bitMine(recv_buf.data()[2], recv_buf.data()[1]));
    setSteeringvalue(value);
}
