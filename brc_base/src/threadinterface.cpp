#include "threadinterface.h"

ThreadInterface::ThreadInterface()
{
    running = false;
}

ThreadInterface::~ThreadInterface()
{

}

void ThreadInterface::start()
{
    running = true;
    thread internalThread(&ThreadInterface::run, this);
    internalThread.detach();
}

void ThreadInterface::stop()
{
    running = false;
}

void ThreadInterface::setFps(int fps)
{
    ms = 1000 / fps;
}

bool ThreadInterface::isRunning() const
{
    return running;
}

void ThreadInterface::run()
{
    while(running) {
        timeTracker.start();

        process();

        timeTracker.stop();
        auto passed = timeTracker.getDurationInMilliseconds();

        if (passed < ms)
        {
            this_thread::sleep_for(std::chrono::milliseconds(ms - passed));
        }
    }
}
