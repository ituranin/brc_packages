#include "settingsloader.h"

SettingsLoader* SettingsLoader::_instance = nullptr;

SettingsLoader *SettingsLoader::getInstance()
{
    if(nullptr == _instance)
    {
        _instance = new SettingsLoader();
    }

    return _instance;
}

Json::Value SettingsLoader::loadSettings(const string fileName)
{
    Json::Value loadedJson;
    std::ifstream param_file(this->settingsPath + fileName, std::ifstream::binary);
    param_file >> loadedJson;

    return loadedJson;
}

SettingsLoader::SettingsLoader()
{
    const string& user = getenv("USER");
    settingsPath = "/home/" + user + "/brc_packages/brc_settings/";
}

SettingsLoader::~SettingsLoader()
{

}

SettingsLoader::SettingsLoader(const SettingsLoader &)
{

}
