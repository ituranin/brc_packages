#include "basesteeringcalculation.h"

BaseSteeringCalculation::BaseSteeringCalculation()
{

}

BaseSteeringCalculation::~BaseSteeringCalculation()
{

}

void BaseSteeringCalculation::addResultHolder(BaseInputOutput *result)
{
    if(result->getDescription() == "steeringResult") {
        this->result = dynamic_cast<Int32InputOutput*>(result);
    }
}

void BaseSteeringCalculation::initialize()
{
    BaseRosCalculation::initialize();

    directorReadyPublisher = nodeHandle->advertise<std_msgs::Bool>("/director/steering/ready", 1);
    speedSubscriber  = nodeHandle->subscribe("/wheelSpeed", 1, &BaseSteeringCalculation::setSpeed, this);
    steeringSubscriber = nodeHandle->subscribe("/steering/sensorData1", 1, &BaseSteeringCalculation::setSteering, this);

    if (result == nullptr) {
        throw std::invalid_argument("result object must be set");
    }

    // TODO get back in in production
    while (this->directorReadyPublisher.getNumSubscribers() == 0);
}

void BaseSteeringCalculation::setReady() {
	std_msgs::Bool msg;
	msg.data = true;
	directorReadyPublisher.publish(msg);
}

void BaseSteeringCalculation::setSpeed(const brc_base::WheelSpeed::ConstPtr& msg) {
    this->speed = msg->frontLeft * 0.036;
}

void BaseSteeringCalculation::setSteering(const brc_base::SensorData::ConstPtr &msg)
{
    this->currentSteeringPosition = static_cast<int16_t>(msg->value2);
}
