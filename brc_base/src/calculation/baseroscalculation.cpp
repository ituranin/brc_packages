#include "baseroscalculation.h"

BaseRosCalculation::BaseRosCalculation()
{

}

BaseRosCalculation::~BaseRosCalculation()
{

}

void BaseRosCalculation::setNodeHandle(NodeHandle *handle)
{
    this->nodeHandle = handle;
}

void BaseRosCalculation::setRate(int rate)
{
    this->rate = rate;
    this->rosRate = new Rate(rate);
}

void BaseRosCalculation::initialize()
{
    if(this->rate == -1 || this->nodeHandle == nullptr) {
        throw std::invalid_argument("Rate or Nodehandle not set");
    }
}
