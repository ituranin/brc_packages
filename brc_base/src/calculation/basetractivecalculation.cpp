#include "basetractivecalculation.h"

BaseTractiveCalculation::BaseTractiveCalculation()
{

}

BaseTractiveCalculation::~BaseTractiveCalculation()
{

}

void BaseTractiveCalculation::addResultHolder(BaseInputOutput *result)
{
    if(result->getType() == "ts") {
        this->result = dynamic_cast<TractiveSystemResult*>(result);
    }

    if (result->getDescription() == "directorGo") {
        this->directorGo = dynamic_cast<BoolInputOutput*>(result);
    }
}

void BaseTractiveCalculation::initialize()
{
    BaseRosCalculation::initialize();

    directorReadyPublisher = nodeHandle->advertise<std_msgs::Bool>("/director/tractive/ready", 1);
    directorFinishedPublisher = nodeHandle->advertise<std_msgs::Bool>("/director/tractive/finished", 1);

    shiftPublisher = nodeHandle->advertise<std_msgs::Bool>("/shiftRequest", 1);
    clutchSubscriber = nodeHandle->subscribe("/clutch/sensorData1", 1, &BaseTractiveCalculation::setCheckClutchPressure, this);
    brakeSubscriber = nodeHandle->subscribe("/serviceBrake/sensorData1", 1, &BaseTractiveCalculation::setBrakeEngaged, this);
    speedSubscriber  = nodeHandle->subscribe("/wheelSpeed", 1, &BaseTractiveCalculation::setSpeed, this);
    dashOneSubscriber = nodeHandle->subscribe("dashOne", 1, &BaseTractiveCalculation::setGearAndRpm, this);
    corrSysData1Subscriber = nodeHandle->subscribe("corSys/DATA1", 1, &BaseTractiveCalculation::setCorrSysSpeed, this);
    steeringSubscriber = nodeHandle->subscribe("/steering/sensorData1", 1, &BaseTractiveCalculation::setSteering, this);

    if (result == nullptr) {
        throw std::invalid_argument("result object must be set");
    }

    if (directorGo == nullptr) {
        throw std::invalid_argument("BoolResult with description directorGo needed");
    }

    shiftCooldown = ros::Time::now();
    gearCooldown = ros::Time::now();

    while (this->directorReadyPublisher.getNumSubscribers() == 0);
}

void BaseTractiveCalculation::sendUpshift() {
    if ((ros::Time::now() - shiftCooldown).toSec() < settingsTractive.SHIFT_UP_REQUEST_TIMEOUT) {
        return;
    }

    std_msgs::Bool msg;
    msg.data = true;
    shiftPublisher.publish(msg);

    shiftCooldown = ros::Time::now();
}

void BaseTractiveCalculation::sendDownshift() {
    if ((ros::Time::now() - shiftCooldown).toSec() < settingsTractive.SHIFT_DOWN_REQUEST_TIMEOUT) {
        return;
    }

    std_msgs::Bool msg;
    msg.data = false;
    shiftPublisher.publish(msg);

    shiftCooldown = ros::Time::now();
}

void BaseTractiveCalculation::setGearAndRpm(const brc_base::DashOne::ConstPtr &msg)
{
    setGear(static_cast<uint8_t>(msg->gear));
    setRpm(msg->rpm);
}

void BaseTractiveCalculation::setReady() {
    std_msgs::Bool msg;
    msg.data = true;
    directorReadyPublisher.publish(msg);
}

void BaseTractiveCalculation::setFinished() {
    std_msgs::Bool msg;
    msg.data = true;
    directorFinishedPublisher.publish(msg);
}

void BaseTractiveCalculation::setGear(uint8_t value) {
    if ((ros::Time::now() - gearCooldown).toSec() < 0.2) {
        return;
    }

    this->gearCheck = value;

    if(this->gearCheck != this->gear)
    {
        this->gear = this->gearCheck;
    }

    gearCooldown = ros::Time::now();
}

void BaseTractiveCalculation::setBrakeEngaged(const brc_base::SensorData::ConstPtr& msg) {
    this->brakePressureFront = getBrakePressureInput(static_cast<int16_t>(msg->value1));
    this->brakePressureRear = getBrakePressureInput(static_cast<int16_t>(msg->value2));

    if(this->brakePressureFront < 3. && this->brakePressureRear < 3.) {
        this->brakeEngaged = false;
    } else {
        this->brakeEngaged = true;
    }
}

void BaseTractiveCalculation::setCheckClutchPressure(const brc_base::SensorData::ConstPtr& msg) {
    this->clutchPressure = getClutchPressureInput(static_cast<int16_t>(msg->value1));
}

void BaseTractiveCalculation::setRpm(int16_t value) {
    this->rpm = value;
}

void BaseTractiveCalculation::setSpeed(const brc_base::WheelSpeed::ConstPtr& msg) {
    this->wheelSpeedFL = msg->frontLeft * 0.036;
    this->wheelSpeedFR = msg->frontRight * 0.036;
    this->wheelSpeedRL = msg->rearLeft * 0.036;
    this->wheelSpeedRR = msg->rearRight * 0.036;

    //calculate vehicle speed by front wheels: check for sensor errors (e.g. one sensor is 0)
    if((this->wheelSpeedFL > 0.01 && this->wheelSpeedFR < 0.01)
       || (this->wheelSpeedFL < 0.01 && this->wheelSpeedFR > 0.01))
    {
        this->speed = max(this->wheelSpeedFL, this->wheelSpeedFR);
    }
    else
    {
        //mittelwert vorne
        this->speed = (this->wheelSpeedFL + this->wheelSpeedFR) / 2;
    }

    if(this->wheelSpeedFL + this->wheelSpeedFR == 0.) {
        this->frontWheelsStopped = true;
    } else {
        this->frontWheelsStopped = false;
    }

    if(this->wheelSpeedRL + this->wheelSpeedRR == 0.) {
        this->rearWheelsStopped = true;
    } else {
        this->rearWheelsStopped = false;
    }

    if(this->frontWheelsStopped && this->rearWheelsStopped) {
        this->allWheelsStopped = true;
    } else {
        this->allWheelsStopped = false;
    }

    if(this->allWheelsStopped && this->corrSysSpeed == 0) {
        this->speedIsZero = true;
    } else {
        this->speedIsZero = false;
    }
}

void BaseTractiveCalculation::setCorrSysSpeed(const brc_base::CorSysData1::ConstPtr &msg)
{
    this->corrSysSpeed = msg->v;
}

void BaseTractiveCalculation::setSteering(const brc_base::SensorData::ConstPtr &msg)
{
    this->steeringTravel = static_cast<int16_t>(msg->value2) * 0.01;
}



void BaseTractiveCalculation::setResultApps(double percentage) {
    if(this->brakePressureFront >= settingsTractive.BRAKE_BSPD_LIMIT)
    {
        percentage = settingsTractive.APPS_BSPD_LIMIT;
    }

    this->result->apps = getAppsOutput(percentage);
}

void BaseTractiveCalculation::setResultBrakeFront(double pressure) {
    //if(pressure > 0)
    //{
    //    this->result->apps = getAppsOutput(settingsTractive.APPS_BSPD_LIMIT);
    //}

    this->result->brakeFront = getBrakePressureOutput(pressure);
}

void BaseTractiveCalculation::setResultBrakeRear(double pressure) {
    //if(pressure > 0)
    //{
    //    this->result->apps = getAppsOutput(settingsTractive.APPS_BSPD_LIMIT);
    //}

    this->result->brakeRear = getBrakePressureOutput(pressure);
}

BaseTractiveCalculationSettings::BaseTractiveCalculationSettings()
{
    Json::Value settings = SettingsLoader::getInstance()->loadSettings("tractiveSettings.json");

    //load shift values
    this->SHIFT_UP_REQUEST_TIMEOUT    = settings["shifting"]["upshiftTimeout"].asDouble();
    this->SHIFT_DOWN_REQUEST_TIMEOUT  = settings["shifting"]["downshiftTimeout"].asDouble();

    //load brake values
    this->BRAKE_BSPD_LIMIT            = settings["bspd"]["brakeBspdLimit"].asDouble();
    this->APPS_BSPD_LIMIT             = settings["bspd"]["appsBspdLimit"].asDouble();

    cout << "Tractive Settings" << endl;
    cout << this->SHIFT_UP_REQUEST_TIMEOUT << endl;
    cout << this->SHIFT_DOWN_REQUEST_TIMEOUT << endl;

    cout << this->BRAKE_BSPD_LIMIT << endl;
    cout << this->APPS_BSPD_LIMIT << endl;
}
