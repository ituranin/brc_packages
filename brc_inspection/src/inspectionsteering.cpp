#include "brc_inspection/inspectionsteering.h"

InspectionSteering::InspectionSteering() {}

InspectionSteering::~InspectionSteering() {}

void InspectionSteering::addResultHolder(BaseInputOutput *result)
{
    BaseSteeringCalculation::addResultHolder(result);
}

void InspectionSteering::initialize()
{
    BaseSteeringCalculation::initialize();

    this->stepSize = (360.0 / this->rate) / (this->timeInS / 2.0);
    this->result->value = 0;

    this->setReady();
}

void InspectionSteering::calculate()
{
    this->angle = this->angle + stepSize;
    this->result->value = static_cast<int>(1600 * sin(angle * M_PI / 180));
}
