#include "brc_inspection/teststeering.h"

TestSteering::TestSteering() {}

TestSteering::~TestSteering() {}

void TestSteering::addResultHolder(BaseInputOutput *result)
{
    BaseSteeringCalculation::addResultHolder(result);
}

void TestSteering::initialize()
{
    BaseSteeringCalculation::initialize();

    server = new ControlServer();
    server->start();

    this->result->value = 0;

    this->setReady();
}

void TestSteering::calculate()
{
    this->result->value = server->getSteeringvalue();
}
