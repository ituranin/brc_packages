#include "brc_inspection/inspectiontracker.h"

InspectionTracker::InspectionTracker() {}

InspectionTracker::~InspectionTracker() {}

void InspectionTracker::addResultHolder(BaseInputOutput *result)
{
    if(result->getDescription() == "missionFinished") {
        this->missionFinished = dynamic_cast<BoolInputOutput*>(result);
    }

    if(result->getDescription() == "missionShouldFinish") {
        this->missionShouldFinish = dynamic_cast<BoolInputOutput*>(result);
    }
}

void InspectionTracker::initialize()
{
    BaseRosCalculation::initialize();

    steeringFinishedSubscriber = nodeHandle->subscribe("/director/steering/finished", 1, &InspectionTracker::setSteeringFinished, this);
    tractiveFinishedSubscriber = nodeHandle->subscribe("/director/tractive/finished", 1, &InspectionTracker::setTractiveFinished, this);

    if (missionFinished == nullptr || missionShouldFinish == nullptr) {
        throw std::invalid_argument("missionFinished or missionShouldFinish not set");
    }
}

void InspectionTracker::calculate()
{
    if (startTimeSet == false) {
        startTime = ros::Time::now();
        startTimeSet = true;
    }

    if ((ros::Time::now() - startTime).toSec() >= 20.0) {
        missionShouldFinish->value = true;
    } else {
        return;
    }

    if (tractiveFinished) {
        missionFinished->value = true;
    }
}

void InspectionTracker::setSteeringFinished(const std_msgs::Bool::ConstPtr& msg)
{
    this->steeringFinished = msg->data;
}

void InspectionTracker::setTractiveFinished(const std_msgs::Bool::ConstPtr& msg)
{
    this->tractiveFinished = msg->data;
}
