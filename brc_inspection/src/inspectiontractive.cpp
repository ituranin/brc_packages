#include "brc_inspection/inspectiontractive.h"

InspectionTractive::InspectionTractive() {}

InspectionTractive::~InspectionTractive() {}

void InspectionTractive::addResultHolder(BaseInputOutput *result)
{
    BaseTractiveCalculation::addResultHolder(result);
}

void InspectionTractive::initialize()
{
    BaseTractiveCalculation::initialize();

    this->setReady();
}

void InspectionTractive::calculate()
{
    if(this->directorGo->value == false) {
        this->result->clutch = getClutchPressureOutput(20.0);

        if (this->clutchPressure >= 18.0) {
            this->result->brakeFront = getBrakePressureOutput(20.0);
            this->result->brakeRear = getBrakePressureOutput(20.0);
            this->result->apps = getAppsOutput(0.0);
            sendDownshift();
        }

        if(this->brakeEngaged) {
            this->setFinished();
        }

        return;
    }

    if(this->brakeEngaged) {
        this->result->clutch = getClutchPressureOutput(20.0);
        this->clutchStartTimeSet = false;
    }

    if(this->clutchPressure >= 18.0 && this->gear == 0) {
        sendUpshift();
        this->result->brakeFront = getBrakePressureOutput(0.0);
        this->result->brakeRear = getBrakePressureOutput(0.0);
        this->result->apps = static_cast<int16_t>(getAppsPosition(0.07));
        return;
    }

    if(this->clutchPressure >= 18.0 && this->gear == 1) {

        if(this->clutchStartTimeSet == false) {
            this->clutchStartTime = ros::Time::now();
            this->clutchStartTimeSet = true;
        }

        // wait 2 seconds
        if( (ros::Time::now() - this->clutchStartTime).toSec() >= 2.0 && !this->brakeEngaged) {
            this->result->clutch = getClutchPressureOutput(0.0);
        }
    }

    if(this->clutchPressure <= 3.0) {
        this->result->apps = static_cast<int16_t>(getAppsPosition(0.07));
    }
}
