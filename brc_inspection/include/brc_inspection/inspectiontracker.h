#ifndef INSPECTIONTRACKER_H
#define INSPECTIONTRACKER_H

#include <brc_base/calculation/baseroscalculation.h>
#include <brc_base/calculation/boolinputoutput.h>
#include <ros/ros.h>
#include <std_msgs/Bool.h>

using namespace ros;

class InspectionTracker : public BaseRosCalculation
{
public:
    InspectionTracker();
    virtual ~InspectionTracker();
    virtual void initialize();
    virtual void calculate();
    virtual void addResultHolder(BaseInputOutput *result);
private:

    Subscriber steeringFinishedSubscriber;
    Subscriber tractiveFinishedSubscriber;

    Time startTime;

    bool startTimeSet = false;
    bool steeringFinished;
    bool tractiveFinished;

    BoolInputOutput *missionFinished = nullptr;
    BoolInputOutput *missionShouldFinish = nullptr;

    void setSteeringFinished(const std_msgs::Bool::ConstPtr& msg);
    void setTractiveFinished(const std_msgs::Bool::ConstPtr& msg);
};

#endif // INSPECTIONTRACKER_H
