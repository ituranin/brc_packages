#ifndef TESTSTEERING_H
#define TESTSTEERING_H

#include <brc_base/calculation/basesteeringcalculation.h>
#include <brc_base/controlserver.h>

class TestSteering : public BaseSteeringCalculation
{
public:
    TestSteering();
    virtual ~TestSteering();
    virtual void initialize();
    virtual void calculate();
    virtual void addResultHolder(BaseInputOutput *result);
    ControlServer* server = nullptr;
};

#endif // TESTSTEERING_H
