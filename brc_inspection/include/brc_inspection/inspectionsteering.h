#ifndef INSPECTIONSTEERING_H
#define INSPECTIONSTEERING_H

#include <brc_base/calculation/basesteeringcalculation.h>
#include <cmath>

class InspectionSteering : public BaseSteeringCalculation
{
public:
    InspectionSteering();
    virtual ~InspectionSteering();
    virtual void initialize();
    virtual void calculate();
    virtual void addResultHolder(BaseInputOutput *result);
private:
    double stepSize = 0.0;
    double angle = 0.0;
    const int32_t timeInS = 20;
};

#endif // INSPECTIONSTEERING_H
