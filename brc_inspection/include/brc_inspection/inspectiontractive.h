#ifndef INSPECTIONTRACKTIVE_H
#define INSPECTIONTRACKTIVE_H

#include <brc_base/calculation/basetractivecalculation.h>
#include <iostream>

class InspectionTractive : public BaseTractiveCalculation
{
public:
    InspectionTractive();
    virtual ~InspectionTractive();
    virtual void initialize();
    virtual void calculate();
    virtual void addResultHolder(BaseInputOutput* result);
private:
    ros::Time clutchStartTime;
    bool clutchStartTimeSet = false;
};

#endif // INSPECTIONTRACKTIVE_H
