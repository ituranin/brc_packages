#ifndef TENSORFLOWSSDMOBILENETTHREAD_H
#define TENSORFLOWSSDMOBILENETTHREAD_H

#include <brc_base/detectionthread.h>
#include <brc_base/detectionhelpers.h>
#include <ros/ros.h>

#include <vector>

#include "tensorflow/cc/ops/const_op.h"
#include "tensorflow/cc/ops/image_ops.h"
#include "tensorflow/cc/ops/standard_ops.h"
#include "tensorflow/core/framework/graph.pb.h"
#include "tensorflow/core/graph/default_device.h"
#include "tensorflow/core/graph/graph_def_builder.h"
#include "tensorflow/core/lib/core/threadpool.h"
#include "tensorflow/core/lib/io/path.h"
#include "tensorflow/core/lib/strings/stringprintf.h"
#include "tensorflow/core/platform/init_main.h"
#include "tensorflow/core/public/session.h"
#include "tensorflow/core/util/command_line_flags.h"

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <regex>

using tensorflow::Flag;
using tensorflow::Tensor;
using tensorflow::Status;
using tensorflow::string;
using tensorflow::int32;

using namespace std;
using namespace cv;

#define TENSOR_WIDTH  300
#define TENSOR_HEIGHT 300

class TensorflowSSDMobilenetThread : public DetectionThread
{
public:
    TensorflowSSDMobilenetThread();
    ~TensorflowSSDMobilenetThread();

    void initDetection(ros::NodeHandle *nodeHandle, CameraThread *cameraThread);

private:
    // TODO load path from JSON or launch file
    string ROOTDIR = "/home/" + string(getenv("USER")) + "/catkin_ws/brc_settings";
    string LABELS = "labels_map.pbtxt";
    string GRAPH = "frozen_inference_graph.pb";

    // Set input & output nodes names
    string inputLayer = "image_tensor:0";
    vector<string> outputLayer = {"detection_boxes:0", "detection_scores:0", "detection_classes:0", "num_detections:0"};

    // set options
    tensorflow::SessionOptions session_options;

    std::unique_ptr<tensorflow::Session> session;

    std::map<int, std::string> labelsMap;

    Tensor tensor;
    std::vector<Tensor> outputs;
    double thresholdScore = 0.9; // 0.7
    double thresholdIOU = 0.9; // 0.8

    tensorflow::TensorShape shape;

    std::shared_ptr<Mat> frame;
    Mat resized;

    ros::Publisher detectionPub;

    void process();
};

#endif // TENSORFLOWSSDMOBILENETTHREAD_H
