#include <cstdlib>
#include <ros/ros.h>
#include <brc_matrixvision/bluefox3capture.h>
//#include <brc_baumer/baumergapicapture.h>
#include "tensorflowssdmobilenetthread.h"
#include <opencv2/opencv.hpp>
#include <brc_base/videothread.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

using namespace std;
using namespace cv;

shared_ptr<image_transport::ImageTransport> it;
image_transport::Publisher ip;

void captureCallback(shared_ptr<Mat> image)
{
    cv_bridge::CvImage bImage;
    bImage.header.stamp = ros::Time::now();
    bImage.image = image->clone();
    bImage.encoding = "bgr8";
    ip.publish(bImage.toImageMsg());
}

int main(int argc, char *argv[]){

    ros::init(argc, argv, "mobilenet");
    ros::NodeHandle n;

    it = make_shared<image_transport::ImageTransport>(n);
    ip = it->advertise("/camera/ImageTransport", 1);

    std::string user = std::getenv("USER");

    Bluefox3Capture camera;
    camera.setFps(30);
    camera.setSerial("-");
    camera.initCamera("/home/" + user + "/catkin_ws/brc_settings/bluefoxRGB8_flat2048_new.xml");
    camera.setCallback(&captureCallback);
    camera.startCapture();

    /*VideoThread camera;
    camera.initCamera("/home/" + user + "/catkin_ws/brc_settings/test_videos/20180804_185449_1533401689_489076496_cropped.m4v");
    camera.setCallback(&captureCallback);
    camera.startCapture();*/

    TensorflowSSDMobilenetThread detection;
    detection.initDetection(&n, &camera);
    detection.startDetection();

    //namedWindow("test", WINDOW_NORMAL & WINDOW_OPENGL);

    while(ros::ok()) {
        //imshow("test", *(detection.getFrame()));
        //if(waitKey(30)%256 == 27 ) break;
    }

    ros::shutdown();

    return 0;

}
