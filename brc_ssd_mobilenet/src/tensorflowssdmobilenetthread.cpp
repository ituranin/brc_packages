#include "tensorflowssdmobilenetthread.h"

Status readLabelsMapFile(const string &fileName, std::map<int, string> &labelsMap);

Status loadGraph(const string &graph_file_name,
                 std::unique_ptr<tensorflow::Session> *session);

std::vector<size_t> filterBoxes(tensorflow::TTypes<float>::Flat &scores,
                                tensorflow::TTypes<float, 3>::Tensor &boxes,
                                double thresholdIOU, double thresholdScore);

void drawBoundingBoxOnImage(Mat &image,
                            double yMin,
                            double xMin,
                            double yMax,
                            double xMax,
                            double score,
                            string label,
                            bool scaled=true);

void drawBoundingBoxesOnImage(Mat &image,
                              tensorflow::TTypes<float>::Flat &scores,
                              tensorflow::TTypes<float>::Flat &classes,
                              tensorflow::TTypes<float,3>::Tensor &boxes,
                              map<int, string> &labelsMap,
                              vector<size_t> &idxs);


TensorflowSSDMobilenetThread::TensorflowSSDMobilenetThread()
{

}

TensorflowSSDMobilenetThread::~TensorflowSSDMobilenetThread()
{

}

void TensorflowSSDMobilenetThread::initDetection(ros::NodeHandle *nodeHandle, CameraThread *cameraThread)
{
    DetectionThread::initDetection(cameraThread);

    detectionPub = nodeHandle->advertise<sensor_msgs::PointCloud2>("/camera/ConePosition", 1);

    session_options.config.mutable_gpu_options()->set_allow_growth(false);
    session_options.config.mutable_gpu_options()->set_per_process_gpu_memory_fraction(0.5); // 0.05

    session = std::unique_ptr<tensorflow::Session>(tensorflow::NewSession(session_options));

    string graphPath = tensorflow::io::JoinPath(ROOTDIR, GRAPH);
    LOG(INFO) << "graphPath:" << graphPath;
    Status loadGraphStatus = loadGraph(graphPath, &session);
    if (!loadGraphStatus.ok()) {
            LOG(ERROR) << "loadGraph(): ERROR" << loadGraphStatus;
    } else
            LOG(INFO) << "loadGraph(): frozen graph loaded" << endl;

    labelsMap = std::map<int,std::string>();

    Status readLabelsMapStatus = readLabelsMapFile(tensorflow::io::JoinPath(ROOTDIR, LABELS), labelsMap);
    if (!readLabelsMapStatus.ok()) {
            LOG(ERROR) << "readLabelsMapFile(): ERROR" << loadGraphStatus;
    } else
            LOG(INFO) << "readLabelsMapFile(): labels map loaded with " << labelsMap.size() << " label(s)" << endl;

    shape = tensorflow::TensorShape();
    shape.AddDim(1);
    shape.AddDim(TENSOR_HEIGHT);
    shape.AddDim(TENSOR_WIDTH);
    shape.AddDim(3);

    initialized = true;
}

void TensorflowSSDMobilenetThread::process()
{
    frame = this->getCamera()->getFrame();
    ros::Time tempTime = ros::Time::now();

    // resize
    resize(*(frame), resized, Size(300,300));
    cvtColor(resized, resized, COLOR_BGR2RGB);

    // covert Mat to Tensor
    tensor = Tensor(tensorflow::DT_UINT8, shape);
    uint8_t *p = tensor.flat<tensorflow::uint8>().data();
    Mat fake(resized.rows, resized.cols, CV_8UC3, p);
    resized.convertTo(fake, CV_8UC3);

    // Run the graph on tensor
    outputs.clear();
    Status runStatus = session->Run({{inputLayer, tensor}}, outputLayer, {}, &outputs);
    if (!runStatus.ok()) {
        LOG(ERROR) << "Running model failed: " << runStatus;
    }

    // Extract results from the outputs vector
    tensorflow::TTypes<float>::Flat scores = outputs[1].flat<float>();
    tensorflow::TTypes<float>::Flat classes = outputs[2].flat<float>();
    tensorflow::TTypes<float>::Flat numDetections = outputs[3].flat<float>();
    tensorflow::TTypes<float, 3>::Tensor boxes = outputs[0].flat_outer_dims<float,3>();

    vector<size_t> goodIdxs = filterBoxes(scores, boxes, thresholdIOU, thresholdScore);

    sensor_msgs::PointCloud2 cloud;
    cloud.header.stamp = tempTime;

    detectionHelper::prepareDetectionCloud(cloud, goodIdxs.size());

    sensor_msgs::PointCloud2Iterator<float> cloudItr(cloud, DETECTION_CLOUD_FIRST_FIELD);

    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;

    for (size_t i = 0; i < goodIdxs.size(); i++) {
        cv::Point2d cone = pointFromCoords(*(frame),
                                           boxes(0,goodIdxs.at(i),0), boxes(0,goodIdxs.at(i),1),
                                           boxes(0,goodIdxs.at(i),2), boxes(0,goodIdxs.at(i),3));
        cone.y += CAMERA_CAPTURE_Y_OFFSET;

        cv::Point2d coneWorld = transform->cam2world(cone);

        x = static_cast<float>(coneWorld.x) / 1000;
        y = static_cast<float>(-coneWorld.y) / 1000;

        detectionHelper::addDetection(cloudItr, x, y, z, scores(goodIdxs.at(i)), classes(goodIdxs.at(i)));
        ++cloudItr;
    }

    detectionPub.publish(cloud);

    //TODO
    //drawBoundingBoxesOnImage(*(frame), scores, classes, boxes, labelsMap, goodIdxs);
    //setFrame(frame);
}

/**
  Utils from:
  https://github.com/lysukhin/tensorflow-object-detection-cpp
  */

/** Read a model graph definition (xxx.pb) from disk, and creates a session object you can use to run it.
 */
Status loadGraph(const string &graph_file_name,
                 unique_ptr<tensorflow::Session> *session) {
    tensorflow::GraphDef graph_def;
    Status load_graph_status =
            ReadBinaryProto(tensorflow::Env::Default(), graph_file_name, &graph_def);
    if (!load_graph_status.ok()) {
        return tensorflow::errors::NotFound("Failed to load compute graph at '",
                                            graph_file_name, "'");
    }

    session->reset(tensorflow::NewSession(tensorflow::SessionOptions()));
    Status session_create_status = (*session)->Create(graph_def);
    if (!session_create_status.ok()) {
        return session_create_status;
    }
    return Status::OK();
}

/** Read a labels map file (xxx.pbtxt) from disk to translate class numbers into human-readable labels.
 */
Status readLabelsMapFile(const string &fileName, map<int, string> &labelsMap) {

    // Read file into a string
    ifstream t(fileName);
    if (t.bad())
        return tensorflow::errors::NotFound("Failed to load labels map at '", fileName, "'");
    stringstream buffer;
    buffer << t.rdbuf();
    string fileString = buffer.str();

    // Search entry patterns of type 'item { ... }' and parse each of them
    smatch matcherEntry;
    smatch matcherId;
    smatch matcherName;
    const regex reEntry("item \\{([\\S\\s]*?)\\}");
    const regex reId("[0-9]+");
    const regex reName("\'.+\'");
    string entry;

    auto stringBegin = sregex_iterator(fileString.begin(), fileString.end(), reEntry);
    auto stringEnd = sregex_iterator();

    int id;
    string name;
    for (sregex_iterator i = stringBegin; i != stringEnd; i++) {
        matcherEntry = *i;
        entry = matcherEntry.str();
        regex_search(entry, matcherId, reId);
        if (!matcherId.empty())
            id = stoi(matcherId[0].str());
        else
            continue;
        regex_search(entry, matcherName, reName);
        if (!matcherName.empty())
            name = matcherName[0].str().substr(1, matcherName[0].str().length() - 2);
        else
            continue;
        labelsMap.insert(pair<int, string>(id, name));
    }
    return Status::OK();
}

/** Calculate intersection-over-union (IOU) for two given bbox Rects.
 */
double IOU(Rect2f box1, Rect2f box2) {

    float xA = max(box1.tl().x, box2.tl().x);
    float yA = max(box1.tl().y, box2.tl().y);
    float xB = min(box1.br().x, box2.br().x);
    float yB = min(box1.br().y, box2.br().y);

    float intersectArea = abs((xB - xA) * (yB - yA));
    float unionArea = abs(box1.area()) + abs(box2.area()) - intersectArea;

    return 1. * intersectArea / unionArea;
}

/** Return idxs of good boxes (ones with highest confidence score (>= thresholdScore)
 *  and IOU <= thresholdIOU with others).
 */
vector<size_t> filterBoxes(tensorflow::TTypes<float>::Flat &scores,
                           tensorflow::TTypes<float, 3>::Tensor &boxes,
                           double thresholdIOU, double thresholdScore) {

    vector<size_t> sortIdxs(scores.size());
    iota(sortIdxs.begin(), sortIdxs.end(), 0);

    // Create set of "bad" idxs
    set<size_t> badIdxs = set<size_t>();
    size_t i = 0;
    while (i < sortIdxs.size()) {
        if (scores(sortIdxs.at(i)) < thresholdScore)
            badIdxs.insert(sortIdxs[i]);
        if (badIdxs.find(sortIdxs.at(i)) != badIdxs.end()) {
            i++;
            continue;
        }

        Rect2f box1 = Rect2f(Point2f(boxes(0, sortIdxs.at(i), 1), boxes(0, sortIdxs.at(i), 0)),
                             Point2f(boxes(0, sortIdxs.at(i), 3), boxes(0, sortIdxs.at(i), 2)));
        for (size_t j = i + 1; j < sortIdxs.size(); j++) {
            if (scores(sortIdxs.at(j)) < thresholdScore) {
                badIdxs.insert(sortIdxs[j]);
                continue;
            }
            Rect2f box2 = Rect2f(Point2f(boxes(0, sortIdxs.at(j), 1), boxes(0, sortIdxs.at(j), 0)),
                                 Point2f(boxes(0, sortIdxs.at(j), 3), boxes(0, sortIdxs.at(j), 2)));
            if (IOU(box1, box2) > thresholdIOU)
                badIdxs.insert(sortIdxs[j]);
        }
        i++;
    }

    // Prepare "good" idxs for return
    vector<size_t> goodIdxs = vector<size_t>();
    for (auto it = sortIdxs.begin(); it != sortIdxs.end(); it++)
        if (badIdxs.find(sortIdxs.at(*it)) == badIdxs.end())
            goodIdxs.push_back(*it);

    return goodIdxs;
}

/** Draw bounding box and add caption to the image.
 *  Boolean flag _scaled_ shows if the passed coordinates are in relative units (true by default in tensorflow detection)
 */
void drawBoundingBoxOnImage(Mat &image, double yMin, double xMin, double yMax, double xMax, double score, string label, bool scaled) {
    cv::Point tl, br;
    if (scaled) {
        tl = cv::Point((int) (xMin * image.cols), (int) (yMin * image.rows));
        br = cv::Point((int) (xMax * image.cols), (int) (yMax * image.rows));
    } else {
        tl = cv::Point((int) xMin, (int) yMin);
        br = cv::Point((int) xMax, (int) yMax);
    }
    int classIndex = -1;

    if(label == "fsg_cone_orange") classIndex = 0;
    if(label == "fsg_cone_orange_big") classIndex = 3;
    if(label == "fsg_cone_yellow") classIndex = 1;
    if(label == "fsg_cone_blue") classIndex = 2;

    switch(classIndex) {
        case 0: cv::rectangle(image, tl, br, cv::Scalar(0, 125, 255), 1); break;
        case 1: cv::rectangle(image, tl, br, cv::Scalar(0, 255, 255), 1); break;
        case 2: cv::rectangle(image, tl, br, cv::Scalar(255, 0, 0), 1); break;
        case 3: cv::rectangle(image, tl, br, cv::Scalar(255, 255, 255), 1); break;
        default: cout << "error" << endl;
    }

    // Ceiling the score down to 3 decimals (weird!)
    float scoreRounded = floorf(score * 1000) / 1000;
    string scoreString = to_string(scoreRounded).substr(0, 5);
    string caption = label + " (" + scoreString + ")";

    // Adding caption of type "LABEL (X.XXX)" to the top-left corner of the bounding box
    int fontCoeff = 12;
    cv::Point brRect = cv::Point(tl.x + caption.length() * fontCoeff / 1.6, tl.y + fontCoeff);
    cv::rectangle(image, tl, brRect, cv::Scalar(0, 255, 255), -1);
    cv::Point textCorner = cv::Point(tl.x, tl.y + fontCoeff * 0.9);
    cv::putText(image, caption, textCorner, FONT_HERSHEY_SIMPLEX, 0.4, cv::Scalar(255, 0, 0));
}

/** Draw bounding boxes and add captions to the image.
 *  Box is drawn only if corresponding score is higher than the _threshold_.
 */
void drawBoundingBoxesOnImage(Mat &image,
                              tensorflow::TTypes<float>::Flat &scores,
                              tensorflow::TTypes<float>::Flat &classes,
                              tensorflow::TTypes<float,3>::Tensor &boxes,
                              map<int, string> &labelsMap,
                              vector<size_t> &idxs) {
    for (int j = 0; j < idxs.size(); j++)
        drawBoundingBoxOnImage(image,
                               boxes(0,idxs.at(j),0), boxes(0,idxs.at(j),1),
                               boxes(0,idxs.at(j),2), boxes(0,idxs.at(j),3),
                               scores(idxs.at(j)), labelsMap[classes(idxs.at(j))]);
}
