#include "baumergapicapture.h"

BaumerGapiCapture::BaumerGapiCapture()
{

}

BaumerGapiCapture::~BaumerGapiCapture()
{

}

void BaumerGapiCapture::initCamera(string settingsPath)
{
    BGAPI2::SystemList* syslist = BGAPI2::SystemList::GetInstance();
    syslist->Refresh();
    BGAPI2::System* sys = nullptr;

    for (auto iter = syslist->begin(); iter != syslist->end(); iter++) {
        if(iter->second->GetDisplayName() == "Baumer U3V TL") {
            sys = iter->second;
        }
    }

    try {
        sys->Open();
    } catch (BGAPI2::Exceptions::IException &e) {
        cout << e.GetErrorDescription() << endl;
    }

    BGAPI2::InterfaceList* ifaces = sys->GetInterfaces();
    ifaces->Refresh(100);

    BGAPI2::Interface* iface = nullptr;

    if(ifaces->size() != 0) {
        iface = ifaces->begin()->second;
    }

    try {
        iface->Open();
    } catch (BGAPI2::Exceptions::IException &e) {
        cout << e.GetErrorDescription() << endl;
    }

    BGAPI2::DeviceList* devices = iface->GetDevices();
    devices->Refresh(100);

    if(devices->size() != 0) {
        dev = devices->begin()->second;
        cout << dev->GetDisplayName() << endl;
    }

    try {
        dev->Open();
    } catch (BGAPI2::Exceptions::IException &e) {
        cout << e.GetErrorDescription() << endl;
    }

    dev->GetRemoteNode("AcquisitionStop")->Execute();

    dev->GetRemoteNode("PixelFormat")->SetString("BayerRG8");
    dev->GetRemoteNode("ExposureTime")->SetDouble(20000);
    dev->GetRemoteNode("Gain")->SetDouble(30);
    dev->GetRemoteNode("Height")->SetInt(832);
    dev->GetRemoteNode("OffsetY")->SetInt(816);

    BGAPI2::DataStreamList* streamlist = dev->GetDataStreams();

    streamlist->Refresh();

    if(streamlist->size() != 0) {
        stream = streamlist->begin()->second;
    }

    try {
        stream->Open();
    } catch (BGAPI2::Exceptions::IException &e) {
        cout << e.GetErrorDescription() << endl;
    }

    //dev->GetRemoteNode("AcquisitionStop")->Execute();
    stream->StopAcquisition();
    stream->GetBufferList()->DiscardAllBuffers();

    buffer = new BGAPI2::Buffer();
    stream->GetBufferList()->Add(buffer);
    stream->GetBufferList()->begin()->second->QueueBuffer();
    stream->StartAcquisitionContinuous();

    dev->GetRemoteNode("AcquisitionStart")->Execute();

    initialized = true;
}

void BaumerGapiCapture::process()
{
    // from https://www.baumer.com/de/en/service-support/know-how/technical-application-notes-industrial-cameras/baumer-gapi-and-opencv/a/baumer-gapi-and-opencv
    filledBuffer = stream->GetFilledBuffer(100);

    cv::Mat imOriginal(static_cast<int>(filledBuffer->GetHeight()),
                                          static_cast<int>(filledBuffer->GetWidth()),
                                          CV_8UC1,
                                          static_cast<char*>(filledBuffer->GetMemPtr()));
    cv::Mat imTransformBGR8(static_cast<int>(filledBuffer->GetHeight()),
                                       static_cast<int>(filledBuffer->GetWidth()),
                                       CV_8UC3);

    cv::cvtColor(imOriginal, imTransformBGR8, CV_BayerBG2BGR);

    setFrame(make_shared<Mat>(imTransformBGR8));

    filledBuffer->QueueBuffer();
}
