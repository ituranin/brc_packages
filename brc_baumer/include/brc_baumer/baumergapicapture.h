#ifndef BAUMERGAPICAPTURE_H
#define BAUMERGAPICAPTURE_H

#include <brc_base/camerathread.h>
#include <bgapi2_genicam/bgapi2_genicam.hpp>

using namespace std;

class BaumerGapiCapture : public CameraThread
{
public:
    BaumerGapiCapture();
    ~BaumerGapiCapture();

    void initCamera(string settingsPath);

private:

    BGAPI2::Device* dev = nullptr;
    BGAPI2::DataStream* stream = nullptr;
    BGAPI2::Buffer* buffer = nullptr;
    BGAPI2::Buffer *filledBuffer = nullptr;

    void process();
};

#endif // BAUMERGAPICAPTURE_H
