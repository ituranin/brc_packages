#include <iostream>
#include <stdexcept>
#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <brc_base/SysLog.h>
#include <brc_base/ASState.h>
#include <brc_base/asStates.h>
#include <brc_base/calculation/boolinputoutput.h>
#include <brc_inspection/inspectiontracker.h>
#include <brc_acceleration/accelerationtracker.h>

#include "brc_director/readytodrivetracker.h"
#include "brc_director/endlesstracker.h"

#define RATE 20

static ros::Subscriber missionSubscriber;
static ros::Subscriber asStateSubscriber;
static ros::Publisher directorGoPublisher;
static ros::Publisher statePublisher;
static ReadyToDriveTracker* readyToDriveTracker = nullptr;
static BaseCalculation* missionFinishedTracker = nullptr;

static uint8_t mission = MISSION_NONE;
static uint8_t state = AS_STATE_OFF;
static BoolInputOutput* directorGo = nullptr;
static BoolInputOutput* missionFinished = nullptr;
static BoolInputOutput* missionShouldFinish = nullptr;

void setMission(const brc_base::SysLog::ConstPtr& msg) {
    mission = msg->state;
}

void setState(const brc_base::ASState::ConstPtr& msg) {
    state = msg->currentState;
}

int main(int argc, char *argv[]){

    ros::init(argc, argv, "director");
    ros::NodeHandle n;

    missionSubscriber = n.subscribe("/mission/syslog", 1, setMission);
    asStateSubscriber = n.subscribe("/asState", 1, setState);
    directorGoPublisher = n.advertise<std_msgs::Bool>("/director/go", 1);
    statePublisher = n.advertise<brc_base::SysLog>("/director/state", 1);

    directorGo = new BoolInputOutput();
    directorGo->setDescription("directorGo");

    missionFinished = new BoolInputOutput();
    missionFinished->setDescription("missionFinished");

    missionShouldFinish = new BoolInputOutput();
    missionShouldFinish->setDescription("missionShouldFinish");

    readyToDriveTracker = new ReadyToDriveTracker();
    readyToDriveTracker->setNodeHandle(&n);
    readyToDriveTracker->setRate(RATE);
    readyToDriveTracker->addResultHolder(directorGo);
    readyToDriveTracker->initialize();

    brc_base::SysLog syslogMsg;
    syslogMsg.state = CPU_STATE_OFF;
    statePublisher.publish(syslogMsg);

    ros::Rate r(RATE);

    while (ros::ok() && mission == MISSION_NONE) {
        ros::spinOnce();
        r.sleep();
    }

    switch(mission) {
        case MISSION_NONE: throw std::invalid_argument("mission none"); break;
        case MISSION_MANUAL: break;
        case MISSION_ACCELERATION: {
            AccelerationTracker* accelerationTracker = new AccelerationTracker();
            accelerationTracker->setNodeHandle(&n);
            accelerationTracker->setRate(RATE);
            accelerationTracker->addResultHolder(missionFinished);
            accelerationTracker->addResultHolder(missionShouldFinish);
            accelerationTracker->initialize();

            missionFinishedTracker = accelerationTracker;
            break;
        }
        case MISSION_INSPECTION: {
            InspectionTracker* inspectionTracker = new InspectionTracker();
            inspectionTracker->setNodeHandle(&n);
            inspectionTracker->setRate(RATE);
            inspectionTracker->addResultHolder(missionFinished);
            inspectionTracker->addResultHolder(missionShouldFinish);
            inspectionTracker->initialize();

            missionFinishedTracker = inspectionTracker;
            break;
        }
        case MISSION_EBSTEST:
        case MISSION_AUTOCROSS:
        case MISSION_SKIDPAD:
        case MISSION_CONTROLLER:
        case MISSION_TRACKDRIVE: {
            EndlessTracker* endlessTracker = new EndlessTracker();
            endlessTracker->setNodeHandle(&n);
            endlessTracker->setRate(RATE);
            endlessTracker->addResultHolder(missionFinished);
            endlessTracker->addResultHolder(missionShouldFinish);
            endlessTracker->initialize();

            missionFinishedTracker = endlessTracker;
            break;
        }

        default: throw std::invalid_argument("mission unknown"); break;
    }

    syslogMsg.state = CPU_STATE_READY;
    statePublisher.publish(syslogMsg);

    while (ros::ok() && state != AS_STATE_DRIVING) {
        ros::spinOnce();
        r.sleep();
    }

    syslogMsg.state = CPU_STATE_ACTIVE;
    statePublisher.publish(syslogMsg);

    while(ros::ok()) {
        ros::spinOnce();

        readyToDriveTracker->calculate();

        if (directorGo->value) {
            missionFinishedTracker->calculate();
        }

        std_msgs::Bool goMsg;

        if (missionShouldFinish->value) {
            goMsg.data = false;
        } else {
            goMsg.data = directorGo->value;
        }

        directorGoPublisher.publish(goMsg);

        if (missionFinished->value) {
            syslogMsg.state = CPU_STATE_FINISHED;
            statePublisher.publish(syslogMsg);
        }

        r.sleep();
    }

    return 0;

}
