#include "brc_director/readytodrivetracker.h"

ReadyToDriveTracker::ReadyToDriveTracker() {}

ReadyToDriveTracker::~ReadyToDriveTracker() {}

void ReadyToDriveTracker::addResultHolder(BaseInputOutput* result)
{
    if(result->getDescription() == "directorGo") {
        this->result = dynamic_cast<BoolInputOutput*>(result);
    }
}

void ReadyToDriveTracker::initialize()
{
    BaseRosCalculation::initialize();

    steeringReadySubscriber = nodeHandle->subscribe("/director/steering/ready", 1, &ReadyToDriveTracker::setSteeringReady, this);
    tractiveReadySubscriber = nodeHandle->subscribe("/director/tractive/ready", 1, &ReadyToDriveTracker::setTractiveReady, this);

    if (this->result == nullptr) {
        throw "BoolResult with description directorGo needed";
    }
}

void ReadyToDriveTracker::calculate()
{
    if (this->steeringReady && this->tractiveReady) {
        this->result->value = true;
    }
}

void ReadyToDriveTracker::setSteeringReady(const std_msgs::Bool::ConstPtr& msg)
{
    this->steeringReady = msg->data;
}

void ReadyToDriveTracker::setTractiveReady(const std_msgs::Bool::ConstPtr& msg)
{
    this->tractiveReady = msg->data;
}
