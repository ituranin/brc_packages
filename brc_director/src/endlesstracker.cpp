#include "brc_director/endlesstracker.h"

EndlessTracker::EndlessTracker() {}

EndlessTracker::~EndlessTracker() {}

void EndlessTracker::addResultHolder(BaseInputOutput *result)
{
    if(result->getDescription() == "missionFinished") {
        this->missionFinished = dynamic_cast<BoolInputOutput*>(result);
    }

    if(result->getDescription() == "missionShouldFinish") {
        this->missionShouldFinish = dynamic_cast<BoolInputOutput*>(result);
    }
}

void EndlessTracker::initialize()
{
    BaseRosCalculation::initialize();

    steeringFinishedSubscriber = nodeHandle->subscribe("/director/steering/finished", 1, &EndlessTracker::setSteeringFinished, this);
    tractiveFinishedSubscriber = nodeHandle->subscribe("/director/tractive/finished", 1, &EndlessTracker::setTractiveFinished, this);
    distanceSubscriber = nodeHandle->subscribe("/VehTripDist", 1, &EndlessTracker::setDistanceTraveled, this);
    lapCounterSubscriber = nodeHandle->subscribe("/LapCount", 1, &EndlessTracker::setLapCount, this);

    if (missionFinished == nullptr || missionShouldFinish == nullptr) {
        throw std::invalid_argument("missionFinished or missionShouldFinish not set");
    }

    distanceToTravel = SettingsLoader::getInstance()->loadSettings("travelDistance.json")["distance"].asDouble();

    steeringFinished = false;
    tractiveFinished = false;
}

void EndlessTracker::calculate()
{
    /*if(distanceTraveled >= distanceToTravel)
    {
        missionShouldFinish->value = true;
    }*/

    if(lapCount > 9)
    {
        missionShouldFinish->value = true;
    }

    if(tractiveFinished)
    {
        missionFinished->value = true;
    }
}

void EndlessTracker::setSteeringFinished(const std_msgs::Bool::ConstPtr& msg)
{
    this->steeringFinished = msg->data;
}

void EndlessTracker::setTractiveFinished(const std_msgs::Bool::ConstPtr& msg)
{
    this->tractiveFinished = msg->data;
}

void EndlessTracker::setDistanceTraveled(const std_msgs::Float64::ConstPtr& msg)
{
    this->distanceTraveled = msg->data;
}

void EndlessTracker::setLapCount(const std_msgs::Int16::ConstPtr &msg)
{
    this->lapCount = msg->data;
}
