#ifndef READYTODRIVETRACKER_H
#define READYTODRIVETRACKER_H

#include <brc_base/calculation/baseroscalculation.h>
#include <brc_base/calculation/boolinputoutput.h>
#include <std_msgs/Bool.h>
#include <iostream>

using namespace ros;

class ReadyToDriveTracker : public BaseRosCalculation
{
public:
    ReadyToDriveTracker();
    virtual ~ReadyToDriveTracker();
    virtual void initialize();
    virtual void calculate();
    virtual void addResultHolder(BaseInputOutput *result);
protected:
    BoolInputOutput* result = nullptr;
    bool steeringReady = false;
    bool tractiveReady = false;
    bool steeringFinished = false;
    bool tractiveFinished = false;

    Subscriber steeringReadySubscriber;
    Subscriber tractiveReadySubscriber;

    void setSteeringReady(const std_msgs::Bool::ConstPtr& msg);
    void setTractiveReady(const std_msgs::Bool::ConstPtr& msg);
};

#endif // READYTODRIVETRACKER_H
