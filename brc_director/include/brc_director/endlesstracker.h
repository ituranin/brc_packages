#ifndef ENDLESSTRACKER_H
#define ENDLESSTRACKER_H

#include <brc_base/calculation/baseroscalculation.h>
#include <brc_base/calculation/boolinputoutput.h>
#include <brc_base/settingsloader.h>
#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int16.h>

using namespace ros;

class EndlessTracker : public BaseRosCalculation
{
public:
    EndlessTracker();
    virtual ~EndlessTracker();
    virtual void initialize();
    virtual void calculate();
    virtual void addResultHolder(BaseInputOutput *result);
private:

    Subscriber steeringFinishedSubscriber;
    Subscriber tractiveFinishedSubscriber;
    Subscriber distanceSubscriber;
    Subscriber lapCounterSubscriber;

    bool steeringFinished = false;
    bool tractiveFinished = false;
    double distanceTraveled = 0.0;
    double distanceToTravel = 0.0;
    int lapCount = -1;

    BoolInputOutput *missionFinished = nullptr;
    BoolInputOutput *missionShouldFinish = nullptr;

    void setSteeringFinished(const std_msgs::Bool::ConstPtr& msg);
    void setTractiveFinished(const std_msgs::Bool::ConstPtr& msg);
    void setDistanceTraveled(const std_msgs::Float64::ConstPtr& msg);
    void setLapCount(const std_msgs::Int16::ConstPtr& msg);
};

#endif // EndlessTracker_H
