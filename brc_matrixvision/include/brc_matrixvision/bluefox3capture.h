#ifndef BLUEFOX3CAPTURE_H
#define BLUEFOX3CAPTURE_H

#include <brc_base/camerathread.h>
#include <mvIMPACT_CPP/mvIMPACT_acquire.h>
#include <opencv2/highgui.hpp>

using namespace std;
using mvIMPACT::acquire::Device;
using mvIMPACT::acquire::DeviceManager;
using mvIMPACT::acquire::FunctionInterface;
using mvIMPACT::acquire::SystemSettings;
using mvIMPACT::acquire::Request;

class Bluefox3Capture : public CameraThread
{
public:
    Bluefox3Capture();
    ~Bluefox3Capture();

    void setSerial(string serial);
    void initCamera(string settingsPath);

private:
    Device* dev = nullptr;
    DeviceManager* deviceManager = nullptr;
    FunctionInterface* fi = nullptr;
    SystemSettings* systemSettings = nullptr;

    Request* imageRequest = nullptr;
    int requestNr = INVALID_ID;

    int resultCode;
    const int timeoutMS = 2000;

    string serialNr = "";

    void process();
};

#endif // BLUEFOX3CAPTURE_H
