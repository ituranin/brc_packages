#include "bluefox3capture.h"

static int toOpenCVImageType(TImageBufferPixelFormat imagePixelFormat);

Bluefox3Capture::Bluefox3Capture()
{

}

Bluefox3Capture::~Bluefox3Capture()
{

}

void Bluefox3Capture::setSerial(string serial)
{
    serialNr = serial;
}

void Bluefox3Capture::initCamera(string settingsPath)
{
    if(initialized) {
        throw std::invalid_argument("do not initialize twice!");
    }

    if(serialNr == "" || serialNr.empty()) {
        throw std::invalid_argument("use setSerial() first!");
    }

    deviceManager = new DeviceManager();

    // old get device by family string
    //dev = deviceManager->getDeviceByFamily("mvBlueFOX3");

    dev = deviceManager->getDeviceBySerial(serialNr);

    if(dev == nullptr) {
        throw std::invalid_argument("device family mvBlueFOX3 not found");
    }

    try {
        dev->open();
    } catch (const ImpactAcquireException& e) {
        throw std::invalid_argument("failed to open camera device: " + e.getErrorString());
    }

    fi = new FunctionInterface(dev);
    systemSettings = new SystemSettings(dev);

    resultCode = fi->loadSetting(settingsPath);

    if (resultCode != DMR_NO_ERROR) {
        throw std::invalid_argument("failed loading settings");
    }

    if( dev->acquisitionStartStopBehaviour.read() == assbUser ) {
        if( ( resultCode = static_cast<TDMR_ERROR>(fi->acquisitionStart()) ) != DMR_NO_ERROR ) {
            throw std::invalid_argument("acquisition start failed");
        }
    }

    initialized = true;
}

void Bluefox3Capture::process()
{
    do {
        requestNr = fi->imageRequestWaitFor(0);

        if(fi->isRequestNrValid(requestNr)) {
            imageRequest = fi->getRequest(requestNr);
            imageRequest->unlock();
            fi->imageRequestSingle();
        }
    } while(fi->isRequestNrValid(requestNr));

    if(imageRequest == nullptr) {
        requestNr = fi->imageRequestWaitFor(timeoutMS);

        if(fi->isRequestNrValid(requestNr)) {
            imageRequest = fi->getRequest(requestNr);
        }
    }

    if(imageRequest != nullptr) {
        if(imageRequest->isOK()) {
            int openCVPixelType = toOpenCVImageType(imageRequest->imagePixelFormat.read());
            int width = imageRequest->imageWidth.read();
            int height = imageRequest->imageHeight.read();
            int pitch = imageRequest->imageLinePitch.read();
            void* data = imageRequest->imageData.read();

            Mat image(Size(width, height), openCVPixelType, data, static_cast<size_t>(pitch));

            setFrame(make_shared<Mat>(image.clone()));
        }

        // seems to corrupt memory and mat.clone to segfault
        //imageRequest->unlock();
    }
    fi->imageRequestSingle();
}

static int toOpenCVImageType(TImageBufferPixelFormat imagePixelFormat) {

    switch(imagePixelFormat) {
        case ibpfMono8:
            return CV_8UC1;
        case ibpfMono10:
        case ibpfMono12:
        case ibpfMono14:
        case ibpfMono16:
            return CV_16UC1;
        case ibpfMono32:
            return CV_32SC1;
        case ibpfBGR888Packed:
        case ibpfRGB888Packed:
            return CV_8UC3;
        case ibpfRGBx888Packed:
           return CV_8UC4;
        case ibpfRGB101010Packed:
        case ibpfRGB121212Packed:
        case ibpfRGB141414Packed:
        case ibpfRGB161616Packed:
            return CV_16UC3;
        default:;
    }
    return -1;
}

