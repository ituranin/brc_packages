#include "brc_autox/polling/transforms.h"

Transforms::Transforms()
{

}

void Transforms::transformPoint(Circle *center, Point *point, double angleInRad)
{
    point->x = (cos(angleInRad) * (point->x - center->x)) - (sin(angleInRad) * (point->y - center->y)) + center->x;
    point->y = (sin(angleInRad) * (point->x - center->x)) + (cos(angleInRad) * (point->y - center->y)) + center->y;
    point->updateDistance();
}

void Transforms::transformMultiplePoints(Circle *center, vector<Point> *points, double angleInDegrees)
{
    if(angleInDegrees == 0.0) {
        return;
    }

    double rad = radians(angleInDegrees);

    for (size_t i = 0; i < points->size(); i++) {
        transformPoint(center, &points->at(i), rad);
    }
}

double Transforms::radians(double degrees)
{
    return degrees * (M_PI / 180);
}

double Transforms::degrees(double radians)
{
    return radians * (180 / M_PI);
}
