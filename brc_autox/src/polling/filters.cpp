#include "brc_autox/polling/filters.h"

Filters::Filters()
{

}

void Filters::filterIntersections(vector<Point> &ints, Line &line)
{
    double xmax, ymax, xmin, ymin = 0;
    vector<Point> newPoints;

    if(line.p1->x < line.p2->x) {
        xmax = line.p2->x;
        xmin = line.p1->x;
    } else {
        xmax = line.p1->x;
        xmin = line.p2->x;
    }

    if(line.p1->y < line.p2->y) {
        ymax = line.p2->y;
        ymin = line.p1->y;
    } else {
        ymax = line.p1->y;
        ymin = line.p2->y;
    }

    for (size_t i = 0; i < ints.size(); i++) {
        Point *point = &ints.at(i);
        if(xmin <= point->x
                && point->x <= xmax
                && ymin <= point->y
                && point->y <= ymax
                && 0.0 < point->x) {
            newPoints.push_back(*point);
        }
    }
    ints.clear();
    for (size_t i = 0; i < newPoints.size(); i++) {
        ints.push_back(newPoints.at(i));
    }
}

Point Filters::getNearestPoint(vector<Point> &points)
{
    Point *returnPoint = nullptr;
    double distance = 1000.0;

    for (size_t i = 0; i < points.size(); i++) {
        Point *point = &points.at(i);

        if(distance >= point->distance) {
            returnPoint = point;
            distance = point->distance;
        }
    }

    if(returnPoint == nullptr)
    {
        return Point(1000, 1000);
    }

    return Point(returnPoint->x, returnPoint->y);
}

Point Filters::getFarthestPointToLineCenter(vector<Point> &points, Line &line)
{
    double x = (line.p1->x + line.p2->x ) / 2;
    double y = (line.p1->y + line.p2->y ) / 2;
    Point middle(x, y);

    double distance = -1000.0;

    Point *nearest = nullptr;

    for (size_t i = 0; i < points.size(); i++) {
        if(distance <= middle.distanceTo(points.at(i)))
        {
            nearest = &points.at(i);
        }
    }

    if(nearest == nullptr)
    {
        return Point(x, y);
    }

    return Point(nearest->x, nearest->y);
}
