#include "brc_autox/polling/point.h"

Point::Point(double x, double y)
{
    this->x = x;
    this->y = y;
    this->distance = sqrt((this->x * this->x) + (this->y * this->y));
}

Point::Point(Cone &cone) : Point(cone.x, cone.y)
{
    this->z = cone.z;
    this->type = cone.type;
    this->score = cone.score;
}

double Point::calcDistance()
{
    return sqrt((this->x * this->x) + (this->y * this->y));
}

double Point::distanceTo(Point &p)
{
    double xDiff = this->x - p.x;
    double yDiff = this->y - p.y;
    return sqrt((xDiff * xDiff) + (yDiff * yDiff));
}

void Point::updateDistance()
{
    this->distance = this->calcDistance();
}

string Point::str()
{
    return "{ x: "
            + to_string(this->x)
            + ", y: " + to_string(this->y)
            + ", distance: "
            + to_string(this->distance)
            + "}";
}

bool Point::compare(const Point &l, const Point &r)
{
    return l.distance < r.distance;
}

void Point::sortTrackBound(vector<Point> &input, vector<Point> &output)
{
    if(input.size() == 0)
    {
        return;
    }

    std::sort(input.begin(), input.end(), &Point::compare);

    // TODO remove if not used
    //double pivotDistance = input[input.size()-1].distance;
    double pivotY = input[input.size()-1].y;

    vector<Point> forward;
    vector<Point> backward;
    Point* helperPoint = nullptr;
    bool helperPointSet = false;

    for (size_t i = 0; i < input.size() - 1; i++) {
        if(input[i].y > pivotY)
        {
            forward.push_back(input[i]);
        } else {
            backward.push_back(input[i]);
        }
    }

    if(forward.size() == 0 && output.size() == 1)
    {
        helperPoint = &output.at(0);
        output.clear();
        helperPointSet = true;
    }

    for (size_t i = 0; i < forward.size(); i++) {
        output.push_back(forward[i]);
    }

    output.push_back(input[input.size()-1]);

    for (size_t i = backward.size(); i-- > 0; ) {
        output.push_back(backward[i]);
    }

    if(helperPointSet)
    {
        output.push_back(*helperPoint);
    }
}
