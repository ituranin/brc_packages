#include "brc_autox/polling/ackermanncar.h"

AckermannCar::AckermannCar(double wheelbase, double tread)
{
    this->wheelbase = wheelbase;
    this->tread = tread;
    this->halfTread = tread / 2.0;
}

void AckermannCar::initializeFromSteering(double steeringAngle)
{
    if(steeringAngle == 0.0) {
        return;
    }

    double radius = this->getRadiusFromSteering(steeringAngle);

    this->steeringAngle = steeringAngle;
    this->calculateWheelAngles(radius);

    this->circle = new Circle(0.0, radius, abs(radius));
    this->leftCircle = new Circle(0.0, radius, abs(radius) + halfTread);
    this->rightCircle = new Circle(0.0, radius, abs(radius) - halfTread);
}

double AckermannCar::getRadiusFromSteering(double steeringAngle)
{
    if(steeringAngle == 0.0) {
        return 0;
    }

    double steeringAngleRad = Transforms::radians(steeringAngle);
    double thirdAngle = Transforms::radians(90.0 - steeringAngle);

    return (this->wheelbase * sin(thirdAngle)) / sin(steeringAngleRad);
}

void AckermannCar::calculateWheelAngles(double radius)
{
    if(radius == 0.0) {
        this->leftAngle = 0.0;
        this->rightAngle = 0.0;
        return;
    }

    double left = this->wheelbase / (radius - this->halfTread);
    double right = this->wheelbase / (radius + this->halfTread);

    this->leftAngle = Transforms::degrees(atan(left));
    this->rightAngle = Transforms::degrees(atan(right));
}

void AckermannCar::getIntersections(vector<Point> &destination, Line &line)
{
    vector<Point> leftPoints;
    this->leftCircle->intersections(leftPoints, line);

    vector<Point> rightPoints;
    this->rightCircle->intersections(rightPoints, line);

    for (size_t i = 0; i < leftPoints.size(); i++) {
        destination.push_back(leftPoints.at(i));
    }

    for (size_t i = 0; i < rightPoints.size(); i++) {
        destination.push_back(rightPoints.at(i));
    }

    /*for (size_t i = 0; i < points->size(); i++) {
        cout << this->steeringAngle
             << " "
             << points->at(i).x
             << " "
             << points->at(i).y
             << " "
             << points->at(i).distance
             << endl;
    }*/

    Filters::filterIntersections(destination, line);
    return;
}

double AckermannCar::getDistanceAngleForDistance(double distance, double radius)
{
    if(radius == 0.0 && distance == 0.0) {
        return 0;
    }

    return (360 * distance) / ((radius * 2) * M_PI);
}
