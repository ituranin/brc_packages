#include "brc_autox/polling/line.h"

Line::Line(Point *p1, Point *p2)
{
    this->p1 = p1;
    this->p2 = p2;
}

string Line::str()
{
    if(this->p1 == nullptr || this->p2 == nullptr) {
        return "not initialized point";
    }

    return "{ p1: "
            + this->p1->str()
            + ", p2: "
            + this->p2->str()
            + "}";
}

Point Line::getPointAtX(double x)
{
    Point p(x, 0.0);
    p.y = (this->m * x) + this->n;
    return p;
}

void Line::createLinesFromPoints(vector<Point> &points, vector<Line> &lines)
{
    if(points.size() == 0)
    {
        return;
    }

    for (size_t i = 0; i < points.size() - 1; i++) {
        lines.push_back(Line(&points[i], &points[i+1]));
    }
}
