#include "brc_autox/polling/util.h"

Util::Util()
{

}

double Util::mean(vector<double> &values)
{
    double mean = 0;

    mean = sum(values) / values.size();

    return mean;
}

double Util::variance(vector<double> &values, double &mean)
{
    double variance = 0;

    vector<double> temp;

    for (size_t i = 0; i < values.size(); i++) {
        temp.push_back(pow(values[i] - mean, 2));
    }

    variance = Util::sum(temp);

    return variance;
}

double Util::sum(vector<double> &values)
{
    double sum = 0;

    for (size_t i = 0; i < values.size(); i++) {
        sum += values[i];
    }

    return sum;
}

double Util::covariance(vector<double> &x, double &meanX, vector<double> &y, double &meanY)
{
    double covariance = 0;

    for (size_t i = 0; i < x.size(); i++) {
        covariance += (x[i] - meanX) * (y[i] - meanY);
    }

    return covariance;
}

Line Util::simpleLinearRegression(vector<Point> &points)
{
    vector<double> x;
    vector<double> y;
    Line line(nullptr, nullptr);

    for (size_t i = 0; i < points.size(); i++) {
        x.push_back(points[i].x);
        y.push_back(points[i].y);
    }

    double meanX = Util::mean(x);
    double meanY = Util::mean(y);

    line.m = Util::covariance(x, meanX, y, meanY) / variance(x, meanX);
    line.n = meanY - (line.m * meanX);

    return line;
}
