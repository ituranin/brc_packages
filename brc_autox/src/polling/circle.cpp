#include "brc_autox/polling/circle.h"

double sgn(double x);

Circle::Circle(double x, double y, double r)
{
    this->x = x;
    this->y = y;
    this->radius = r;
}

void Circle::intersections(vector<Point> &destination, Line &line)
{
    double p1x = (line.p1->x - this->x);
    double p2x = (line.p2->x - this->x);
    double p1y = (line.p1->y - this->y);
    double p2y = (line.p2->y - this->y);
    double dx = p2x - p1x;
    double dy = p2y - p1y;
    double dr = sqrt((dx * dx) + (dy * dy));
    double dr2 = dr * dr;
    double du = (p1x * p2y) - (p2x * p1y);
    double incidence = ((this->radius * this->radius) * dr2) - (du * du);

    if(incidence < 0) {
        return;
    }

    double incidenceSquared = sqrt(incidence);
    double dudy = (du * dy);
    double mdudx = (-du * dx);
    double x1p = (sgn(dy) * dx * incidenceSquared);
    double y1p = (abs(dy) * incidenceSquared);
    double x1 = ((dudy + x1p) / dr2) + this->x;
    double y1 = ((mdudx + y1p) / dr2) + this->y;
    double x2 = ((dudy - x1p) / dr2) + this->x;
    double y2 = ((mdudx - y1p) / dr2) + this->y;

    destination.push_back(Point(x1, y1));

    if(incidence == 0.0) {
        return;
    }

    destination.push_back(Point(x2, y2));
}

string Circle::str()
{
    return "{ x: "
            + to_string(this->x)
            + ", y: "
            + to_string(this->y)
            + ", radius: "
            + to_string(this->radius)
            + "}";
}

double sgn(double x) {
    if(x < 0) {
        return -1;
    }
    return 1;
}
