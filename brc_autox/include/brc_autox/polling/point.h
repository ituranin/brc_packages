#ifndef POINT_H
#define POINT_H
#include <math.h>
#include <string>
#include <vector>
#include <algorithm>
#include <brc_base/cone.h>

using namespace std;

class Point : public Cone
{
public:
    double distance = 0.0;
    Point(double x, double y);
    Point(Cone& cone);
    double calcDistance();
    double distanceTo(Point &p);
    void updateDistance();
    string str();
    static bool compare(const Point& l, const Point& r);
    static void sortTrackBound(vector<Point>& input, vector<Point>& output);
};

#endif // POINT_H
