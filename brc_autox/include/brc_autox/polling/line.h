#ifndef LINE_H
#define LINE_H
#include "brc_autox/polling/point.h"

class Line
{
public:
    Point *p1 = nullptr;
    Point *p2 = nullptr;
    double m = 0.0;
    double n = 0.0;
    Line(Point *p1, Point *p2);
    string str();
    Point getPointAtX(double x);
    static void createLinesFromPoints(vector<Point>& points, vector<Line>& lines);
};

#endif // LINE_H
