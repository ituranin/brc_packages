#ifndef TRANSFORMS_H
#define TRANSFORMS_H

#include "brc_autox/polling/circle.h"
#include "math.h"

class Transforms
{
public:
    Transforms();
    static void transformPoint(Circle *center, Point *point, double angleInRad);
    static void transformMultiplePoints(Circle *center, vector<Point> *points, double angleInDegrees);
    static double radians(double degrees);
    static double degrees(double radians);
};

#endif // TRANSFORMS_H
