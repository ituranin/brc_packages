#ifndef STEERINGMASTER_H
#define STEERINGMASTER_H

#include "brc_autox/polling/ackermanncar.h"
#include "brc_autox/polling/filters.h"
#include <vector>
#include <iostream>
#include <math.h>

class SteeringMaster
{
public:

    double wheelbase = 0.0;
    double tread = 0.0;
    double stepsize = 1.0;
    double maxAngle = 1.0;
    double delayInS = 0.0;

    vector<AckermannCar> *cars = nullptr;
    vector<Point> *nearestPoints = nullptr;
    vector<size_t> *intersectionCount = nullptr;
    AckermannCar *transformCar = nullptr;

    SteeringMaster(double wheelbase, double tread, double stepsize, double maxAngle);
    double calculateAngle(vector<Line> &lines, size_t method);
    void transformMap(double lastSteeringAngle, double speedInKmh ,vector<Point> *points);

private:
    double findSteeringAngleWithFarthestIntersection();
    double findSteeringAngleWithFarthestIntersectionTwoStaged();
    double findSteeringAngleWithNearestIntersetionToLastLine(Line &line);
};

#endif // STEERINGMASTER_H
