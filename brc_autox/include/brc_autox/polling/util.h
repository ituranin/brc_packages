#ifndef UTIL_H
#define UTIL_H

#include <vector>
#include <math.h>
#include <brc_autox/polling/point.h>
#include <brc_autox/polling/line.h>

using namespace std;

class Util
{
public:
    Util();
    //static double max(vector<double> values);
    static double mean(vector<double> &values);
    static double variance(vector<double> &values, double &mean);
    static double sum(vector<double> &values);
    static double covariance(vector<double> &x, double &meanX, vector<double> &y, double &meanY);
    static Line simpleLinearRegression(vector<Point> &points);
};

#endif // UTIL_H
