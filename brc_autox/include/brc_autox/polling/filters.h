#ifndef FILTERS_H
#define FILTERS_H
#include "brc_autox/polling/line.h"
#include <vector>
#include <iostream>

using namespace std;

class Filters
{
public:
    Filters();
    static void filterIntersections(vector<Point> &ints, Line &line);
    static Point getNearestPoint(vector<Point> &points);
    static Point getFarthestPointToLineCenter(vector<Point> &points, Line &line);
};

#endif // FILTERS_H
