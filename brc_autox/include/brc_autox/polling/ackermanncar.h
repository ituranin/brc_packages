#ifndef ACKERMANNCAR_H
#define ACKERMANNCAR_H

#include "brc_autox/polling/circle.h"
#include "brc_autox/polling/transforms.h"
#include "brc_autox/polling/filters.h"

class AckermannCar
{
public:
    double wheelbase = 0.0;
    double tread = 0.0;
    double halfTread = 0.0;
    double leftAngle = 0.0;
    double rightAngle = 0.0;
    double steeringAngle = 0.0;
    Circle *circle = nullptr;
    Circle *leftCircle = nullptr;
    Circle *rightCircle = nullptr;

    AckermannCar(double wheelbase, double tread);
    void initializeFromSteering(double steeringAngle);
    double getRadiusFromSteering(double steeringAngle);
    void calculateWheelAngles(double radius);
    void getIntersections(vector<Point> &destination, Line &line);

    static double getDistanceAngleForDistance(double distance, double radius);
};

#endif // ACKERMANNCAR_H
