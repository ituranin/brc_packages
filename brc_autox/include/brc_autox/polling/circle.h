#ifndef CIRCLE_H
#define CIRCLE_H
#include "brc_autox/polling/line.h"
#include <math.h>
#include <vector>

using namespace std;

class Circle
{
public:
    double x = 0.0;
    double y = 0.0;
    double radius = 0.0;
    Circle(double x, double y, double r);
    void intersections(vector<Point> &destination, Line &line);
    string str();
};

#endif // CIRCLE_H
