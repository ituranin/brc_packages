#include <iostream>
#include <stdexcept>
#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <brc_base/TargetValue.h>
#include <brc_base/SysLog.h>
#include <brc_base/BrcHelpers.h>
#include <brc_base/asStates.h>
#include <brc_base/calculation/boolinputoutput.h>
#include <brc_base/calculation/tractivesystemresult.h>
#include <brc_inspection/inspectiontractive.h>
#include <brc_acceleration/accelerationtractive.h>
#include <brc_tempomat/tempomattractive.h>

#define RATE 20

static ros::Subscriber missionSubscriber;
static ros::Subscriber directorGoSubscriber;
static ros::Publisher appsPublisher;
static ros::Publisher clutchPublisher;
static ros::Publisher brakePublisher;
static ros::Publisher brakeRearPublisher;
static BaseCalculation* calculation = nullptr;
static BoolInputOutput* directorGoValue = nullptr;
static TractiveSystemResult* tsValue = nullptr;

static uint8_t mission = MISSION_NONE;
static bool directorGo = false;

void setMission(const brc_base::SysLog::ConstPtr& msg) {
    mission = msg->state;
}

void setDirectorGo(const std_msgs::Bool::ConstPtr& msg) {
    directorGo = msg->data;
    directorGoValue->value = directorGo;
}

void publishValues() {
    brc_base::TargetValue appsMsg;
    appsMsg.targetValue = tsValue->apps;

    brc_base::TargetValue clutchMsg;
    clutchMsg.targetValue = tsValue->clutch;

    brc_base::TargetValue brakeMsg;
    brakeMsg.targetValue = tsValue->brakeFront;

    brc_base::TargetValue brakeRearMsg;
    brakeRearMsg.targetValue = tsValue->brakeRear;

    appsPublisher.publish(appsMsg);
    clutchPublisher.publish(clutchMsg);
    brakePublisher.publish(brakeMsg);
    brakeRearPublisher.publish(brakeRearMsg);
}

int main(int argc, char *argv[]){

    ros::init(argc, argv, "tractive");
    ros::NodeHandle n;

    directorGoValue = new BoolInputOutput();
    directorGoValue->setDescription("directorGo");

    missionSubscriber = n.subscribe("/mission/syslog", 1, setMission);
    directorGoSubscriber = n.subscribe("/director/go", 1, setDirectorGo);
    appsPublisher = n.advertise<brc_base::TargetValue>("/throttle/targetValue", 1);
    clutchPublisher = n.advertise<brc_base::TargetValue>("/clutch/targetValue", 1);
    brakePublisher = n.advertise<brc_base::TargetValue>("/serviceBrake/targetValue", 1);
    brakeRearPublisher = n.advertise<brc_base::TargetValue>("/serviceBrakeRear/targetValue", 1);

    ros::Rate r(RATE);

    tsValue = new TractiveSystemResult();
    tsValue->setDescription("ts");
    tsValue->apps = static_cast<int16_t>(getAppsPosition(0.07));
    tsValue->clutch = getClutchPressureOutput(20.0);
    tsValue->brakeFront = getBrakePressureOutput(30.0);
    tsValue->brakeRear = getBrakePressureOutput(20.0);

    publishValues();

    while (ros::ok() && mission == MISSION_NONE) {
        ros::spinOnce();
        r.sleep();
    }

    switch(mission) {
        case MISSION_NONE: throw std::invalid_argument("mission none"); break;
        case MISSION_MANUAL: throw std::invalid_argument("mission manual"); break;
        case MISSION_INSPECTION: {
            InspectionTractive* inspectionCalc = new InspectionTractive();
            inspectionCalc->setNodeHandle(&n);
            inspectionCalc->setRate(RATE);
            inspectionCalc->addResultHolder(directorGoValue);
            inspectionCalc->addResultHolder(tsValue);
            inspectionCalc->initialize();

            calculation = inspectionCalc;
            break;
        }
        case MISSION_ACCELERATION:
        case MISSION_EBSTEST:
        case MISSION_SKIDPAD:
        case MISSION_AUTOCROSS:
        case MISSION_CONTROLLER:
        case MISSION_TRACKDRIVE: {
            TempomatTractive* trackdriveCalc = new TempomatTractive();
            trackdriveCalc->setNodeHandle(&n);
            trackdriveCalc->setRate(RATE);
            trackdriveCalc->addResultHolder(directorGoValue);
            trackdriveCalc->addResultHolder(tsValue);
            trackdriveCalc->initialize();

            calculation = trackdriveCalc;
            break;
        }
        default: throw std::invalid_argument("mission unknown"); break;
    }

    while(ros::ok() && directorGo == false) {
        ros::spinOnce();
        r.sleep();
    }

    while(ros::ok()) {
        ros::spinOnce();

        calculation->calculate();

        publishValues();

        r.sleep();
    }

    return 0;

}
