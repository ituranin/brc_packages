#include "brc_acceleration/accelerationtracker.h"

AccelerationTracker::AccelerationTracker() {}

AccelerationTracker::~AccelerationTracker() {}

void AccelerationTracker::addResultHolder(BaseInputOutput *result)
{
    if(result->getDescription() == "missionFinished") {
        this->missionFinished = dynamic_cast<BoolInputOutput*>(result);
    }

    if(result->getDescription() == "missionShouldFinish") {
        this->missionShouldFinish = dynamic_cast<BoolInputOutput*>(result);
    }
}

void AccelerationTracker::initialize()
{
    BaseRosCalculation::initialize();

    steeringFinishedSubscriber = nodeHandle->subscribe("/director/steering/finished", 1, &AccelerationTracker::setSteeringFinished, this);
    tractiveFinishedSubscriber = nodeHandle->subscribe("/director/tractive/finished", 1, &AccelerationTracker::setTractiveFinished, this);
    coneSubscriber = nodeHandle->subscribe("/camera/ConePosition", 1, &AccelerationTracker::setCones, this);
    //coneCountPublisher = nodeHandle->advertise<std_msgs::UInt8>("/coneCount", 1);
    speedSubscriber = nodeHandle->subscribe("/wheelSpeed", 1, &AccelerationTracker::setSpeed, this);
    dashOneSubscriber = nodeHandle->subscribe("/dashOne", 1, &AccelerationTracker::setGearAndRpm, this);
    finishedPublisher = nodeHandle->advertise<std_msgs::UInt8>("/accTracker/finished", 1);

    gearCooldown = ros::Time::now();

    if (missionFinished == nullptr || missionShouldFinish == nullptr) {
        throw std::invalid_argument("missionFinished or missionShouldFinish not set");
    }

    tractiveFinished = false;
}

void AccelerationTracker::calculate()
{
    int yellow = 0;
    int blue = 0;
    int orange = 0;
    for (size_t i = 0; i < cones.size(); i++) {
        if (cones[i].type == CONE_YELLOW) {
            yellow++;
        }

        if (cones[i].type == CONE_BLUE) {
            blue++;
        }

        if (cones[i].type == CONE_ORANGE || cones[i].type == CONE_ORANGE_BIG) {
            orange++;
        }
    }

    if(blue == 0 && yellow == 0 && orange > 1 && speed > 5.0) // && this->gear > 1
    {
        missionShouldFinish->value = true;
        std_msgs::UInt8 error;
        error.data = 1;
        finishedPublisher.publish(error);
    }

    if(tractiveFinished)
    {
        missionFinished->value = true;
    }

    /*std_msgs::UInt8 count;
    count.data = cones.size();
    coneCountPublisher.publish(count);*/

    return;
}

void AccelerationTracker::setSteeringFinished(const std_msgs::Bool::ConstPtr& msg)
{
    this->steeringFinished = msg->data;
}

void AccelerationTracker::setTractiveFinished(const std_msgs::Bool::ConstPtr& msg)
{
    this->tractiveFinished = msg->data;
}

void AccelerationTracker::setCones(const sensor_msgs::PointCloud2::ConstPtr& msg)
{
    detectionHelper::readDetectionsToVector(msg, cones);
}

void AccelerationTracker::setSpeed(const brc_base::WheelSpeed::ConstPtr& msg)
{
    double temp = ((msg->frontLeft + msg->frontRight) / 2) * 0.036;
    if(temp >= speed)
    {
        speed = temp;
    }
}

void AccelerationTracker::setGear(uint8_t value) {
    if ((ros::Time::now() - gearCooldown).toSec() < 0.2) {
        return;
    }

    this->gearCheck = value;

    if(this->gearCheck != this->gear)
    {
        this->gear = this->gearCheck;
    }

    gearCooldown = ros::Time::now();
}

void AccelerationTracker::setGearAndRpm(const brc_base::DashOne::ConstPtr &msg)
{
    setGear(static_cast<uint8_t>(msg->gear));
}
