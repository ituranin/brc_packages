#include "brc_acceleration/accelerationtractive.h"

AccelerationTractive::AccelerationTractive() {}

AccelerationTractive::~AccelerationTractive() {}

void AccelerationTractive::addResultHolder(BaseInputOutput* result)
{
    BaseTractiveCalculation::addResultHolder(result);
}

void AccelerationTractive::initialize()
{
    BaseTractiveCalculation::initialize();

    this->setReady();
}

void AccelerationTractive::calculate()
{
    if(this->directorGo->value == false) {
        this->result->clutch = getClutchPressureOutput(20.0);
        this->result->apps = static_cast<int16_t>(getAppsPosition(0.0));

        if (this->clutchPressure >= 18.0) {
            this->result->brakeFront = getBrakePressureOutput(20.0);
            this->result->brakeRear = getBrakePressureOutput(0.0);
            //this->result->apps = static_cast<int16_t>(getAppsPosition(0.0));
            sendDownshift();
        }

        if(this->brakeEngaged && this->speedIsZero) {
            this->setFinished();
        }

        return;
    }

    if(this->brakeEngaged) {
        this->result->clutch = getClutchPressureOutput(20.0);
        this->clutchStartTimeSet = false;
    }

    if(this->clutchPressure >= 18.0 && this->gear == 0) {
        sendUpshift();
        this->result->apps = static_cast<int16_t>(getAppsPosition(0.1));
        return;
    }

    if(this->clutchPressure >= 18.0 && this->gear == 1) {

        this->result->brakeFront = getBrakePressureOutput(0.0);
        this->result->brakeRear = getBrakePressureOutput(0.0);
        this->result->apps = static_cast<int16_t>(getAppsPosition(0.7));

        if(this->clutchStartTimeSet == false) {
            this->clutchStartTime = ros::Time::now();
            this->clutchStartTimeSet = true;
        }

        // wait 2 seconds
        if( (ros::Time::now() - this->clutchStartTime).toSec() >= 2.0 && !this->brakeEngaged) {
            this->result->clutch = getClutchPressureOutput(0.0);
        }
    }

    if(this->speed > 20.0)
    {
        this->result->apps = static_cast<int16_t>(getAppsPosition(1.0));
    }

    if(this->clutchPressure <= 3.0 && this->gear < 4) {
        switch(this->gear)
        {
            case 1:
            {
                if(this->rpm > AS_GEAR_1_MIN_RPM && this->speed > AS_GEAR_1_MIN_SPEED)
                {
                    sendUpshift();
                }
                break;
            }
            case 2:
            {
                if(this->rpm > AS_GEAR_2_MIN_RPM && this->speed > AS_GEAR_2_MIN_SPEED)
                {
                    sendUpshift();
                }
                break;
            }
            case 3:
            {
                if(this->rpm > AS_GEAR_3_MIN_RPM && this->speed > AS_GEAR_3_MIN_SPEED)
                {
                    sendUpshift();
                }
                break;
            }
            default: break;
        }
    }
}
