#include "brc_acceleration/accelerationsteering.h"

AccelerationSteering::AccelerationSteering() {}

AccelerationSteering::~AccelerationSteering() {}

void AccelerationSteering::addResultHolder(BaseInputOutput *result)
{
    BaseSteeringCalculation::addResultHolder(result);
}

void AccelerationSteering::initialize()
{
    BaseSteeringCalculation::initialize();

    // load settings
    Json::Value settings = SettingsLoader::getInstance()->loadSettings("accelerationSteering.json");

    this->MAX_SPEED = settings["settings"]["maxSpeed"].asDouble();
    this->MAX_TRAVEL = settings["settings"]["maxTravel"].asDouble();
    this->STEERING_DAMPING = settings["settings"]["steeringDamping"].asDouble();
    this->STEERING_DAMPING_MAX = settings["settings"]["steeringMaxDamping"].asDouble();

    this->MAX_TRAVEL_X2 = this->MAX_TRAVEL * 2.0;
    this->STEERING_DAMPING_DIFF = this->STEERING_DAMPING - this->STEERING_DAMPING_MAX;

    /*cout << this->MAX_SPEED << endl;
    cout << this->MAX_TRAVEL << endl;
    cout << this->STEERING_DAMPING << endl;
    cout << this->STEERING_DAMPING_MAX << endl;
    cout << this->MAX_TRAVEL_X2 << endl;
    cout << this->STEERING_DAMPING_DIFF << endl;*/

    coneSubscriber = nodeHandle->subscribe("/camera/ConePosition", 1, &AccelerationSteering::setCones, this);
    markerPublisher = nodeHandle->advertise<visualization_msgs::Marker>("/sMarker", 1);

    yellowFilter = new MeanFilter<float>();
    blueFilter = new MeanFilter<float>();

    this->result->value = 0;

    bool differentConesFound = false;

    while (differentConesFound == false) {
        ros::spinOnce();

        if (cones.size() > 1) {

            blue = 0;
            yellow = 0;
            blueY = 0.0;
            yellowY = 0.0;

            for (size_t i = 0; i < cones.size(); i++) {
                if (cones[i].type == CONE_YELLOW) {
                    yellow++;
                    yellowY += cones[i].y;
                }

                if (cones[i].type == CONE_BLUE) {
                    blue++;
                    blueY += cones[i].y;
                }
            }

            if (blue > 1 && yellow > 1) {
                differentConesFound = true;

                blueY = blueY / blue;
                yellowY = yellowY / yellow;

                blueFilter->getFiltered(blueY);
                yellowFilter->getFiltered(yellowY);

                mapWidth = abs(blueY) + abs(yellowY);

                calculate();
            }
        }

        rosRate->sleep();
    }

    this->setReady();
}

void AccelerationSteering::calculate()
{
    if (cones.size() < 2) {
        return;
    }

    blue = 0;
    yellow = 0;
    blueY = 0.0;
    yellowY = 0.0;
    allX = 0.0;
    orange.clear();

    for (size_t i = 0; i < cones.size(); i++) {
        if (cones[i].x > 25.0f
                || cones[i].y > mapWidth
                || cones[i].y < -mapWidth) {
            continue;
        }

        if (cones[i].type == CONE_YELLOW) {
            yellow++;
            yellowY += cones[i].y;
            allX += cones[i].x;
        }

        if (cones[i].type == CONE_BLUE) {
            blue++;
            blueY += cones[i].y;
            allX += cones[i].x;
        }

        if (cones[i].type == CONE_ORANGE || cones[i].type == CONE_ORANGE_BIG) {
            orange.push_back(cones[i]);
        }
    }

    if ((blue < 1 || yellow < 1) && orange.size() > 1) {
        vector<Point> orangePoints;

        for (size_t i = 0; i < orange.size(); i++) {
            orangePoints.push_back(Point(orange[i]));
        }

        Line border = Util::simpleLinearRegression(orangePoints);
        for (size_t i = 0; i < orange.size(); i++) {
            Point yPoint = border.getPointAtX(orange[i].x);
            if(orange[i].y >= yPoint.y)
            {
                blue++;
                blueY += orange[i].y;
            }

            if(orange[i].y <= yPoint.y)
            {
                yellow++;
                yellowY += orange[i].y;
            }
        }
    }

    /*if (blue < 1 || yellow < 1) {
        for (size_t i = 0; i < orange.size(); i++) {
            if(orange[i].y >= (blueFilter->getValue() - 0.5f))
            {
                blue++;
                blueY += orange[i].y;
            }

            if(orange[i].y <= (yellowFilter->getValue() + 0.5f))
            {
                yellow++;
                yellowY += orange[i].y;
            }
        }
    }*/

    if (blue < 1 || yellow < 1) {
        return;
    }

    blueY = blueY / blue;
    yellowY = yellowY / yellow;

    blueFilter->getFiltered(blueY);
    yellowFilter->getFiltered(yellowY);

    allX = allX / ( blue + yellow );

    center = ( blueY + yellowY ) / 2;
    mapCenter = (mapWidth / 2) + center;
    centerPercentage = mapCenter / mapWidth;

    if(centerPercentage > 1.0f)
    {
        centerPercentage = 1.0f;
    }

    if(centerPercentage < 0.0f)
    {
        centerPercentage = 0.0f;
    }

    int16_t directTarget = static_cast<int16_t>((this->MAX_TRAVEL_X2 * centerPercentage) - this->MAX_TRAVEL);

    double speedFactor = this->STEERING_DAMPING_DIFF * (this->speed / this->MAX_SPEED);

    this->result->value = static_cast<int16_t>((directTarget + currentSteeringPosition) * (this->STEERING_DAMPING - speedFactor));

    /* MARKER */
    /*visualization_msgs::Marker point;
    point.header.frame_id = "ConePosition";
    point.header.stamp = ros::Time::now();
    point.type = visualization_msgs::Marker::SPHERE;
    point.color.g = 1.0;
    point.color.a = 1.0;
    point.pose.position.x = static_cast<double>(15.0f);
    point.pose.position.y = static_cast<double>(center);
    point.pose.position.z = 0.0;
    point.scale.x = 0.1;
    point.scale.y = 0.1;
    point.scale.z = 0.1;
    markerPublisher.publish(point);*/
}

void AccelerationSteering::setCones(const sensor_msgs::PointCloud2::ConstPtr& msg)
{
    detectionHelper::readDetectionsToVector(msg, cones);
}
