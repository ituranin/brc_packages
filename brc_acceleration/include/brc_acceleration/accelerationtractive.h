#ifndef ACCELERATIONTRACKTIVE_H
#define ACCELERATIONTRACKTIVE_H

#include <brc_base/calculation/basetractivecalculation.h>
#include <iostream>

#define AS_GEAR_1_MIN_RPM		9500
#define AS_GEAR_1_MIN_SPEED		45		//30km/h

#define AS_GEAR_2_MIN_RPM		9500
#define AS_GEAR_2_MIN_SPEED		45	//40km/h

#define AS_GEAR_3_MIN_RPM		9500
#define AS_GEAR_3_MIN_SPEED		45	//45km/H


class AccelerationTractive : public BaseTractiveCalculation
{
public:
    AccelerationTractive();
    virtual ~AccelerationTractive();
    virtual void initialize();
    virtual void calculate();
    virtual void addResultHolder(BaseInputOutput* result);
private:
    ros::Time clutchStartTime;
    bool clutchStartTimeSet = false;
};

#endif // ACCELERATIONTRACKTIVE_H
