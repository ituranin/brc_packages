#ifndef ACCELERATIONSTEERING_H
#define ACCELERATIONSTEERING_H

#include <brc_base/calculation/basesteeringcalculation.h>
#include <brc_base/settingsloader.h>
#include <brc_base/detectionhelpers.h>
#include <brc_base/filter/meanfilter.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <vector>
#include <math.h>
#include <brc_autox/polling/util.h>

class AccelerationSteering : public BaseSteeringCalculation
{
public:
    AccelerationSteering();
    virtual ~AccelerationSteering();
    virtual void initialize();
    virtual void calculate();
    virtual void addResultHolder(BaseInputOutput *result);
private:
    ros::Subscriber coneSubscriber;
    ros::Publisher markerPublisher;

    std::vector<Cone> cones;
    std::vector<float> smoothener;

    MeanFilter<float>* yellowFilter = nullptr;
    MeanFilter<float>* blueFilter = nullptr;

    int blue = 0;
    int yellow = 0;
    float blueY = 0.0;
    float yellowY = 0.0;
    float allX = 0.0;
    float center = 0.0;
    float mapCenter = 0.0;
    float centerPercentage = 0.0;
    float mapWidth = 10.0f;
    std::vector<Cone> orange;

    // settings
    double MAX_SPEED = 100.0;
    double MAX_TRAVEL = 2500.0;
    double MAX_TRAVEL_X2 = 5000.0;
    double STEERING_DAMPING = 0.15;
    double STEERING_DAMPING_MAX = 0.05;
    double STEERING_DAMPING_DIFF = 0.10;

    void setCones(const sensor_msgs::PointCloud2::ConstPtr& msg);
};

#endif // ACCELERATIONSTEERING_H
