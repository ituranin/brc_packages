#ifndef ACCELERATIONTRACKER_H
#define ACCELERATIONTRACKER_H

#include <brc_base/calculation/baseroscalculation.h>
#include <brc_base/calculation/boolinputoutput.h>
#include <brc_base/detectionhelpers.h>
#include <brc_base/WheelSpeed.h>
#include <brc_base/DashOne.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt8.h>

using namespace ros;

class AccelerationTracker : public BaseRosCalculation
{
public:
    AccelerationTracker();
    virtual ~AccelerationTracker();
    virtual void initialize();
    virtual void calculate();
    virtual void addResultHolder(BaseInputOutput *result);
private:

    Subscriber steeringFinishedSubscriber;
    Subscriber tractiveFinishedSubscriber;
    Subscriber coneSubscriber;
    Subscriber speedSubscriber;
    Publisher coneCountPublisher;
    Publisher finishedPublisher;

    Subscriber dashOneSubscriber;
    Time gearCooldown;
    uint8_t gear = 0;
    uint8_t gearCheck = 0;

    bool steeringFinished;
    bool tractiveFinished;
    std::vector<Cone> cones;
    double speed = 0.0;

    BoolInputOutput *missionFinished = nullptr;
    BoolInputOutput *missionShouldFinish = nullptr;

    void setSteeringFinished(const std_msgs::Bool::ConstPtr& msg);
    void setTractiveFinished(const std_msgs::Bool::ConstPtr& msg);

    void setCones(const sensor_msgs::PointCloud2::ConstPtr& msg);
    void setSpeed(const brc_base::WheelSpeed::ConstPtr& msg);

    void setGear(uint8_t value);
    void setGearAndRpm(const brc_base::DashOne::ConstPtr &msg);
};

#endif // ACCELERATIONTRACKER_H
