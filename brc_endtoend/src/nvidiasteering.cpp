#include "brc_endtoend/nvidiasteering.h"

NvidiaSteering::NvidiaSteering()
{

}

NvidiaSteering::~NvidiaSteering()
{

}

void NvidiaSteering::initDetection(ros::NodeHandle *nodeHandle, CameraThread *cameraThread)
{
    DetectionThread::initDetection(cameraThread);
    detectionPub = nodeHandle->advertise<brc_base::TargetValue>("/endToEnd", 1);

    const String user = getenv("USER");
    const String path = "/home/" + user + "/catkin_ws/brc_settings/brc.uff";

    // TODO
    // create parser
    IUffParser* parser = createUffParser();
    parser->registerInput("brc_in", DimsCHW(channel, height, width), UffInputOrder::kNCHW);
    parser->registerOutput("brc_out/Tanh");

    // load model
    IBuilder* builder = createInferBuilder(gLogger);
    INetworkDefinition* network = builder->createNetwork();

    if(!parser->parse(path.c_str(), *network, nvinfer1::DataType::kFLOAT))
    {
        throw std::invalid_argument("failed to parse FP32");
    }

    /*if(!parser->parse(path.c_str(), *network, nvinfer1::DataType::kHALF))
    {
        throw std::invalid_argument("failed to parse FP16");
    }*/

    //builder->setFp16Mode(true);
    //builder->setDLACore(0);
    //builder->allowGPUFallback(false);
    builder->setMaxBatchSize(1);
    builder->setMaxWorkspaceSize((1 << 30));

    engine = builder->buildCudaEngine(*network);

    if(!engine)
    {
        throw std::invalid_argument("failed to build engine");
    }

    context = engine->createExecutionContext();

    network->destroy();
    builder->destroy();

    initialized = true;
}

void NvidiaSteering::process()
{
    frame = this->getCamera()->getFrame();
    ros::Time tempTime = ros::Time::now();

    auto start = std::chrono::system_clock::now();
    resize(*(frame), resized, Size(224, 224));

    cv::Rect cropRegion(0, 36, width, height);
    cv::Mat cropped = resized(cropRegion);
    cv::cvtColor(cropped, cropped, CV_BGR2RGB);

    //setFrame(make_shared<Mat>(cropped.clone()));

    //context = engine->createExecutionContext();

    int sizeOfInput = height * width * channel * sizeof(float);
    int sizeOfOutput = sizeof(float);

    int inputIndex = engine->getBindingIndex("brc_in");
    int outputIndex = engine->getBindingIndex("brc_out/Tanh");

    int nbBindings = engine->getNbBindings();
    std::vector<void*> buffers(nbBindings);

    cudaMalloc(&buffers[inputIndex], sizeOfInput);
    cudaMalloc(&buffers[outputIndex], sizeOfOutput);

    if (buffers[inputIndex] == nullptr)
    {
        std::cout << "Out of memory" << std::endl;
        exit(1);
    }

    cv::Mat destination(cv::Size(width, height*3), CV_8UC1);

    for (int i = 0; i < cropped.channels(); ++i)
    {
        cv::extractChannel(
            cropped,
            cv::Mat(
                height,
                width,
                CV_8UC1,
                &(destination.at<uint8_t>(height*width*i))),
            i);
    }

    setFrame(make_shared<Mat>(destination.clone()));

    uint8_t* ptr = cropped.ptr();
    size_t inputSize = height * width * channel;
    float* input = new float[inputSize];

    for (size_t i = 0; i < inputSize; i++) {
        input[i] = ptr[i];
    }

    for (int c = 0; c < channel; ++c) {
        for (int h = 0; h < height; ++h) {
            for (int w = 0; w < width; ++w) {
                input[ c*width*height + h*width + w ] = ptr[ c*width*height + h*width + w ];
            }
        }
    }

    cout << input[0] << " " << input[1] << " " << input[2] << " " << endl;

    /*for (int c = 0; c < channel; ++c) {
        for (int h = 0; h < height; ++h) {
            for (int w = 0; w < width; ++w) {
                input[ c*width*height + h*width + w ] = ptr[ c*width*height + h*width + w ];
            }
        }
    }*/

    cudaMemcpy(buffers[inputIndex], input, sizeOfInput, cudaMemcpyHostToDevice);

    context->execute(1, &buffers[inputIndex]);

    float* output = new float[1];
    output[0] = 25.0f;

    cudaMemcpy(output, buffers[outputIndex], sizeOfOutput, cudaMemcpyDeviceToHost);

    float steeringOutput = output[0] * 2500.0f;

    /*if(steeringOutput > 2500.)
    {
        steeringOutput = 2500;
    }

    if(steeringOutput < -2500.)
    {
        steeringOutput = -2500;
    }*/

    float should = 0.7649008f;

    cout << steeringOutput << " " << output[0] << " should: " << should << endl;

    delete[] input;
    delete[] output;

    cudaFree(buffers[inputIndex]);
    cudaFree(buffers[outputIndex]);

    //context->destroy();

    auto end = std::chrono::system_clock::now();

    auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    auto elapsedMS = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

    cout << "microseconds: " << elapsed.count() << endl;
    cout << "milliseconds: " << elapsedMS.count() << endl;

    brc_base::TargetValue msg;
    msg.header.stamp = ros::Time::now();
    msg.targetValue = static_cast<int16_t>(steeringOutput);
    detectionPub.publish(msg);
}
