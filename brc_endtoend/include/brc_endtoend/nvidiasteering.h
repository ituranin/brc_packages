#ifndef NVIDIASTEERING_H
#define NVIDIASTEERING_H

#include <brc_base/detectionthread.h>
#include <brc_base/TargetValue.h>
#include <ros/ros.h>
#include <brc_base/detectionhelpers.h>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "NvInfer.h"
#include "NvUffParser.h"
#include "NvUtils.h"
#include <cuda.h>
#include <cuda_runtime_api.h>

using namespace nvuffparser;
using namespace nvinfer1;

using namespace std;
using namespace cv;

// Logger for TensorRT info/warning/errors
class Logger : public nvinfer1::ILogger
{
public:
    Logger(Severity severity = Severity::kWARNING)
        : reportableSeverity(severity)
    {
    }

    void log(Severity severity, const char* msg) override
    {
        // suppress messages with severity enum value greater than the reportable
        if (severity > reportableSeverity)
            return;

        switch (severity)
        {
        case Severity::kINTERNAL_ERROR: std::cerr << "INTERNAL_ERROR: "; break;
        case Severity::kERROR: std::cerr << "ERROR: "; break;
        case Severity::kWARNING: std::cerr << "WARNING: "; break;
        case Severity::kINFO: std::cerr << "INFO: "; break;
        default: std::cerr << "UNKNOWN: "; break;
        }
        std::cerr << msg << std::endl;
    }

    Severity reportableSeverity;
};

class NvidiaSteering : public DetectionThread
{
public:
    NvidiaSteering();
    ~NvidiaSteering();

    void initDetection(ros::NodeHandle *nodeHandle, CameraThread *cameraThread);

private:

    std::shared_ptr<Mat> frame;
    Mat resized;
    ros::Publisher detectionPub;
    Logger gLogger;
    ICudaEngine* engine = nullptr;
    IExecutionContext* context = nullptr;
    size_t channel = 3;
    size_t height = 80;
    size_t width = 224;
    void process();
};

#endif // NVIDIASTEERING_H
