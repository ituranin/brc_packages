#ifndef BRCCAN_H
#define BRCCAN_H

#include <iostream>

#define TARGETVALUE_MSG_ID          0x111
#define TARGETVALUE_MSG_DLC             8
#define SYSLOG_CPU_MSG_ID           0x121
#define SYSLOG_CPU_MSG_DLC              8

#define MSG_ID_ASSTATE              0x110
#define MSG_BASE_ID_SYSLOG          0x120
#define MSG_BASE_ID_CTLLOG          0x130
#define MSG_BASE_ID_SENSOR_DATA1    0x140
#define MSG_BASE_ID_SENSOR_DATA2    0x150
#define MSG_BASE_ID_SENSOR_DATA3    0x160
#define MSG_BASE_ID_CORSYS          0x300
#define MSG_ID_CORSYS_DATA1         0x301
#define MSG_ID_CORSYS_DATA2         0x302

#define MSG_BASE_ID_WHEELSPEED      0x230
#define MSG_DASH_DATA1              0x201
#define MSG_MAN_STEERING            0x331

typedef struct
{
    uint8_t brakeFront = 0;
    uint8_t brakeRear = 0;
    int16_t steeringAngle = 0;
    uint16_t throttle = 0;
    uint8_t clutch = 0;
    uint8_t shift = 0;
} TargetValues;

typedef struct
{
    double lat = 0.0;
    double lon = 0.0;
    double alt = 0.0;
    double yaw = 0.0;
    double pitch = 0.0;
    double roll = 0.0;
    double accX = 0.0;
    double accY = 0.0;
    double accZ = 0.0;
} GpsDataRAW;

typedef struct
{
    int32_t lat = 0;
    int32_t lon = 0;
    int16_t alt = 0;
    int16_t yaw = 0;
    int16_t pitch = 0;
    int16_t roll = 0;
    int16_t accX = 0;
    int16_t accY = 0;
    int16_t accZ = 0;
} GpsDataCAN;

#endif // BRCCAN_H
