#include <iostream>
#include <boost/unordered_set.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/make_shared.hpp>
#include <class_loader/class_loader.hpp>
#include <socketcan_interface/socketcan.h>
#include <socketcan_interface/threading.h>

#include "ros/ros.h"
#include "brc_base/TargetValue.h"
#include "brc_base/SysLog.h"
#include "brc_base/ASState.h"
#include "brc_base/DashOne.h"
#include "brc_base/ControlLog.h"
#include "brc_base/SensorData.h"

#include "brc_base/BrcHelpers.h"
#include "brc_base/asStates.h"
#include "brc_base/deviceIDs.h"

#include "std_msgs/Bool.h"
#include "std_msgs/Int8.h"
#include "std_msgs/UInt8.h"
#include "std_msgs/Int16.h"
#include "std_msgs/Float64.h"

#include "brc_base/Imu.h"
#include "brc_base/Ins.h"

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>

#include "brccan.h"
#include "fsg_logger_brc.h"

#define PI 3.1415926538
#define HZ 100

using namespace can;

static ThreadedSocketCANInterfaceSharedPtr g_driver = nullptr;
static StateListenerConstSharedPtr error_printer = nullptr;

static ros::Subscriber targetValueSubscribers[DEVICE_ID_NUMBER];
static ros::Subscriber brakeRearSubscriber;
static ros::Subscriber missionStatusSubscriber;
static ros::Subscriber shiftSub;
static ros::Subscriber asSub;
static ros::Subscriber imuSub;
static ros::Subscriber insSub;
static ros::Subscriber amiSub;
static ros::Subscriber ebsSub;
static ros::Subscriber steeringSub;
static ros::Subscriber sbSub;
static ros::Subscriber coneCountSub;
static ros::Subscriber coneSub;
static ros::Subscriber lidarSub;
static ros::Subscriber trackerErrorSub;
static ros::Subscriber targetSpeedSub;
static ros::Subscriber actualSpeedSub;
static ros::Subscriber throttleSub;
static ros::Subscriber brakeSensorSub;
static ros::Subscriber steeringSensorSub;
static ros::Subscriber lapCounterSubscriber;

static ros::NodeHandle* nHandle;

static uint8_t missionStatus = 0;
static uint8_t asMode = 0;
static double accFactor = 1000.0 / 9.81;
static double yprFactor = 1800.0 / PI;
static bool doSend = false;

static GpsDataRAW gpsdataRAW = {};
static GpsDataCAN gpsdataCAN = {};
static TargetValues targets = {};
static fsg_logger_brc_dv_driving_dynamics_1_t dyn1 = {};
static fsg_logger_brc_dv_driving_dynamics_2_t dyn2 = {};
static fsg_logger_brc_dv_system_status_t dv_status = {};
static bool syslogDelayToggle = true;
static std::string interface;

static int syslogCounter = 5; // TODO define
static int targetValueCounter = 5; // TODO define
static bool cameraOn = true;
static ros::Time cameraTime;
static bool imuOn = true;
static ros::Time imuTime;
static bool lidarOn = true;
static ros::Time lidarTime;
static bool trackerFinished = false;

void initializeDriver();

void setBrakeActual(const brc_base::SensorData::ConstPtr& msg)
{
    dyn1.brake_current = static_cast<uint8_t>(((msg->value1 / 100.0) / 40.0) * 100.0);

    if(dyn1.brake_current > 100)
    {
        dyn1.brake_current = 100;
    }
}

void setSteeringActual(const brc_base::SensorData::ConstPtr& msg)
{
    dyn1.steering_angle_current = static_cast<int8_t>((static_cast<int16_t>(msg->value2) / 2500.0) * 12.5 * 2.0);
}

void setMotorMoment(const brc_base::ControlLog::ConstPtr& msg)
{
    dyn1.motor_current = static_cast<int8_t>(((msg->value / 8192.0) * 0.45) * 100.0);
    dyn1.motor_target = static_cast<int8_t>(((msg->targetValue / 8192.0) * 0.45) * 100.0);
}

void trackerFinishedHandle(const std_msgs::UInt8::ConstPtr& msg)
{
    trackerFinished = msg->data;
}

void cameraCones(const sensor_msgs::Image::ConstPtr& msg)
{
    cameraTime = ros::Time::now();
}

void lidarData(const sensor_msgs::PointCloud2::ConstPtr& msg)
{
    lidarTime = ros::Time::now();
}

void countCones(const std_msgs::UInt8::ConstPtr& msg)
{
    dv_status.cones_current = msg->data;
    dv_status.cones_all += msg->data;
}

void setTargetSpeed(const std_msgs::Float64::ConstPtr& msg)
{
    dyn1.speed_target = static_cast<uint8_t>(msg->data * 3.6);
}

void setActualSpeed(const brc_base::DashOne::ConstPtr& msg)
{
    dyn1.speed_current = static_cast<uint8_t>(msg->speed * 0.036);
}

void setMission(const brc_base::SysLog::ConstPtr& msg)
{
    switch(msg->state)
    {
        case MISSION_ACCELERATION: dv_status.ami = 1; break;
        case MISSION_SKIDPAD: dv_status.ami = 2; break;
        case MISSION_TRACKDRIVE: dv_status.ami = 3; break;
        case MISSION_EBSTEST: dv_status.ami = 4; break;
        case MISSION_INSPECTION: dv_status.ami = 5; break;
        case MISSION_AUTOCROSS: dv_status.ami = 6; break;
        default: dv_status.ami = 0; break;
    }
}

void setEBS(const brc_base::SysLog::ConstPtr& msg)
{
    dv_status.ebs = msg->rulesState + 1;
}

void setSteering(const brc_base::SysLog::ConstPtr& msg)
{
    dv_status.steering_state = msg->rulesState;
}

void setSB(const brc_base::SysLog::ConstPtr& msg)
{
    dv_status.sb = msg->rulesState + 1;
}

void setIMUData(const brc_base::Imu::ConstPtr& msg)
{
    gpsdataRAW.yaw = msg->angularRate.x;
    gpsdataRAW.pitch = msg->angularRate.y;
    gpsdataRAW.roll = msg->angularRate.z;
    gpsdataRAW.accX = msg->accel.x;
    gpsdataRAW.accY = msg->accel.y;
    gpsdataRAW.accZ = msg->accel.z;
    imuTime = ros::Time::now();
}

void setGPSData(const brc_base::Ins::ConstPtr& msg)
{
    gpsdataRAW.lat = msg->position.latitude;
    gpsdataRAW.lon = msg->position.longitude;
    gpsdataRAW.alt = msg->position.altitude;
}

void setTargetValueSB(const brc_base::TargetValue::ConstPtr& msg)
{
    targets.brakeFront = static_cast<uint8_t>(msg->targetValue);
    dyn1.brake_target = static_cast<uint8_t>(((targets.brakeFront / 2.0) / 40.0) * 100.0);

    if(dyn1.brake_target > 100)
    {
        dyn1.brake_target = 100;
    }
}

void setTargetValueSBR(const brc_base::TargetValue::ConstPtr& msg)
{
    targets.brakeRear = static_cast<uint8_t>(msg->targetValue);
}

void setTargetValueSA(const brc_base::TargetValue::ConstPtr& msg)
{
    targets.steeringAngle = msg->targetValue;
    dyn1.steering_angle_target = static_cast<int8_t>((targets.steeringAngle / 2500.0) * 12.5 * 2.0);
}

void setTargetValueCL(const brc_base::TargetValue::ConstPtr& msg)
{
    targets.clutch = static_cast<uint8_t>(msg->targetValue);
}

void setTargetValueAPPS(const brc_base::TargetValue::ConstPtr& msg)
{
    targets.throttle = static_cast<uint16_t>(msg->targetValue);
}

void setMissionStatus(const brc_base::SysLog::ConstPtr& msg)
{
    missionStatus = msg->state;
}

void setLapCount(const std_msgs::Int16::ConstPtr &msg)
{
    if(msg->data >= 0)
    {
        dv_status.lap = static_cast<uint8_t>(msg->data);
    }
    else
    {
        dv_status.lap = 0;
    }
}

void handleAsState(const brc_base::ASState::ConstPtr& msg)
{
    asMode = msg->currentState;

    switch(msg->currentState)
    {
        case AS_STATE_OFF: dv_status.as_state = 1; break;
        case AS_STATE_READY: dv_status.as_state = 2; break;
        case AS_STATE_DRIVING: dv_status.as_state = 3; break;
        case AS_STATE_EBS: dv_status.as_state = 4; break;
        case AS_STATE_FINISHED: dv_status.as_state = 5; break;
        default: dv_status.as_state = 0; break;
    }
}

void publishTargetValues()
{
    Frame frame;
    frame.id = TARGETVALUE_MSG_ID;
    frame.dlc = TARGETVALUE_MSG_DLC;
    frame.data[0] = targets.brakeFront;
    frame.data[1] = targets.brakeRear;
    frame.data[2] = HIGH_BYTE(targets.steeringAngle);
    frame.data[3] = LOW_BYTE(targets.steeringAngle);
    frame.data[4] = HIGH_BYTE(targets.throttle);
    frame.data[5] = LOW_BYTE(targets.throttle);
    frame.data[6] = targets.clutch;
    frame.data[7] = targets.shift;
    g_driver->send(frame);
}

//TODO own node maybe
void publishSysLog()
{
    Frame frame;
    frame.id = SYSLOG_CPU_MSG_ID;
    frame.dlc = SYSLOG_CPU_MSG_DLC;
    frame.data[0] = getFullCSState(missionStatus, 0);
    frame.data[6] = ((0b1 & static_cast<uint8_t>(cameraOn)) << 7) | ((0b1 & static_cast<uint8_t>(lidarOn)) << 6) | ((0b1 & static_cast<uint8_t>(imuOn)) << 5);
    frame.data[7] = ((0b1 & static_cast<bool>(trackerFinished)) << 7);
    g_driver->send(frame);
}

void publishIMUData()
{
    Frame imuAcc;
    Frame imuGyro;
    imuAcc.id = 0x320;
    imuGyro.id = 0x321;
    imuAcc.dlc = 8;
    imuGyro.dlc = 8;

    gpsdataCAN.yaw = static_cast<int16_t>(yprFactor * gpsdataRAW.yaw);
    gpsdataCAN.pitch = static_cast<int16_t>(yprFactor * gpsdataRAW.pitch);
    gpsdataCAN.roll = static_cast<int16_t>(yprFactor * gpsdataRAW.roll);
    gpsdataCAN.accX = static_cast<int16_t>(accFactor * gpsdataRAW.accX);
    gpsdataCAN.accY = static_cast<int16_t>(accFactor * gpsdataRAW.accY);
    gpsdataCAN.accZ = static_cast<int16_t>(accFactor * gpsdataRAW.accZ);

    dyn2.acc_lat = fsg_logger_brc_dv_driving_dynamics_2_acc_lat_encode(gpsdataRAW.accY);
    dyn2.acc_long = fsg_logger_brc_dv_driving_dynamics_2_acc_long_encode(gpsdataRAW.accX);

    imuAcc.data[0] = HIGH_BYTE(gpsdataCAN.accX);
    imuAcc.data[1] = LOW_BYTE(gpsdataCAN.accX);
    imuAcc.data[2] = HIGH_BYTE(gpsdataCAN.accY);
    imuAcc.data[3] = LOW_BYTE(gpsdataCAN.accY);
    imuAcc.data[4] = HIGH_BYTE(gpsdataCAN.accZ);
    imuAcc.data[5] = LOW_BYTE(gpsdataCAN.accZ);

    imuGyro.data[0] = HIGH_BYTE(gpsdataCAN.roll);
    imuGyro.data[1] = LOW_BYTE(gpsdataCAN.roll);
    imuGyro.data[2] = HIGH_BYTE(gpsdataCAN.pitch);
    imuGyro.data[3] = LOW_BYTE(gpsdataCAN.pitch);
    imuGyro.data[4] = HIGH_BYTE(gpsdataCAN.yaw);
    imuGyro.data[5] = LOW_BYTE(gpsdataCAN.yaw);

    g_driver->send(imuAcc);
    g_driver->send(imuGyro);
}

void publishGPSData()
{
    Frame gpsMessageOne;
    Frame gpsMessageTwo;
    gpsMessageOne.id = 0x310;
    gpsMessageTwo.id = 0x311;
    gpsMessageOne.dlc = 8;
    gpsMessageTwo.dlc = 8;

    gpsdataCAN.lat = static_cast<int32_t>(gpsdataRAW.lat * 10000000);
    gpsdataCAN.lon = static_cast<int32_t>(gpsdataRAW.lon * 10000000);
    gpsdataCAN.alt = static_cast<int16_t>(gpsdataRAW.alt);

    int8_t latArray[4];
    int8_t lonArray[4];

    int32ToInt8ArrayBigEndian(gpsdataCAN.lat, latArray);
    int32ToInt8ArrayBigEndian(gpsdataCAN.lon, lonArray);

    gpsMessageOne.data[0] = static_cast<uint8_t>(latArray[0]);
    gpsMessageOne.data[1] = static_cast<uint8_t>(latArray[1]);
    gpsMessageOne.data[2] = static_cast<uint8_t>(latArray[2]);
    gpsMessageOne.data[3] = static_cast<uint8_t>(latArray[3]);
    gpsMessageOne.data[4] = static_cast<uint8_t>(lonArray[0]);
    gpsMessageOne.data[5] = static_cast<uint8_t>(lonArray[1]);
    gpsMessageOne.data[6] = static_cast<uint8_t>(lonArray[2]);
    gpsMessageOne.data[7] = static_cast<uint8_t>(lonArray[3]);

    gpsMessageTwo.data[0] = HIGH_BYTE(gpsdataCAN.alt);
    gpsMessageTwo.data[1] = LOW_BYTE(gpsdataCAN.alt);

    g_driver->send(gpsMessageOne);
    g_driver->send(gpsMessageTwo);
}

void publishDataLogger()
{
    Frame dyna1;
    Frame dyna2;
    Frame stats;

    dyna1.id = FSG_LOGGER_BRC_DV_DRIVING_DYNAMICS_1_FRAME_ID;
    dyna1.dlc = FSG_LOGGER_BRC_DV_DRIVING_DYNAMICS_1_LENGTH;
    uint8_t dyna1_data[FSG_LOGGER_BRC_DV_DRIVING_DYNAMICS_1_LENGTH];

    dyna2.id = FSG_LOGGER_BRC_DV_DRIVING_DYNAMICS_2_FRAME_ID;
    dyna2.dlc = FSG_LOGGER_BRC_DV_DRIVING_DYNAMICS_2_LENGTH;
    uint8_t dyna2_data[FSG_LOGGER_BRC_DV_DRIVING_DYNAMICS_2_LENGTH];

    stats.id = FSG_LOGGER_BRC_DV_SYSTEM_STATUS_FRAME_ID;
    stats.dlc = FSG_LOGGER_BRC_DV_SYSTEM_STATUS_LENGTH;
    uint8_t stats_data[FSG_LOGGER_BRC_DV_SYSTEM_STATUS_LENGTH];

    fsg_logger_brc_dv_driving_dynamics_1_pack(dyna1_data, &dyn1, FSG_LOGGER_BRC_DV_DRIVING_DYNAMICS_1_LENGTH);
    fsg_logger_brc_dv_driving_dynamics_2_pack(dyna2_data, &dyn2, FSG_LOGGER_BRC_DV_DRIVING_DYNAMICS_2_LENGTH);
    fsg_logger_brc_dv_system_status_pack(stats_data, &dv_status, FSG_LOGGER_BRC_DV_SYSTEM_STATUS_LENGTH);

    dyna1.data[0] = dyna1_data[0];
    dyna1.data[1] = dyna1_data[1];
    dyna1.data[2] = dyna1_data[2];
    dyna1.data[3] = dyna1_data[3];
    dyna1.data[4] = dyna1_data[4];
    dyna1.data[5] = dyna1_data[5];
    dyna1.data[6] = dyna1_data[6];
    dyna1.data[7] = dyna1_data[7];

    dyna2.data[0] = dyna2_data[0];
    dyna2.data[1] = dyna2_data[1];
    dyna2.data[2] = dyna2_data[2];
    dyna2.data[3] = dyna2_data[3];
    dyna2.data[4] = dyna2_data[4];
    dyna2.data[5] = dyna2_data[5];

    stats.data[0] = stats_data[0];
    stats.data[1] = stats_data[1];
    stats.data[2] = stats_data[2];
    stats.data[3] = stats_data[3];
    stats.data[4] = stats_data[4];

    g_driver->send(dyna1);
    g_driver->send(dyna2);
    g_driver->send(stats);
}

void initSubscribers()
{
    targetValueSubscribers[DEV_ID_ASSBC] = nHandle->subscribe(mainTopics[DEV_ID_ASSBC] + "/targetValue", 1, setTargetValueSB);
    targetValueSubscribers[DEV_ID_ASSAC] = nHandle->subscribe(mainTopics[DEV_ID_ASSAC] + "/targetValue", 1, setTargetValueSA);
    targetValueSubscribers[DEV_ID_ASCAC] = nHandle->subscribe(mainTopics[DEV_ID_ASCAC] + "/targetValue", 1, setTargetValueCL);
    targetValueSubscribers[DEV_ID_ASETC] = nHandle->subscribe(mainTopics[DEV_ID_ASETC] + "/targetValue", 1, setTargetValueAPPS);
    brakeRearSubscriber = nHandle->subscribe("/serviceBrakeRear/targetValue", 1, setTargetValueSBR);
}

void handleShiftRequest(const std_msgs::Bool::ConstPtr& msg)
{
    if(msg->data) {
        targets.shift = 2;
    } else {
        targets.shift = 1;
    }
}

void print_error(const State & s){
    std::string err;
    g_driver->translateError(s.internal_error,err);
    if(s.internal_error == 4)
    {
        doSend = false;
    }
    std::cout << "ERROR: state=" << s.driver_state << " internal_error=" << s.internal_error << "('" << err << "') asio: " << s.error_code << std::endl;
}

void initializeDriver()
{
    if(g_driver != nullptr)
    {
        g_driver->shutdown();
    }

    g_driver = std::make_shared<ThreadedSocketCANInterface>();
    error_printer = g_driver->createStateListener(print_error);
}

int main(int argc, char *argv[]){

    ros::init(argc, argv, "brcCANtargetValueSender");
    ros::NodeHandle n;
    nHandle = &n;

    interface = argv[1];
    dv_status.lap = 0;
    dv_status.cones_current = 0;
    dv_status.cones_all = 0;

    initSubscribers();
    missionStatusSubscriber = nHandle->subscribe("/director/state", 1, setMissionStatus);
    shiftSub = nHandle->subscribe("/shiftRequest", 1, handleShiftRequest);
    asSub = nHandle->subscribe("/asState", 1, handleAsState);
    imuSub = nHandle->subscribe("/vectornav/IMU", 1, setIMUData);
    insSub = nHandle->subscribe("/vectornav/INS", 1, setGPSData);
    amiSub = nHandle->subscribe("/mission/syslog", 1, setMission);
    ebsSub = nHandle->subscribe("/ebs/syslog", 1, setEBS);
    steeringSub = nHandle->subscribe("/steering/syslog", 1, setSteering);
    sbSub = nHandle->subscribe("/serviceBrake/syslog", 1, setSB);
    coneCountSub = nHandle->subscribe("/coneCount", 1, countCones);
    coneSub = nHandle->subscribe("/camera/ImageTransport", 1, cameraCones);
    lidarSub = nHandle->subscribe("/velodyne_points", 1, lidarData);
    trackerErrorSub = nHandle->subscribe("/accTracker/finished", 1, trackerFinishedHandle);
    targetSpeedSub = nHandle->subscribe("/VelocityLimit", 1, setTargetSpeed);
    actualSpeedSub = nHandle->subscribe("/dashOne", 1, setActualSpeed);
    throttleSub = nHandle->subscribe("/throttle/controlLog", 1, setMotorMoment);
    brakeSensorSub = nHandle->subscribe("/serviceBrake/sensorData1", 1, setBrakeActual);
    steeringSensorSub = nHandle->subscribe("/steering/sensorData1", 1, setSteeringActual);
    lapCounterSubscriber = nHandle->subscribe("/LapCount", 1, setLapCount);

    cameraTime = ros::Time::now();
    imuTime = ros::Time::now();
    lidarTime = ros::Time::now();

    std::cout.precision(17); // set debug print precision

    if(argc != 2)
    {
        std::cout << "specify interface" << std::endl;
        return 1;
    }

    initializeDriver();

    if(!g_driver->init(interface, false))
    {
        print_error(g_driver->getState());
        return 1;
    }

    doSend = true;

    ros::Rate r(HZ);

    while(ros::ok())
    {
    	ros::spinOnce();

        if(((ros::Time::now() - cameraTime).toSec() > 0.5))
        {
            cameraOn = false;
        }
        else
        {
            cameraOn = true;
        }

        if(((ros::Time::now() - lidarTime).toSec() > 0.5))
        {
            lidarOn = false;
        }
        else
        {
            lidarOn = true;
        }

        if(((ros::Time::now() - imuTime).toSec() > 0.5))
        {
            imuOn = false;
        }
        else
        {
            imuOn = true;
        }

        if(doSend)
        {
            if(asMode == AS_STATE_READY || asMode == AS_STATE_DRIVING)
            {
                if(targetValueCounter > 4)
                {
                    publishTargetValues();
                    targetValueCounter = 0;
                    // reset shift request after sending
                    if(targets.shift != 0)
                    {
                        targets.shift = 0;
                    }
                }
                targetValueCounter++;
            }

            if(syslogCounter > 9)
            {
                publishSysLog();
                publishDataLogger();
                syslogCounter = 0;
            }
            syslogCounter++;

            publishGPSData();
            publishIMUData();
        }
        else
        {
            if(g_driver != nullptr)
            {
                g_driver->shutdown();
            }

            g_driver = std::make_shared<ThreadedSocketCANInterface>();
            error_printer = g_driver->createStateListener(print_error);
            //std::cout << "before retry" << std::endl;
            if(!g_driver->init(interface, false))
            {
                print_error(g_driver->getState());
                //std::cout << "retry fail" << std::endl;
            }
            else
            {
                doSend = true;
                //std::cout << "retry success" << std::endl;
            }
            //std::cout << "after retry" << std::endl;
        }

    	r.sleep();
    }

    g_driver->shutdown();
    g_driver.reset();

    return 0;
}
