#include <iostream>
#include <boost/unordered_set.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/make_shared.hpp>
#include <class_loader/class_loader.hpp>
#include <socketcan_interface/socketcan.h>
#include <socketcan_interface/threading.h>

#include "ros/ros.h"
#include "brc_base/ASState.h"
#include "brc_base/SysLog.h"
#include "brc_base/ControlLog.h"
#include "brc_base/Error.h"
#include "brc_base/TargetValue.h"
#include "brc_base/SensorData.h"
#include "brc_base/SensorVoltages.h"
#include "brc_base/CorSysData1.h"
#include "brc_base/CorSysData2.h"
#include "brc_base/WheelSpeed.h"
#include "brc_base/DashOne.h"

#include <std_msgs/Bool.h>
#include <std_msgs/Int16.h>

#include <brc_base/BrcHelpers.h>
#include <brc_base/asStates.h>
#include <brccan.h>

using namespace can;

static ThreadedSocketCANInterfaceSharedPtr g_driver = nullptr;
static FrameListenerConstSharedPtr frame_printer = nullptr;
static StateListenerConstSharedPtr error_printer = nullptr;

static ros::Publisher asStatePub;
static ros::Publisher corSysData1Pub;
static ros::Publisher corSysData2Pub;
static ros::Publisher wheelSpeedPub;

static ros::Publisher manSteeringPub;
static ros::Publisher dashOnePub;

static ros::NodeHandle* nHandle;

static ros::Publisher publishers[DEVICE_ID_NUMBER][5];
static std::string interface;

void initializeDriver();

void initPublishers()
{
    for (size_t i = 0; i < DEVICE_ID_NUMBER; i++)
    {
        // we do not need publishers for assc(has it's own), cpu and vc
        //if(i < 0x3) continue;

        publishers[i][0] = nHandle->advertise<brc_base::SysLog>(mainTopics[i] + "/syslog", 1);
        publishers[i][1] = nHandle->advertise<brc_base::ControlLog>(mainTopics[i] + "/controlLog", 1);
        publishers[i][2] = nHandle->advertise<brc_base::SensorData>(mainTopics[i] + "/sensorData1", 1);
        publishers[i][3] = nHandle->advertise<brc_base::SensorData>(mainTopics[i] + "/sensorData2", 1);
        publishers[i][4] = nHandle->advertise<brc_base::SensorData>(mainTopics[i] + "/sensorData3", 1);
    }
}

void handleCorSys(const Frame &f, ros::Time rTime)
{
  switch (f.id)
  {
    case MSG_ID_CORSYS_DATA1:
    {
        brc_base::CorSysData1 data1Msg;
        data1Msg.header.stamp = rTime;
        data1Msg.timestamp = to16bit(f.data[0], f.data[1]);
        data1Msg.v = to16bit(f.data[2], f.data[3]);
        data1Msg.vq = static_cast<int16_t>(to16bit(f.data[4], f.data[5]));
        data1Msg.beta = static_cast<int16_t>(to16bit(f.data[6], f.data[7]));
        corSysData1Pub.publish(data1Msg);
        break;
    }
    case MSG_ID_CORSYS_DATA2:
    {
        brc_base::CorSysData2 data2Msg;
        data2Msg.header.stamp = rTime;
        data2Msg.distance = to16bit(f.data[0], f.data[1]);
        corSysData2Pub.publish(data2Msg);
        break;
    }
    default: break; // may become handy for 0x300 msg (corsys info)
  }
}

// TODO update message handling (bit shifting)
void publishFrame(const Frame &f)
{
  if(f.is_error || f.is_rtr) {
      return;
  }

  ros::Time rTime = ros::Time::now();

  if(f.id == MSG_ID_ASSTATE)
  {
      brc_base::ASState asStateMsg;
      uint8_t state = getASState(f.data[0]); // TODO maybe get cp and vc commands
      asStateMsg.header.stamp = rTime;
      asStateMsg.mode = state;
      asStateMsg.currentState = state;
      asStateMsg.nextState = state;
      asStatePub.publish(asStateMsg);
      return;
  }

  if(f.id == MSG_MAN_STEERING)
  {
      std_msgs::Int16 manSteeringMsg;
      manSteeringMsg.data = static_cast<int16_t>(to16bit(f.data[7], f.data[6]));
      manSteeringPub.publish(manSteeringMsg);
      return;
  }

  if(f.id == MSG_DASH_DATA1)
  {
      brc_base::DashOne dashOneMsg;
      dashOneMsg.header.stamp = rTime;
      dashOneMsg.gear = static_cast<int16_t>(to16bit(f.data[1], f.data[0])) - 2;
      dashOneMsg.rpm = static_cast<int16_t>(to16bit(f.data[3], f.data[2]));
      dashOneMsg.speed = static_cast<int16_t>(to16bit(f.data[5], f.data[4])); // wheelSpeeds from front wheels
      dashOnePub.publish(dashOneMsg);
      return;
  }

  switch (f.id & MSG_BASE_FILTER)
  {
    case MSG_BASE_ID_SYSLOG:
    {
        //if(f.data[0] == 1) break;
        brc_base::SysLog sysMsg;
        uint8_t state = getCSState(f.data[0]); // TODO maybe get byte6 and byte7 flags
        sysMsg.header.stamp = rTime;
        sysMsg.mode = state;
        sysMsg.rulesState = state;
        sysMsg.state = getCSSubstate(f.data[0]);
        publishers[filterDeviceID(f.id)][0].publish(sysMsg);
        break;
    }
    case MSG_BASE_ID_CTLLOG:
    {
        brc_base::ControlLog ctlMsg;
        ctlMsg.header.stamp = rTime;
        ctlMsg.actuatorState = f.data[0];
        ctlMsg.actuatorCurrent = f.data[1];
        ctlMsg.outputSignal = to16bit(f.data[3], f.data[2]);
        ctlMsg.targetValue = static_cast<int16_t>(to16bit(f.data[5], f.data[4]));
        ctlMsg.value = static_cast<int16_t>(to16bit(f.data[7], f.data[6]));
        publishers[filterDeviceID(f.id)][1].publish(ctlMsg);
        break;
    }
    case MSG_BASE_ID_SENSOR_DATA1:
    {
        brc_base::SensorData dataMsg;
        dataMsg.header.stamp = rTime;
        dataMsg.value1 = to16bit(f.data[1], f.data[0]);
        dataMsg.value2 = to16bit(f.data[3], f.data[2]);
        dataMsg.value3 = to16bit(f.data[5], f.data[4]);
        dataMsg.value4 = to16bit(f.data[7], f.data[6]);
        publishers[filterDeviceID(f.id)][2].publish(dataMsg);
        break;
    }
    case MSG_BASE_ID_SENSOR_DATA2:
    {
        brc_base::SensorData dataMsg;
        dataMsg.header.stamp = rTime;
        dataMsg.value1 = to16bit(f.data[1], f.data[0]);
        dataMsg.value2 = to16bit(f.data[3], f.data[2]);
        dataMsg.value3 = to16bit(f.data[5], f.data[4]);
        dataMsg.value4 = to16bit(f.data[7], f.data[6]);
        publishers[filterDeviceID(f.id)][3].publish(dataMsg);
        break;
    }
    case MSG_BASE_ID_SENSOR_DATA3:
    {
        brc_base::SensorData dataMsg;
        dataMsg.header.stamp = rTime;
        dataMsg.value1 = to16bit(f.data[1], f.data[0]);
        dataMsg.value2 = to16bit(f.data[3], f.data[2]);
        dataMsg.value3 = to16bit(f.data[5], f.data[4]);
        dataMsg.value4 = to16bit(f.data[7], f.data[6]);
        publishers[filterDeviceID(f.id)][4].publish(dataMsg);
        break;
    }
    case MSG_BASE_ID_CORSYS:
    {
        handleCorSys(f, rTime);
        break;
    }
    case MSG_BASE_ID_WHEELSPEED:
    {
        brc_base::WheelSpeed wheelSpeedMsg;
        wheelSpeedMsg.header.stamp = rTime;
        wheelSpeedMsg.frontLeft = to16bit(f.data[1], f.data[0]);
        wheelSpeedMsg.frontRight = to16bit(f.data[3], f.data[2]);
        wheelSpeedMsg.rearLeft = to16bit(f.data[5], f.data[4]);
        wheelSpeedMsg.rearRight = to16bit(f.data[7], f.data[6]);
        wheelSpeedPub.publish(wheelSpeedMsg);
        break;
    }
    default: break;
  }
}

void print_error(const State & s)
{
    std::string err;
    g_driver->translateError(s.internal_error,err);
    std::cout << "ERROR: state=" << s.driver_state << " internal_error=" << s.internal_error << "('" << err << "') asio: " << s.error_code << std::endl;
}

void initializeDriver()
{
    if(g_driver != nullptr)
    {
        g_driver->shutdown();
    }

    g_driver = std::make_shared<ThreadedSocketCANInterface>();
    frame_printer = g_driver->createMsgListener(publishFrame);
    error_printer = g_driver->createStateListener(print_error);
}

int main(int argc, char *argv[])
{

    ros::init(argc, argv, "brcCAN");
    ros::NodeHandle n;
    nHandle = &n;

    interface = argv[1];

    initPublishers();
    asStatePub = n.advertise<brc_base::ASState>("asState", 1);
    corSysData1Pub = n.advertise<brc_base::CorSysData1>("corSys/DATA1", 1);
    corSysData2Pub = n.advertise<brc_base::CorSysData2>("corSys/DATA2", 1);
    wheelSpeedPub = n.advertise<brc_base::WheelSpeed>("wheelSpeed", 1);
    manSteeringPub = n.advertise<std_msgs::Int16>("manSteering", 1);
    dashOnePub = n.advertise<brc_base::DashOne>("dashOne", 1);

    std::cout.precision(17); // set debug print precision

    if(argc != 2)
    {
        std::cout << "specify interface" << std::endl;
        return 1;
    }

    initializeDriver();

    if(!g_driver->init(interface, false))
    {
        print_error(g_driver->getState());
        return 1;
    }

    ros::spin();

    g_driver->shutdown();
    g_driver.reset();

    return 0;
}
