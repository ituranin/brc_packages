#include <iostream>
#include <stdexcept>
#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <brc_base/TargetValue.h>
#include <brc_base/SysLog.h>
#include <brc_base/calculation/int32inputoutput.h>
#include <brc_base/asStates.h>
#include <brc_inspection/inspectionsteering.h>
#include <brc_inspection/teststeering.h>
#include <brc_acceleration/accelerationsteering.h>
#include <brc_autox/autoxsteeringv1.h>
#include <brc_cruisecontrol/cruisecontrolsteering.h>

#define RATE 20

static ros::Subscriber missionSubscriber;
static ros::Subscriber directorGoSubscriber;
static ros::Publisher sacTargetValuePublisher;
static BaseCalculation* calculation = nullptr;
static Int32InputOutput* steeringValue = nullptr;

static uint8_t mission = MISSION_NONE;
static bool directorGo = false;

void setMission(const brc_base::SysLog::ConstPtr& msg) {
    mission = msg->state;
}

void setDirectorGo(const std_msgs::Bool::ConstPtr& msg) {
    directorGo = msg->data;
}

int main(int argc, char *argv[]){

    ros::init(argc, argv, "steering");
    ros::NodeHandle n;

    missionSubscriber = n.subscribe("/mission/syslog", 1, setMission);
    directorGoSubscriber = n.subscribe("/director/go", 1, setDirectorGo);
    sacTargetValuePublisher = n.advertise<brc_base::TargetValue>("/steering/targetValue", 1);

    ros::Rate r(RATE);

    steeringValue = new Int32InputOutput();
    steeringValue->setDescription("steeringResult");

    while (ros::ok() && mission == MISSION_NONE) {
        ros::spinOnce();
        r.sleep();
    }

    switch(mission) {
        case MISSION_NONE: throw std::invalid_argument("mission none"); break;
        case MISSION_MANUAL: throw std::invalid_argument("mission manual"); break;
        //case MISSION_EBSTEST:
        case MISSION_ACCELERATION: {
            AccelerationSteering* accelerationCalc = new AccelerationSteering();
            accelerationCalc->setNodeHandle(&n);
            accelerationCalc->setRate(RATE);
            accelerationCalc->addResultHolder(steeringValue);
            accelerationCalc->initialize();

            calculation = accelerationCalc;
            break;
        }
        case MISSION_INSPECTION: {
            InspectionSteering* inspectionCalc = new InspectionSteering();
            inspectionCalc->setNodeHandle(&n);
            inspectionCalc->setRate(RATE);
            inspectionCalc->addResultHolder(steeringValue);
            inspectionCalc->initialize();

            calculation = inspectionCalc;
            break;
        }
        case MISSION_EBSTEST:
        case MISSION_AUTOCROSS:
        case MISSION_TRACKDRIVE:
        case MISSION_SKIDPAD: {
            CruiseControlSteering* skidpadCalc = new CruiseControlSteering();
            skidpadCalc->setNodeHandle(&n);
            skidpadCalc->setRate(RATE);
            skidpadCalc->addResultHolder(steeringValue);
            skidpadCalc->initialize();

            calculation = skidpadCalc;
            break;
        }
        case MISSION_CONTROLLER: {
            TestSteering* testCalc = new TestSteering();
            testCalc->setNodeHandle(&n);
            testCalc->setRate(RATE);
            testCalc->addResultHolder(steeringValue);
            testCalc->initialize();

            calculation = testCalc;
            break;
        }
        default: throw std::invalid_argument("mission unknown"); break;
    }

    while(ros::ok() && directorGo == false) {
        ros::spinOnce();
        r.sleep();
    }

    while(ros::ok()) {
        ros::spinOnce();

        calculation->calculate();

        brc_base::TargetValue sacMsg;
        sacMsg.header.stamp = ros::Time::now();
        sacMsg.targetValue = static_cast<int16_t>(steeringValue->value);
        sacTargetValuePublisher.publish(sacMsg);

        r.sleep();
    }

    return 0;

}
