#include <cstdlib>
#include <ros/ros.h>
#include <brc_matrixvision/bluefox3capture.h>
#include <opencv2/opencv.hpp>
#include <brc_base/videothread.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

using namespace std;
using namespace cv;

shared_ptr<image_transport::ImageTransport> itRight;
image_transport::Publisher ipRight;

shared_ptr<image_transport::ImageTransport> itLeft;
image_transport::Publisher ipLeft;

void captureCallbackRight(shared_ptr<Mat> image)
{
    cv_bridge::CvImage bImage;
    bImage.header.stamp = ros::Time::now();
    bImage.image = image->clone();
    bImage.encoding = "bgr8";
    ipRight.publish(bImage.toImageMsg());
}

void captureCallbackLeft(shared_ptr<Mat> image)
{
    cv_bridge::CvImage bImage;
    bImage.header.stamp = ros::Time::now();
    bImage.image = image->clone();
    bImage.encoding = "bgr8";
    ipLeft.publish(bImage.toImageMsg());
}

int main(int argc, char *argv[]){

    ros::init(argc, argv, "stereo_camera");
    ros::NodeHandle n;

    itRight = make_shared<image_transport::ImageTransport>(n);
    ipRight = itRight->advertise("/cameraRight/ImageTransport", 1);

    itLeft = make_shared<image_transport::ImageTransport>(n);
    ipLeft = itLeft->advertise("/cameraLeft/ImageTransport", 1);

    std::string user = std::getenv("USER");

    Bluefox3Capture cameraRight;
    cameraRight.setFps(30);
    cameraRight.setSerial("-");
    cameraRight.initCamera("/home/" + user + "/catkin_ws/brc_settings/bluefoxRGB8_flat2048_new.xml");
    cameraRight.setCallback(&captureCallbackRight);

    Bluefox3Capture cameraLeft;
    cameraLeft.setFps(30);
    cameraLeft.setSerial("-");
    cameraLeft.initCamera("/home/" + user + "/catkin_ws/brc_settings/bluefoxRGB8_flat2048_new.xml");
    cameraLeft.setCallback(&captureCallbackLeft);

    cameraRight.startCapture();
    cameraLeft.startCapture();

    while(ros::ok());

    ros::shutdown();

    return 0;

}
